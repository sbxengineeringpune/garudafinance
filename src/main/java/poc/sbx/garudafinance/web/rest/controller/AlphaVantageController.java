package poc.sbx.garudafinance.web.rest.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import poc.sbx.garudafinance.config.alpha.DataType;
import poc.sbx.garudafinance.service.AlphaVantageService;
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval;
import poc.sbx.garudafinance.service.alpha.timeseries.CryptoCurrencySearchResult;
import poc.sbx.garudafinance.service.alpha.timeseries.CryptoCurrencyTimeSeriesResult;
import poc.sbx.garudafinance.service.alpha.timeseries.CryptocurrencyFunction;
import poc.sbx.garudafinance.service.alpha.timeseries.CurrencyExchange;
import poc.sbx.garudafinance.service.alpha.timeseries.ForeignExchangeFunction;
import poc.sbx.garudafinance.service.alpha.timeseries.ForeignExchangeResult;
import poc.sbx.garudafinance.service.alpha.timeseries.InvalidSymbolLengthException;
import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;
import poc.sbx.garudafinance.service.alpha.timeseries.SymbolResult;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeriesFunction;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeriesResult;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeseriesDataResult;
import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity;

@RestController
@RequestMapping("api/alpha/")
public class AlphaVantageController {

	private AlphaVantageService alphaService;

	public AlphaVantageController(){}

	@Autowired
	public AlphaVantageController(AlphaVantageService alphaService) {
		this.alphaService = alphaService;
	}

	/**
	 * Rest end point for getting Symbol Search result Data from Alpha Vantage API.
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping("stock/search")
	public SymbolResult getSymbolSearchData(@RequestParam("keyword") String keyword)
			throws IOException, MissingRequiredQueryParameterException {
		return alphaService.getSymbols(keyword);
	}
	
	/**
	 * Rest end point for getting TimeSeries Data from Alpha Vantage API.
	 * 
	 * @param symbol
	 * @param function
	 * @param interval
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@GetMapping("stock")
	public TimeSeriesResult getTimeSeriesData(@RequestParam("symbol") String symbol,
			@RequestParam("function") TimeSeriesFunction function,
			@RequestParam(value = "interval", required = false) IntradayInterval interval,
			@RequestParam(value = "username", required = false) String user)
			throws IOException, MissingRequiredQueryParameterException {
		UserEntity loggedinUser = new UserEntity();
		loggedinUser.setUsername(user);
		return alphaService.getTimeSeriesData(symbol, function, interval);
	}
	
	/**
	 * Rest end point for getting TimeSeries Data point from Alpha Vantage API.
	 * 
	 * @param symbol
	 * @param function
	 * @param dataType (csv or json)
	 * @param interval
	 * @return
	 * @throws Exception
	 */
	@GetMapping("stock/data")
	public TimeseriesDataResult getTimeSeriesDataPoint(@RequestParam("symbol") String symbol,
			@RequestParam("function") TimeSeriesFunction function,
			@RequestParam("datatype") DataType dataType,
			@RequestParam(value = "interval", required = false) IntradayInterval interval,
			@RequestParam(value = "userid", required = false) Long userId)
			throws IOException, MissingRequiredQueryParameterException {
		UserEntity loggedinUser = new UserEntity();
		loggedinUser.setUserId(userId);
		
		return alphaService.getTimeSeriesDataPoint(symbol, function, interval, dataType, loggedinUser);
	}

	@GetMapping("forex")
	public CurrencyExchange getExchangeRate(@RequestParam("to_symbol") String toSymbol,
			@RequestParam("from_symbol") String fromSymbol)
			throws MissingRequiredQueryParameterException, InvalidSymbolLengthException, IOException {
		return alphaService.getCurrencyExchange(toSymbol, fromSymbol);
	}

    @GetMapping("forexseries")
    public ForeignExchangeResult getForeignExchangeSeries(@RequestParam("function") ForeignExchangeFunction function,
                                                          @RequestParam(value = "interval", required = false) IntradayInterval interval,
                                                          @RequestParam("from_currency") String fromCurrency, @RequestParam("to_currency") String toCurrency)
        throws MissingRequiredQueryParameterException, InvalidSymbolLengthException, IOException {
        return alphaService.getForeignExchangeSeries(function, interval, fromCurrency, toCurrency);
    }
	
	/**
	 * Rest end point for getting TimeSeries Data point from Alpha Vantage Forex API.
	 *
	 * @param function
	 * @param dataType (csv or json)
	 * @param interval
	 * @return
	 * @throws Exception
	 */
	@GetMapping("forexseries/data")
	public TimeseriesDataResult getForexSeriesDataPoint(
			@RequestParam("function") ForeignExchangeFunction function,
			@RequestParam(value = "interval", required = false) IntradayInterval interval,
			@RequestParam("from_currency") String fromCurrency, @RequestParam("to_currency") String toCurrency,
			@RequestParam("datatype") DataType dataType,
			@RequestParam(value = "userid", required = false) Long userId)
			throws IOException, MissingRequiredQueryParameterException {
		UserEntity loggedinUser = new UserEntity();
		loggedinUser.setUserId(userId);
		
		return alphaService.getForexSeriesDataPoint(function, interval, fromCurrency, toCurrency, dataType, loggedinUser);
	}
	
	@GetMapping("crypto/search")
	public CryptoCurrencySearchResult getCryptoCurrencies(@RequestParam("keyword") String  keyword)
			throws IOException {
		return alphaService.getCryptoSearchResult(keyword);
	}
	
	@GetMapping("crypto")
	public CryptoCurrencyTimeSeriesResult getCryptocurrencyTimeSeriesData(@RequestParam("function") CryptocurrencyFunction function,
			 @RequestParam("symbol") String  symbol)
			throws MissingRequiredQueryParameterException, InvalidSymbolLengthException, IOException {
		return alphaService.getCryptocurrencyTimeSeriesData(function, symbol);
	}
	
	/**
	 * Rest end point for getting TimeSeries Data point from Alpha Vantage Crypto API.
	 *
	 * @param function
	 * @param dataType (csv or json)
	 * @return
	 * @throws InvalidSymbolLengthException 
	 * @throws Exception
	 */
	@GetMapping("crypto/data")
	public TimeseriesDataResult getCryptoDataPoint(
			@RequestParam("function") CryptocurrencyFunction function,
			@RequestParam("symbol") String  symbol,
			@RequestParam("datatype") DataType dataType,
			@RequestParam(value = "userid", required = false) Long userId)
			throws IOException, MissingRequiredQueryParameterException, InvalidSymbolLengthException {
		UserEntity loggedinUser = new UserEntity();
		loggedinUser.setUserId(userId);
		
		return alphaService.getCryptoDataPoint(function, symbol, dataType, loggedinUser);
	}
	
}
