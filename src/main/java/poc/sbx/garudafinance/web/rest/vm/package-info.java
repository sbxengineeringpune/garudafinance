/**
 * View Models used by Spring MVC REST controllers.
 */
package poc.sbx.garudafinance.web.rest.vm;
