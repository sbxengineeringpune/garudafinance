package poc.sbx.garudafinance.web.rest.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import poc.sbx.garudafinance.service.TickDataService;
import poc.sbx.garudafinance.service.alpha.timeseries.InvalidSymbolLengthException;
import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeriesResult;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeseriesDataResult;
import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchFutureResult;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchSymbolResult;

@RestController
@RequestMapping("api/tickdata")
public class TickDataController {

	@Autowired
    private TickDataService tickDataService;


    @GetMapping("/search/stocks")
    public TickSearchSymbolResult getStockSearchResult(@RequestParam("keyword") String keyword) throws IOException, MissingRequiredQueryParameterException {
        return tickDataService.getStockSearchResult(keyword);
    }

    @GetMapping("/dailybars/{type}")
    public TimeSeriesResult getDailyBarsData(@PathVariable("type") String type, @RequestParam("symbol") String symbol, @RequestParam("companyId") String companyId, @RequestParam("daysBack") String daysBack) throws IOException, MissingRequiredQueryParameterException {
    	TimeSeriesResult result = tickDataService.getDailyBarsData(type, symbol, companyId, daysBack);;
    	return result;
    }
    
	/**
	 * Rest end point for getting TimeSeries Data point from TickData Daily Bars API.
	 *
	 * @param symbol
	 * @param companyId
	 * @param daysBack
	 * @return
	 * @throws InvalidSymbolLengthException 
	 * @throws Exception
	 */
	@GetMapping("dailybars/data/{type}")
	public TimeseriesDataResult getDailyBarsDataPoint(
			@PathVariable("type") String type,
			@RequestParam("symbol") String  symbol,
			@RequestParam("companyId") String companyId,
			@RequestParam("daysBack") String daysBack,
			@RequestParam(value = "userid", required = false) Long userId)
			throws IOException, MissingRequiredQueryParameterException, InvalidSymbolLengthException {
		UserEntity loggedinUser = new UserEntity();
		loggedinUser.setUserId(userId);
		
		TimeseriesDataResult result = tickDataService.getDailyBarsDataPoint(type, symbol, companyId, daysBack,loggedinUser);
    	
    	return result;
	}
	
    @GetMapping("/search/futures")
    public TickSearchFutureResult getFuturesData(@RequestParam("keyword") String keyword) throws IOException, MissingRequiredQueryParameterException {
        return tickDataService.getFuturesSearhResult(keyword);
    }
}
