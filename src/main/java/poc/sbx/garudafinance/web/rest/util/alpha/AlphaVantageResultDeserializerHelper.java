package poc.sbx.garudafinance.web.rest.util.alpha;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.JsonNode;

import poc.sbx.garudafinance.service.alpha.response.MetaData;


public class AlphaVantageResultDeserializerHelper {

  public static Map<String, Object> sanitizeNodeKeys(JsonNode jsonNode) {
    Map<String, Object> sanitizedNodes = new HashMap<>();
    jsonNode.fields().forEachRemaining((node) -> {
      String regexMatch = Regex.getMatch(REMOVE_NUMBER_REGEX, node.getKey());
      if (regexMatch != null) {
        sanitizedNodes.put(regexMatch, node.getValue());
      }
      else {
        sanitizedNodes.put(node.getKey(), node.getValue());
      }
    });
    return sanitizedNodes;
  }



    public static Map<String, Object> sanitizeNodeKeysForCryptoCurrency(JsonNode jsonNode) {
        Map<String, Object> sanitizedNodes = new HashMap<>();
        jsonNode.fields().forEachRemaining((node) -> {
            String regexMatch = searchAndGetKey(node.getKey());
            sanitizedNodes.put(regexMatch, node.getValue());
        });
        return sanitizedNodes;
    }


  public static MetaData getMetaData(JsonNode node)
      throws IOException {
    Map<String, Object> sanitizedNodes = sanitizeNodeKeys(
        node.get(MetaData.META_DATA_RESPONSE_KEY)
    );
    return JsonParser.toObject(
        JsonParser.toJson(sanitizedNodes),
        MetaData.class
    );
  }

  public static <T> Map<Date, T> getDateObjectMap(JsonNode node, Class<T> resultObject) {
    Map<Date, T> dateObjMap = new TreeMap<>();
    node.fields().forEachRemaining(nodeEntry -> {
      // ignore meta data, we want the time series data
      if (!nodeEntry.getKey().equals(MetaData.META_DATA_RESPONSE_KEY)) {
        nodeEntry.getValue().fields().forEachRemaining(timeSeriesEntry -> {
          try {
            dateObjMap.put(
                parseDate(timeSeriesEntry.getKey()),
                JsonParser.toObject(
                    JsonParser.toJson(sanitizeNodeKeys(timeSeriesEntry.getValue())),
                    resultObject
                )
            );
          }
          catch (IOException | ParseException e) {
            e.printStackTrace();
          }
        });
      }
    });
    return dateObjMap;
  }

  private static Date parseDate(String dateStr)
      throws ParseException {
    Date date = DATE_PARSER.parse(dateStr);
    if (dateStr.length() > 10)
      date = DATE_TIME_PARSER.parse(dateStr);
    return date;
  }


    public static String searchAndGetKey(String text) {
       String open = "open";
       String close = "close";
       String high = "high";
       String low = "low";
       String volume = "volume";
       String marketCap = "market cap";


        if(text.contains(open))
            return open;
        else if(text.contains(close))
            return close;
        else if(text.contains(high))
            return high;
        else if(text.contains(low))
            return low;
        else if(text.contains(volume))
            return volume;
        else if(text.contains(marketCap))
            return marketCap;
        else
            return text;

    }

  private static final String REMOVE_NUMBER_REGEX = "\\d*.\\s(.*)";

  private static final SimpleDateFormat DATE_TIME_PARSER = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
  private static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd");
}
