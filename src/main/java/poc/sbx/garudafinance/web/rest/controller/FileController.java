package poc.sbx.garudafinance.web.rest.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import poc.sbx.garudafinance.config.alpha.FileEditterService;
import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity;
import poc.sbx.garudafinance.service.causalens.FileRequest;
import poc.sbx.garudafinance.service.causalens.MultiFileRequest;

@RestController
@RequestMapping("/api/file/")
public class FileController {

	private FileEditterService fileService;

	public FileController(){}

	@Autowired
	public FileController(FileEditterService fileService) {
		this.fileService = fileService;
	}

	@PostMapping("/causaTarget/multi")
	public ResponseEntity<List<String>> getTargetValues(@RequestBody MultiFileRequest fileRequest) throws Exception {
		List<String> list = fileService.getTargets(fileRequest.getCsvFilePaths());
		return new ResponseEntity<List<String>>(list, HttpStatus.OK);

	}
	
	@PostMapping("/causaTarget")
	public ResponseEntity<List<String>> getTargetValues(@RequestBody FileRequest fileRequest) throws Exception {
		List<String> list = fileService.getTargets(fileRequest.getCsvFilePath());
		return new ResponseEntity<List<String>>(list, HttpStatus.OK);

	}

	@GetMapping("/causaFileList")
	public ResponseEntity<List<UserEntity>> getFileList(@RequestParam("loggedinuser") Long loggedInUser) {
		List<UserEntity> list = fileService.getFileList(loggedInUser);
		return new ResponseEntity<List<UserEntity>>(list, HttpStatus.OK);
	}
	
	@GetMapping("download/{fileName}")
	public ResponseEntity<Resource> getDownloadFile(HttpServletResponse response, @PathVariable("fileName") String fileName) throws IOException {
		
		return fileService.getFile(fileName);
	}
}
