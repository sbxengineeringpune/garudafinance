package poc.sbx.garudafinance.web.rest.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import poc.sbx.garudafinance.service.CausalensService;
import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;
import poc.sbx.garudafinance.service.causalens.CausaLensClientRequest;
import poc.sbx.garudafinance.service.causalens.CausaLensTimeSeriesResponse;

@RestController
@RequestMapping("api/causa/")
public class CausaLensController {

	@Autowired
	private CausalensService causaService;

	@PostMapping("prediction")
	public CausaLensTimeSeriesResponse getTimeSeriesPredictionFromCausalens(
			@Valid @RequestBody CausaLensClientRequest request) throws IOException, MissingRequiredQueryParameterException {
		return causaService.getTimeSeriesPredictionFromCausalens(request);
	}
//	@PostMapping("merge-prediction")
//	public CausaLensTimeSeriesResponse getMergedCSVFiles(
//			@Valid @RequestBody CausaLensRequest alphaRequest,@Valid @RequestBody CausaLensRequest tickRequest) throws IOException, MissingRequiredQueryParameterException {
//		return causaService.getMergedCSVFiles(alphaRequest,tickRequest);
//	}
}
