package poc.sbx.garudafinance.service.tickdata;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import poc.sbx.garudafinance.service.alpha.response.ErrorResponse;

@Service
public class TickApiRequest {

    private static final String USER_NAME = "lawrence@deephorizons.ai";
    private static final String PASSWORD = "trading7";

    private final static String TICK_URL = "https://tickapi.tickdata.com/stream?";
    private final static String TICK_HELP_URL = "https://tickapi.tickdata.com/help?";

    private static HttpHeaders createHeaders() {
        String auth = USER_NAME + ":" + PASSWORD;
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));

        return new HttpHeaders() {
            {
                String authHeader = "Basic " + new String(encodedAuth);
                set("Authorization", authHeader);
                setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
            }
        };
    }

    /**
     * This method performs the HTTP call to the Alpha Vantage API.
     *
     * @param queryParameters Perform the API call with the given query parameters.
     * @return The API response in JSON.
     */
    static String sendRequest(String queryParameters) {

        ResponseEntity<String> responseEntity = new RestTemplate().getForEntity(TICK_HELP_URL + queryParameters,
            String.class);

        if (responseEntity.getBody() != null) {
            return responseEntity.getBody();
        } else if (responseEntity.getBody() == null && responseEntity.getStatusCode() == HttpStatus.OK) {
            responseEntity = new ResponseEntity<String>("[]", HttpStatus.OK);
            return responseEntity.getBody();
        } else {
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setError("Sorry! No Result Found");
            errorResponse.setQueryParameters(queryParameters);
            throw new HttpServerErrorException(responseEntity.getStatusCode(), errorResponse.toString(),
                responseEntity.getHeaders(), null, Charset.defaultCharset());
        }

    }

    /**
     * This method performs the HTTP call to the Alpha Vantage API.
     *
     * @param queryParameters Perform the API call with the given query parameters.
     * @return The API response in CSV file.
     * @throws IOException
     */

    static File sendFileRequest(String queryParameters) throws IOException {
        File file = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<String> entity = new HttpEntity<String>(createHeaders());
            ResponseEntity<byte[]> response = restTemplate.exchange(TICK_URL + queryParameters, HttpMethod.GET, entity, byte[].class);
            file = File.createTempFile("tickDataTimeSeriesData", ".csv");
            FileUtils.writeByteArrayToFile(file, response.getBody() == null ? new byte[]{}: response.getBody());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return file;
    }


}
