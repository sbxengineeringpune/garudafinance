package poc.sbx.garudafinance.service.tickdata.request;

/**
 * Available parameters for the query parameter function in the TimeSeries
 * endpoints.
 */
public enum Field {
	DATE("DATE_FIELD"),
	OPEN_PRICE("OPEN_PRICE_FIELD"),
	HIGH_PRICE("HIGH_PRICE_FIELD"),
	LOW_PRICE("LOW_PRICE_FIELD"),
	CLOSE_PRICE("CLOSE_PRICE_FIELD"),
	VOLUME("VOLUME_FIELD"),
	TICK_COUNT("TICK_COUNT_FIELD"),
	UP_TICKS("UP_TICKS_FIELD"),
	DOWN_TICKS("DOWN_TICKS_FIELD");
	

	Field(String queryParameterKey) {
		this.queryParameterKey = queryParameterKey;
	}

	public String getQueryParameterKey() {
		return queryParameterKey;
	}

	private String queryParameterKey;
}
