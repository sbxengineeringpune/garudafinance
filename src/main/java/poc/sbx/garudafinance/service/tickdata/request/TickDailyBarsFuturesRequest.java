package poc.sbx.garudafinance.service.tickdata.request;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import poc.sbx.garudafinance.service.alpha.request.APIRequest;
import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;

/**
 * A wrapper class for the available query parameters for the TimeSeries
 * endpoints of the API.
 */  
@Builder
public class TickDailyBarsFuturesRequest implements APIRequest {


    private String symbol;
    private String companyId;
    private String daysBack;


  /**
   * Convert all the selected query parameters to a query parameter string
   * to be used the in the API request.
   * @return A Query parameter string.
   */
  public String toQueryParameters()
      throws MissingRequiredQueryParameterException {
    if (symbol == null || symbol.isEmpty())
      throw new MissingRequiredQueryParameterException("Futures");
    if (daysBack == null || daysBack.isEmpty())
          throw new MissingRequiredQueryParameterException("required number of days");
    
    List<String> fields = new ArrayList<String>();
    fields.add(Field.DATE.getQueryParameterKey());
    fields.add(Field.OPEN_PRICE.getQueryParameterKey());
    fields.add(Field.HIGH_PRICE.getQueryParameterKey());
    fields.add(Field.LOW_PRICE.getQueryParameterKey());
    fields.add(Field.CLOSE_PRICE.getQueryParameterKey());
    
    String selectedFields = String.join("|", fields);
    
    StringBuilder builder = new StringBuilder();
    builder
        .append("COLLECTION_TYPE=")
        .append(CollectionType.FUTURES.getQueryParameterKey())
        .append("&EXTRACT_TYPE=")
        .append(ExtractType.FUT_TICK_DAILYBAR.getQueryParameterKey())
        .append("&REQUESTED_DATA=")
        .append(companyId +"|"+symbol)
        .append("&SELECTED_FIELDS=")
        .append(selectedFields)
        .append("&DATE_FORMAT=")
        .append("yyyy/MM/dd")
        .append("&DAYS_BACK=")
        .append(daysBack)
        .append("&INCLUDE_HEADER_ROW=")
        .append("true")
        .append("&COMPRESSION_TYPE=")
        .append(CompressionType.NONE);
        
    return builder.toString();
  }


}
