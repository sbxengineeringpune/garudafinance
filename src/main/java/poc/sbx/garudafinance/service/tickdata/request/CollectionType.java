package poc.sbx.garudafinance.service.tickdata.request;

/**
 * Available parameters for the query parameter function in the TimeSeries
 * endpoints.
 */
public enum CollectionType {
	US_TED("COLLECTION_TYPE.US_TED"),
	FUTURES("COLLECTION_TYPE.FUTURES");

	CollectionType(String queryParameterKey) {
		this.queryParameterKey = queryParameterKey;
	}

	public String getQueryParameterKey() {
		return queryParameterKey;
	}

	private String queryParameterKey;
}
