package poc.sbx.garudafinance.service.tickdata.request;

import lombok.Builder;
import poc.sbx.garudafinance.service.alpha.request.APIRequest;
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval;
import poc.sbx.garudafinance.service.alpha.request.OutputSize;
import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;

/**
 * A wrapper class for the available query parameters for the Futures search
 * endpoints of the API.
 */
@Builder
public class TickSearchFuturesRequest implements APIRequest {

  /**
   * Convert all the selected query parameters to a query parameter string
   * to be used the in the API request.
   * @return A Query parameter string.
   */
  public String toQueryParameters()
      throws MissingRequiredQueryParameterException {
	  
    StringBuilder builder = new StringBuilder();
    builder
        .append("with=")
        .append("New_Symbol")
        .append("&for=")
        .append("COLLECTOR_SUBTYPE_FUT_TICK_DAILYBAR")
        .append("&as=")
        .append("js");
    
    return builder.toString();
  }

}
