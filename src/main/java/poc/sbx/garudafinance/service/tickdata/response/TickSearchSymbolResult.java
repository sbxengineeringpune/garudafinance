package poc.sbx.garudafinance.service.tickdata.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TickSearchSymbolResult {
	private List<TickSymbol> symbols;
	
}
