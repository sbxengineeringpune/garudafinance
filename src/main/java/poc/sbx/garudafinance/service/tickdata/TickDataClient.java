package poc.sbx.garudafinance.service.tickdata;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Component;

import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;
import poc.sbx.garudafinance.service.tickdata.request.TickDailyBarsFuturesRequest;
import poc.sbx.garudafinance.service.tickdata.request.TickDailyBarsStocksRequest;
import poc.sbx.garudafinance.service.tickdata.request.TickSearchFuturesRequest;
import poc.sbx.garudafinance.service.tickdata.request.TickSearchSymbolRequest;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchFutureResult;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchFuturesResultDeserializer;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchSymbolResult;
import poc.sbx.garudafinance.service.tickdata.response.TickSymbolResultDeserializer;
import poc.sbx.garudafinance.web.rest.util.alpha.JsonParser;

@Component
public class TickDataClient implements ITickDataClient {

	
	public TickDataClient() {
		JsonParser.addDeserializer(TickSearchSymbolResult.class, new TickSymbolResultDeserializer());
		JsonParser.addDeserializer(TickSearchFutureResult.class, new TickSearchFuturesResultDeserializer());
		
	}

	@Override
	public TickSearchSymbolResult getTickSymbolList(String keyword) throws MissingRequiredQueryParameterException, IOException {
		String queryParameters = TickSearchSymbolRequest.builder().keyword(keyword).build().toQueryParameters();
		return sendAPIRequest(queryParameters, TickSearchSymbolResult.class);
	}

    @Override
    public File getDailyBarsStocksData(String symbol, String companyId, String daysBack) throws IOException, MissingRequiredQueryParameterException {
        String queryParameters = TickDailyBarsStocksRequest.builder().symbol(symbol).companyId(companyId).daysBack(daysBack).build().toQueryParameters();
        return sendFileAPIRequest(queryParameters);
    }

	@Override
	public TickSearchFutureResult getTickFuturesList() throws MissingRequiredQueryParameterException, IOException {
		String queryParameters = TickSearchFuturesRequest.builder().build().toQueryParameters();
		return sendAPIRequest(queryParameters, TickSearchFutureResult.class);
	}
	
    /**
     *
     * @param queryParameters The query parameter string from the Request.
     * @param resultObject    The expected result object from the API.
     * @return The Result of the API request.
     * @throws IOException 
     */
    private <T> T sendAPIRequest(String queryParameters, Class<T> resultObject) throws IOException {
        return JsonParser.toObject(TickApiRequest.sendRequest(queryParameters), resultObject);
    }

    private File sendFileAPIRequest(String queryParameters) throws IOException {
     return TickApiRequest.sendFileRequest(queryParameters);
    }

	@Override
	public File getDailyBarsFuturesData(String symbol, String companyId, String daysBack)
			throws IOException, MissingRequiredQueryParameterException {
		String queryParameters = TickDailyBarsFuturesRequest.builder().symbol(symbol).companyId(companyId).daysBack(daysBack).build().toQueryParameters();
        return sendFileAPIRequest(queryParameters);
	}
}
