package poc.sbx.garudafinance.service.tickdata.request;

/**
 * Available parameters for the query parameter function in the TimeSeries
 * endpoints.
 */
public enum ExtractType {
	US_TICK_DAILYBAR("COLLECTOR_SUBTYPE_US_TICK_DAILYBAR"),
	FUT_TICK_DAILYBAR("COLLECTOR_SUBTYPE_FUT_TICK_DAILYBAR");

	ExtractType(String queryParameterKey) {
		this.queryParameterKey = queryParameterKey;
	}

	public String getQueryParameterKey() {
		return queryParameterKey;
	}

	private String queryParameterKey;
}
