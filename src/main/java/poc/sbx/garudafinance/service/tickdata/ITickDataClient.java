package poc.sbx.garudafinance.service.tickdata;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchFutureResult;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchSymbolResult;

public interface ITickDataClient {

	 TickSearchSymbolResult getTickSymbolList(String keyword) throws IOException, MissingRequiredQueryParameterException;

     File getDailyBarsStocksData(String symbol, String companyId, String daysBack) throws IOException, MissingRequiredQueryParameterException;
     
     TickSearchFutureResult getTickFuturesList() throws IOException, MissingRequiredQueryParameterException;
     
     File getDailyBarsFuturesData(String symbol, String companyId, String daysBack) throws IOException, MissingRequiredQueryParameterException;
     

}
