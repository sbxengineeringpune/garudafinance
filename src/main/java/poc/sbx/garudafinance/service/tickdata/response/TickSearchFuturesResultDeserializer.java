package poc.sbx.garudafinance.service.tickdata.response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import poc.sbx.garudafinance.web.rest.util.alpha.JsonParser;

public class TickSearchFuturesResultDeserializer extends JsonDeserializer<TickSearchFutureResult> {

	@Override
	  public TickSearchFutureResult deserialize(
	      com.fasterxml.jackson.core.JsonParser parser, DeserializationContext context)
	      throws IOException {
		TickSearchFutureResult searchFutureResult = new TickSearchFutureResult();
	    ObjectCodec oc = parser.getCodec();
	    JsonNode node = oc.readTree(parser);
	    try {
	    	searchFutureResult.setFutures(getSymbolList(node));
	    } catch (Throwable throwable) {
	      System.out.println("Error when deserializing:");
	      System.out.println(node.toString());
	      throwable.printStackTrace();
	    }
	    	    
	    return searchFutureResult;
	  }
	
	 public static List<TickFutures> getSymbolList(JsonNode node)
		      throws IOException {
		    List<TickFutures> futuresList = new ArrayList<>();
		    
		    node.forEach(futureEntry -> {
		    			try {
		    				futuresList.add(JsonParser.toObject(
							        JsonParser.toJson(futureEntry),
							        TickFutures.class
							    ));
						} catch (IOException e) {
				            e.printStackTrace();
				          }
		    		});
		   
		    
		    return futuresList;
		  }
}
