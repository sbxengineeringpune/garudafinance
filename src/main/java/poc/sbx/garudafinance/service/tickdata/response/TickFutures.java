package poc.sbx.garudafinance.service.tickdata.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TickFutures {

	private String description;
	private String value;
	@JsonProperty("alt_description")private String altDescription;
	private String type;
}
