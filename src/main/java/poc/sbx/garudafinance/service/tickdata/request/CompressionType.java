package poc.sbx.garudafinance.service.tickdata.request;

/**
 * Available parameters for the query parameter function in the TimeSeries
 * endpoints.
 */
public enum CompressionType {
	GZIP,
	ZIP,
	NONE;	
}
