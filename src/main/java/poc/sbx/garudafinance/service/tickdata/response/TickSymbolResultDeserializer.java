package poc.sbx.garudafinance.service.tickdata.response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import poc.sbx.garudafinance.web.rest.util.alpha.JsonParser;

public class TickSymbolResultDeserializer extends JsonDeserializer<TickSearchSymbolResult> {

	@Override
	  public TickSearchSymbolResult deserialize(
	      com.fasterxml.jackson.core.JsonParser parser, DeserializationContext context)
	      throws IOException {
		TickSearchSymbolResult symbolResult = new TickSearchSymbolResult();
	    ObjectCodec oc = parser.getCodec();
	    JsonNode node = oc.readTree(parser);
	    try {
	    	symbolResult.setSymbols(getSymbolList(node));
	    } catch (Throwable throwable) {
	      System.out.println("Error when deserializing:");
	      System.out.println(node.toString());
	      throwable.printStackTrace();
	    }
	    return symbolResult;
	  }
	
	 public static List<TickSymbol> getSymbolList(JsonNode node)
		      throws IOException {
		    List<TickSymbol> symbolList = new ArrayList<>();
		    
		    node.forEach(symbolEntry -> {
		    			try {
							symbolList.add(JsonParser.toObject(
							        JsonParser.toJson(symbolEntry),
							        TickSymbol.class
							    ));
						} catch (IOException e) {
				            e.printStackTrace();
				          }
		    		});
		   
		    
		    return symbolList;
		  }
}
