package poc.sbx.garudafinance.service.tickdata.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TickSymbol {

	private String companyId;
	private String symbol;
	private String companyName;
	private String exchange;
	private String startDate;
	private String endDate;
	private String valueStartDate;
	private String valueEndDate;
	private String altIdType;
	private String altIdValue;
	private String newestSymbol;
	private String quoteStartDate;
	private String quoteEndDate;
	private String optionStartDate;
	private String optionEndDate;
	private String stockType;
	private String altStartDate;
	private String altEndDate;
	
	
}
