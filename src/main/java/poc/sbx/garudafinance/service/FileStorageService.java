package poc.sbx.garudafinance.service;

import static poc.sbx.garudafinance.service.util.FileHelper.createCsvHeader;
import static poc.sbx.garudafinance.service.util.FileHelper.csvWritter;
import static poc.sbx.garudafinance.service.util.FileHelper.getMergedRecordsForTwoCsv;
import static poc.sbx.garudafinance.service.util.FileHelper.readCsv;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import poc.sbx.garudafinance.config.FileStorageProperties;
import poc.sbx.garudafinance.exception.FileStorageException;
import poc.sbx.garudafinance.exception.MyFileNotFoundException;
import poc.sbx.garudafinance.service.alpha.timeseries.CsvEntity;
import poc.sbx.garudafinance.service.util.DateUtil;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String convertToCausaLensFormatSave(File file, String fileName, String dateFormat, String dateDataType) {
    	
    	Path targetLocation = this.fileStorageLocation.resolve(fileName);
    	
        try(BufferedWriter writer = Files.newBufferedWriter(targetLocation);
        		Reader reader = new FileReader(file);
        		CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);) {        	

			List<CsvEntity> csvObjArray = new ArrayList<CsvEntity>();
			
			for (CSVRecord csvRecord : csvParser) {
				CsvEntity csvObj = new CsvEntity();
				List<String> columnList =  new ArrayList<String>();
				csvRecord.forEach(column -> {
					columnList.add(column);
				});
				csvObj.setColumns(columnList);
				csvObjArray.add(csvObj);
			}
			
			List<String> headerRecordColumns = new ArrayList<String>();
			if(!csvObjArray.isEmpty()) {
				csvObjArray.get(0).getColumns().forEach(col -> {
					headerRecordColumns.add(col.toLowerCase());
				});
				headerRecordColumns.set(0, "");
				csvObjArray.remove(0);
			}
			
			//Datatype header row
			List<String> headerDataType = new ArrayList<String>();
			headerDataType.add(dateDataType);
			
			//DateFormat header row
			List<String> headerDateFormat = new ArrayList<String>(headerRecordColumns.size());
			headerDateFormat.add("dateformat="+dateFormat);
			
			for(int index = 1; index < headerRecordColumns.size(); index++) {
				headerDataType.add("ValuesNumerical");
				headerDateFormat.add("");
			}
						
			CSVPrinter csvPrinter = new CSVPrinter(writer,
					CSVFormat.DEFAULT.withHeader(headerRecordColumns.toArray(new String[headerRecordColumns.size()])));
			csvPrinter.printRecord(headerDataType);
			csvPrinter.printRecord(headerDateFormat);
			csvObjArray.stream().limit(100).forEach(csvObject -> {
					try {
						csvPrinter.printRecord(csvObject.getColumns());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			});

			csvPrinter.flush();
        	
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
        
        return fileName;
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }


    public File mergeTwoCsvFile(File firstFile, File secondFile, String firstFileName, String secondFileName) {

        List<List<String>> firstFileRecords = readCsv(firstFile);
        List<List<String>> secondFileRecods = readCsv(secondFile);
        List<List<String>> newRecords = new ArrayList<>();
        List<List<String>> csvHeaders = new ArrayList<>();
        CharSequence future = "FUTURES";
        String fileNameComponents[] = firstFileName.split("-");
        String symbol = fileNameComponents.length > 0 ? fileNameComponents[0]: "BTC";
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        Path targetLocation = this.fileStorageLocation.resolve(symbol+"-DAILY-FUTURES-MERGED-"+timeStamp+".csv");

        if (firstFileRecords.size() > secondFileRecods.size()) {
            newRecords = getMergedRecordsForTwoCsv(firstFileRecords, secondFileRecods);
            if(firstFileName.contains(future)){
                csvHeaders = createCsvHeader(true, firstFileRecords.get(0),secondFileRecods.get(0));
            }else {
                csvHeaders = createCsvHeader(false, firstFileRecords.get(0),secondFileRecods.get(0));
            }

        } else if (secondFileRecods.size() > firstFileRecords.size()) {
            newRecords = getMergedRecordsForTwoCsv(secondFileRecods, firstFileRecords);
            if(secondFileName.contains(future)){
                csvHeaders = createCsvHeader(true,  secondFileRecods.get(0),firstFileRecords.get(0));
            }else {
                csvHeaders = createCsvHeader(false, secondFileRecods.get(0),firstFileRecords.get(0));
            }
        } else {
            newRecords = getMergedRecordsForTwoCsv(firstFileRecords, secondFileRecods);
            if(firstFileName.contains(future)){
                csvHeaders = createCsvHeader(true, firstFileRecords.get(0),secondFileRecods.get(0));
            }else {
                csvHeaders = createCsvHeader(false, firstFileRecords.get(0),secondFileRecods.get(0));
            }
        }

        Comparator<List<String>> comp = new Comparator<List<String>>() {
            public int compare(List<String> csvLine1, List<String> csvLine2) {
                
            	Date date1 = DateUtil.parseDateFromString(csvLine1.get(0));
            	Date date2 = DateUtil.parseDateFromString(csvLine2.get(0));
            	
                return date1.compareTo(date2);
            }
        };
        Collections.sort(newRecords, comp);
        
        newRecords.add(0, csvHeaders.get(0));
        newRecords.add(1, csvHeaders.get(1));
        newRecords.add(2, csvHeaders.get(2));

        return csvWritter(newRecords,targetLocation.toString());
    }
}
