package poc.sbx.garudafinance.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class TimescaleDTO {

    @JsonProperty("data")
    public List<TimeScaleData> timeScaleData;

    @JsonProperty("meta_data")
    private Map<String,String> metaData;

    public List<TimeScaleData> getTimeScaleData() {
        return timeScaleData;
    }

    public void setTimeScaleData(List<TimeScaleData> timeScaleData) {
        this.timeScaleData = timeScaleData;
    }

    public Map<String, String> getMetaData() {
        return metaData;
    }

    public void setMetaData(Map<String, String> metaData) {
        this.metaData = metaData;
    }
}
