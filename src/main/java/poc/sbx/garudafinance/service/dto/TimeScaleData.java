package poc.sbx.garudafinance.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

@Component
public class TimeScaleData {
    @JsonProperty("time")
    private DateTime timeInstant;

    @JsonProperty("value")
    private String value;

    public DateTime getTimeInstant() {
        return timeInstant;
    }

    public void setTimeInstant(DateTime timeInstant) {
        this.timeInstant = timeInstant;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
