package poc.sbx.garudafinance.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import poc.sbx.garudafinance.config.alpha.DataType;
import poc.sbx.garudafinance.config.alpha.IAlphaVantageClient;
import poc.sbx.garudafinance.repository.UserDetailsRepository;
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval;
import poc.sbx.garudafinance.service.alpha.request.OutputSize;
import poc.sbx.garudafinance.service.alpha.timeseries.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static poc.sbx.garudafinance.service.util.FileHelper.*;


@Service
public class AlphaVantageService {

    @Autowired
    private IAlphaVantageClient client;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Value("${target.port}")
    private String port;

    @Value("${target.host}")
    private String host;

    private static final Logger logger = LoggerFactory.getLogger(AlphaVantageService.class);

    /**
     * @param symbol
     * @param function
     * @param interval
     * @return
     * @throws IOException
     * @throws MissingRequiredQueryParameterException
     */
    public TimeSeriesResult getTimeSeriesData(String symbol, TimeSeriesFunction function, IntradayInterval interval)
        throws IOException, MissingRequiredQueryParameterException {

        TimeSeriesResult result = null;

        if (function != null && function.equals(TimeSeriesFunction.INTRADAY) && interval != null) {
            result = client.getTimeSeries(interval, symbol, OutputSize.COMPACT);
        } else {
            result = client.getTimeSeries(function, symbol);
        }
        return result;
    }

    /**
     * Returns data point for time series data for stocks.
     *
     * @param symbol
     * @param function
     * @param interval
     * @param dataType
     * @param loggedinUser
     * @return
     * @throws IOException
     * @throws MissingRequiredQueryParameterException
     */
    public TimeseriesDataResult getTimeSeriesDataPoint(String symbol, TimeSeriesFunction function,
                                                       IntradayInterval interval, DataType dataType, UserEntity loggedinUser)
        throws IOException, MissingRequiredQueryParameterException {
        String dateDataType = "IndexDate";
        String dateFormat = "%Y-%m-%d";

        File file = null;
        if (function != null && function.equals(TimeSeriesFunction.INTRADAY) && interval != null) {
            file = client.getTimeSeriesDataPoint(interval, symbol, OutputSize.COMPACT, dataType);
            dateFormat = "%Y-%m-%d %H:%M:%S";
            dateDataType = "IndexTime";
        } else {
            file = client.getTimeSeriesDataPoint(function, symbol, dataType);
        }

        // Creating a file name with format
        // <SYMBOL>-<FUNCTION>-<INTERVAL>-<USER_ID>-<TIMESTAMP>.<datatype>
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        StringBuilder fileName = new StringBuilder();
        fileName.append(symbol).append("-").append(function).append("-");
        if (interval != null)
            fileName.append(interval.getQueryParameterKey()).append("-");
        fileName.append(loggedinUser.getUserId()).append("-").append(timeStamp).append(".").append(dataType.name());

        String savedFileName = fileStorageService.convertToCausaLensFormatSave(file, fileName.toString(),
            dateFormat, dateDataType);

        String fileDownloadUri = host + ":" + port + "/garudafinance/api/file/download/" + savedFileName;

        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(loggedinUser.getUserId());
        userEntity.setPath(fileDownloadUri);
        userEntity.setFileName(savedFileName);
        userDetailsRepository.save(userEntity);

        return new TimeseriesDataResult(fileDownloadUri, savedFileName);
    }

    /**
     * @param keyword
     * @return
     * @throws IOException
     * @throws MissingRequiredQueryParameterException
     */
    @Cacheable("symbols")
    public SymbolResult getSymbols(String keyword) throws IOException, MissingRequiredQueryParameterException {
        SymbolResult result = null;
        result = client.getSymbolList(keyword);
        return result;
    }

    /**
     * @param toSymbol
     * @param fromSymbol
     * @return
     * @throws IOException
     * @throws MissingRequiredQueryParameterException
     * @throws InvalidSymbolLengthException
     */
    @Cacheable("forex")
    public CurrencyExchange getCurrencyExchange(String toSymbol, String fromSymbol)
        throws MissingRequiredQueryParameterException, InvalidSymbolLengthException, IOException {
        CurrencyExchange result = null;
        result = client.getCurrencyExchange(toSymbol, fromSymbol);
        return result;
    }

    /**
     * Gets the Forex timeseries data.
     *
     * @param function
     * @param interval
     * @param fromCurrency
     * @param toCurrency
     * @return
     * @throws MissingRequiredQueryParameterException
     * @throws IOException
     */
    public ForeignExchangeResult getForeignExchangeSeries(ForeignExchangeFunction function, IntradayInterval interval,
                                                          String fromCurrency, String toCurrency) throws MissingRequiredQueryParameterException, IOException {
        ForeignExchangeResult result = null;

        if (function != null && function.equals(ForeignExchangeFunction.INTRADAY) && interval != null) {
            result = client.getForeignExchange(function, fromCurrency, toCurrency, interval);
        } else {
            result = client.getForeignExchange(function, fromCurrency, toCurrency);
        }

        return result;
    }

    /**
     * Gets the data point for forex file.
     *
     * @param function
     * @param interval
     * @param dataType
     * @param loggedinUser
     * @return
     * @throws IOException
     * @throws MissingRequiredQueryParameterException
     */
    public TimeseriesDataResult getForexSeriesDataPoint(ForeignExchangeFunction function, IntradayInterval interval,
                                                        String fromCurrency, String toCurrency, DataType dataType, UserEntity loggedinUser)
        throws IOException, MissingRequiredQueryParameterException {

        String dateDataType = "IndexDate";
        String dateFormat = "%Y-%m-%d";

        File file = null;
        if (function != null && function.equals(ForeignExchangeFunction.INTRADAY) && interval != null) {
            file = client.getForexSeriesDataPoint(interval, fromCurrency, toCurrency, OutputSize.COMPACT, dataType);
            dateFormat = "%Y-%m-%d %H:%M:%S";
            dateDataType = "IndexTime";
        } else {
            file = client.getForexSeriesDataPoint(function, fromCurrency, toCurrency, dataType);
        }

        // Creating a file name with format <FROM CURRENCY>_<TO
        // CURRENCY>-<FUNCTION>-<INTERVAL>-<USER_ID>-<TIMESTAMP>.datatype
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        StringBuilder fileName = new StringBuilder();
        fileName.append(fromCurrency).append("_").append(toCurrency).append("-").append(function).append("-");
        if (interval != null)
            fileName.append(interval.getQueryParameterKey()).append("-");
        fileName.append(loggedinUser.getUserId()).append("-").append(timeStamp).append(".").append(dataType.toString());

        String savedFileName = fileStorageService.convertToCausaLensFormatSave(file, fileName.toString(), dateFormat, dateDataType);

        String fileDownloadUri = host + ":" + port + "/garudafinance/api/file/download/" + savedFileName;

        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(loggedinUser.getUserId());
        userEntity.setPath(fileDownloadUri);
        userEntity.setFileName(savedFileName);
        userDetailsRepository.save(userEntity);

        return new TimeseriesDataResult(fileDownloadUri, savedFileName);
    }

    /**
     * @param function
     * @param symbol
     * @return
     * @throws MissingRequiredQueryParameterException
     * @throws IOException
     * @throws InvalidSymbolLengthException
     */
    public CryptoCurrencyTimeSeriesResult getCryptocurrencyTimeSeriesData(CryptocurrencyFunction function,
                                                                          String symbol) throws MissingRequiredQueryParameterException, IOException, InvalidSymbolLengthException {
        return client.getCrtproCurrencyTimeSeriesData(function, symbol);
    }

    /**
     * @param keyword
     * @return
     */
    @Cacheable("crypto-currencies")
    public CryptoCurrencySearchResult getCryptoSearchResult(String keyword) {
        String key = keyword.toUpperCase();
        CryptoCurrencySearchResult result = new CryptoCurrencySearchResult();
        List<CryptoCurrency> cryptoCurrencies = new ArrayList<CryptoCurrency>();
        File file = new File(AlphaVantageService.class.getResource("/digital_currency_list.csv").getPath());
        List<List<String>> listRecords = readCsv(file);
        listRecords.remove(0);

        listRecords.forEach(item -> {
            String code = item.get(0).toUpperCase();
            String name = item.get(1).toUpperCase();

            if (code.startsWith(key) || code.contains(key) || name.startsWith(key) || name.contains(key)) {
                CryptoCurrency crypto = new CryptoCurrency();
                crypto.setCurrencyCode(item.get(0));
                crypto.setCurrencyName(item.get(1));
                cryptoCurrencies.add(crypto);
            }

        });

        result.setCryptoCurrencies(cryptoCurrencies);
        return result;
    }

    public TimeseriesDataResult getCryptoDataPoint(CryptocurrencyFunction function, String symbol, DataType dataType,
                                                   UserEntity loggedinUser)
        throws MissingRequiredQueryParameterException, InvalidSymbolLengthException, IOException {

        String dateDataType = "IndexDate";
        String dateFormat = "%Y-%m-%d";

        File file = client.getCryptoDataPoint(function, symbol, dataType);

        file = updateCsv(file);
        // Creating a file name with format
        // <SYMBOL>-<FUNCTION>-<USER_ID>-<TIMESTAMP>.<datatype>
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        StringBuilder fileName = new StringBuilder();
        fileName.append(symbol).append("-").append(function).append("-");
        fileName.append(loggedinUser.getUserId()).append("-").append(timeStamp).append(".").append(dataType.name());

        String savedFileName = fileStorageService.convertToCausaLensFormatSave(file, fileName.toString(), dateFormat, dateDataType);

        String fileDownloadUri = host + ":" + port + "/garudafinance/api/file/download/" + savedFileName;

        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(loggedinUser.getUserId());
        userEntity.setPath(fileDownloadUri);
        userEntity.setFileName(savedFileName);
        userDetailsRepository.save(userEntity);

        return new TimeseriesDataResult(fileDownloadUri, savedFileName);
    }


}
