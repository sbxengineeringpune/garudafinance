package poc.sbx.garudafinance.service;

import static poc.sbx.garudafinance.service.util.DateUtil.parseDate;
import static poc.sbx.garudafinance.service.util.FileHelper.readCsv;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import poc.sbx.garudafinance.repository.UserDetailsRepository;
import poc.sbx.garudafinance.service.alpha.response.MetaData;
import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeries;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeriesResult;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeseriesDataResult;
import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity;
import poc.sbx.garudafinance.service.tickdata.ITickDataClient;
import poc.sbx.garudafinance.service.tickdata.response.TickFutures;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchFutureResult;
import poc.sbx.garudafinance.service.tickdata.response.TickSearchSymbolResult;

@Service
public class TickDataService {

    @Autowired
    private ITickDataClient tickClient;
    
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Value("${target.port}")
    private String port;

    @Value("${target.host}")
    private String host;
    
    private static TickSearchFutureResult futuresResult = null;
    
    private static final Logger logger = LoggerFactory.getLogger(TickDataService.class);
   
    
    /**
     * Service method to retrieve search result for symbol key in TickData.
     *
     * @param keyword
     * @return
     * @throws MissingRequiredQueryParameterException
     * @throws IOException
     */
    @Cacheable("tick-symbols")
    public TickSearchSymbolResult getStockSearchResult(String keyword) throws IOException, MissingRequiredQueryParameterException {
        return tickClient.getTickSymbolList(keyword);
    }

    /**
     * Retrieves Daily bars data from TickData API.
     * @param symbol
     * @param companyId
     * @param daysBack
     * @return
     * @throws IOException
     * @throws MissingRequiredQueryParameterException
     */
    public TimeSeriesResult getDailyBarsData(String type, String symbol, String companyId, String daysBack) throws IOException, MissingRequiredQueryParameterException {
        Map<ZonedDateTime, TimeSeries> dateTimeSeriesMap = new TreeMap<>();
        File file = null;
        String info = null;
        
		switch(type) {
	        case "stocks": file = tickClient.getDailyBarsStocksData(symbol, companyId, daysBack);
	        			   info = "TickData Daily Bars Stocks Data";
	                       break;
	        case "futures": file = tickClient.getDailyBarsFuturesData(symbol, companyId, daysBack);
	        				info = "TickData Daily Bars Futures Data";
	        				break;
		}
		
        List<List<String>> listRecords = readCsv(file);
        List<List<String>> actualRecordList = getRecordList(listRecords);
        actualRecordList.forEach(actualRecord -> {
            dateTimeSeriesMap.put(parseDate(actualRecord.get(0)), getTimeseries(actualRecord));
        });
        file.deleteOnExit();
        TimeSeriesResult timeSeriesResult = new TimeSeriesResult();
        MetaData metaData = new MetaData();
        metaData.setSymbol(symbol);
        metaData.setInformation(info);
        timeSeriesResult.setMetaData(metaData);
        timeSeriesResult.setTimeSeries(dateTimeSeriesMap);
        return timeSeriesResult;
    }
    
	public TimeseriesDataResult getDailyBarsDataPoint(String type, String symbol, String companyId, String daysBack,
			UserEntity loggedinUser) throws IOException, MissingRequiredQueryParameterException {
		String dateDataType = "IndexDate";
		String dateFormat = "%Y/%m/%d";
		
		File file = null;
		
		switch(type) {
	        case "stocks": file = tickClient.getDailyBarsStocksData(symbol, companyId, daysBack);
	                       break;
	        case "futures": file = tickClient.getDailyBarsFuturesData(symbol, companyId, daysBack);
	        				break;
		}
        
        //Creating a file name with format <SYMBOL>-DAILY-<USER_ID>-<TIMESTAMP>.<datatype>
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        StringBuilder fileName = new StringBuilder();
        fileName.append(symbol).append("-").append("DAILY").append("-").append(type.toUpperCase()).append("-").append(daysBack).append("DAYS").append("-");
        fileName.append(loggedinUser.getUserId()).append("-").append(timeStamp).append(".csv");

        String savedFileName = fileStorageService.convertToCausaLensFormatSave(file, fileName.toString(), dateFormat, dateDataType);

        String fileDownloadUri = host+":"+port+"/garudafinance/api/file/download/" + savedFileName;

        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(loggedinUser.getUserId());
        userEntity.setPath(fileDownloadUri);
        userEntity.setFileName(savedFileName);
        userDetailsRepository.save(userEntity);

        return new TimeseriesDataResult(fileDownloadUri, savedFileName);
	}
	
    private List<List<String>> getRecordList(List<List<String>> listRecords) {
        AtomicReference<Integer> i = new AtomicReference<>(0);
        for (List<String> record : listRecords) {
            try {
                parseDate(record.get(0));
            } catch (RuntimeException ex) {
                i.getAndSet(i.get() + 1);
                continue;
            }
            return listRecords.subList(i.get(), listRecords.size());
        }
        return listRecords;
    }
	
    private TimeSeries getTimeseries(List<String> predictedVaule) {
        TimeSeries timeSeries = new TimeSeries();
        timeSeries.setOpen(Double.valueOf(predictedVaule.get(1)));
        timeSeries.setHigh(Double.valueOf(predictedVaule.get(2)));
        timeSeries.setLow(Double.valueOf(predictedVaule.get(3)));
        timeSeries.setClose(Double.valueOf(predictedVaule.get(4)));
        return timeSeries;
    }

    @Cacheable("tick-futures")
	public TickSearchFutureResult getFuturesSearhResult(String keyword) throws IOException, MissingRequiredQueryParameterException {
    	if(futuresResult == null) {
			futuresResult = tickClient.getTickFuturesList();
		}
    	
		TickSearchFutureResult futuresFilteredResult = new TickSearchFutureResult();
		
		List<TickFutures> filteredFuturesList = futuresResult.getFutures().stream().
				filter(f -> f.getDescription().toLowerCase().contains(keyword.toLowerCase())).collect(Collectors.toList());
		
		futuresFilteredResult.setFutures(filteredFuturesList);
		return futuresFilteredResult;
	}

//	public TimeSeriesResult getDailyBarsFuturesData(String symbol, String companyId, String daysBack) throws IOException, MissingRequiredQueryParameterException {
//		Map<ZonedDateTime, TimeSeries> dateTimeSeriesMap = new TreeMap<>();
//        File file = tickClient.getDailyBarsFuturesData(symbol, companyId, daysBack);
//        List<List<String>> listRecords = readCsv(file);
//        List<List<String>> actualRecordList = getRecordList(listRecords);
//        actualRecordList.forEach(actualRecord -> {
//            dateTimeSeriesMap.put(parseDate(actualRecord.get(0)), getTimeseries(actualRecord));
//        });
//        file.deleteOnExit();
//        TimeSeriesResult timeSeriesResult = new TimeSeriesResult();
//        MetaData metaData = new MetaData();
//        metaData.setSymbol(symbol);
//        metaData.setInformation("TickData Daily Bars Futures Data");
//        timeSeriesResult.setMetaData(metaData);
//        timeSeriesResult.setTimeSeries(dateTimeSeriesMap);
//        return timeSeriesResult;
//	}
	
	@PostConstruct
	private void fetchFuturesList() throws IOException, MissingRequiredQueryParameterException {
		futuresResult = tickClient.getTickFuturesList();
	}
}
