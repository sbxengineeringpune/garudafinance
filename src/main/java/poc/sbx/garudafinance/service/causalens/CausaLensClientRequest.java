package poc.sbx.garudafinance.service.causalens;

import lombok.Data;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CausaLensClientRequest {

    @NotNull(message = "csvLocation may not be null")
    @NotEmpty(message = "csvLocation may not be empty")
    private List<String> csvLocations;
    @NotNull(message = "targetColumn may not be null")
    @NotEmpty(message = "targetColumn may not be empty")
    private String targetColumn;
    @NotNull(message = "fileName may not be null")
    @NotEmpty(message = "fileName may not be empty")
    private List<String> fileNames;
    private int futureIndex;
}
