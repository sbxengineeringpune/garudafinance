package poc.sbx.garudafinance.service.causalens;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CausaLensRequest {

    @NotNull(message = "csvLocation may not be null")
    @NotEmpty(message = "csvLocation may not be empty")
    private String csvLocation;
    @NotNull(message = "targetColumn may not be null")
    @NotEmpty(message = "targetColumn may not be empty")
    private String targetColumn;
    @NotNull(message = "fileName may not be null")
    @NotEmpty(message = "fileName may not be empty")
    private String fileName;
    private int futureIndex;
}
