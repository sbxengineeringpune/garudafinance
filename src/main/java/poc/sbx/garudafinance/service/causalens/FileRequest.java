package poc.sbx.garudafinance.service.causalens;

import lombok.Data;

@Data
public class FileRequest {

    private String csvFilePath;
   
}
