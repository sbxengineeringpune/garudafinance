package poc.sbx.garudafinance.service.causalens;

import lombok.Data;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeriesResult;

@Data
public class CausaLensTimeSeriesResponse {

	private TimeSeriesResult actual;
	private TimeSeriesResult predictions;
	private String training;
	private String testing;
	private String validation;
	private String modelName;
	private String modelType;
	private Double modelScore;
}
