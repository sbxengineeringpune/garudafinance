package poc.sbx.garudafinance.service.causalens;

import java.util.List;

import lombok.Data;

@Data
public class MultiFileRequest {

    private List<String> csvFilePaths;
   
}
