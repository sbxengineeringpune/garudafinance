package poc.sbx.garudafinance.service;

import static poc.sbx.garudafinance.service.util.DateUtil.parseDate;
import static poc.sbx.garudafinance.service.util.FileHelper.readCsv;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;

import poc.sbx.garudafinance.service.alpha.response.MetaData;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeries;
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeriesResult;
import poc.sbx.garudafinance.service.causalens.CausaLensClientRequest;
import poc.sbx.garudafinance.service.causalens.CausaLensRequest;
import poc.sbx.garudafinance.service.causalens.CausaLensTimeSeriesResponse;
import poc.sbx.garudafinance.service.util.FileHelper;

@Service
public class CausalensService {

	@Autowired
	private RestTemplate restTemplate;
	
    @Value("${causalens.host}")
    private String causaLensHost;

    @Value("${causalens.port}")
    private String causaLensPort;

    @Value("${target.port}")
    private String port;

    @Value("${target.host}")
    private String host;

    private static final Logger logger = LoggerFactory.getLogger(CausalensService.class);

    @Autowired
    private FileStorageService fileStorageService;

    /**
     * Gets predicted data from causalens api. Merge actual data with prediction data and send the response.
     *
     * @return
     */
    public CausaLensTimeSeriesResponse getTimeSeriesPredictionFromCausalens(CausaLensClientRequest causaLensClientReq) {
        try {
        	
    		CausaLensRequest causaLensReq = new CausaLensRequest();
        	if(causaLensClientReq.getCsvLocations().size() > 1) {

                InputStream firstFileInputStream = new URL(causaLensClientReq.getCsvLocations().get(0)).openStream();
                InputStream secondFileInputStream = new URL(causaLensClientReq.getCsvLocations().get(1)).openStream();

                File firstFile = File.createTempFile("firstFile", ".csv");
                File secondFile = File.createTempFile("secondFile", ".csv");

                FileUtils.copyInputStreamToFile(firstFileInputStream, firstFile);
                FileUtils.copyInputStreamToFile(secondFileInputStream, secondFile);

                //merge two files, save new file in upload/causalens folder and sprepare the downloadable url for merged file and set it to causaLensReq object
                File file = fileStorageService.mergeTwoCsvFile(firstFile,secondFile,causaLensClientReq.getFileNames().get(0),causaLensClientReq.getFileNames().get(1));

                causaLensReq.setCsvLocation(host + ":" + port + "/garudafinance/api/file/download/" + file.getName()); //merged file downloadable url
        		causaLensReq.setFileName(file.getName()); //merged file name
        	}
        	else {
        		causaLensReq.setCsvLocation(causaLensClientReq.getCsvLocations().get(0));
        		causaLensReq.setFileName(causaLensClientReq.getFileNames().get(0));
        		
        	}
        	
        	causaLensReq.setTargetColumn(causaLensClientReq.getTargetColumn());
        	causaLensReq.setFutureIndex(causaLensClientReq.getFutureIndex());
        	
            ObjectMapper mapper = new ObjectMapper();
            MapType type = mapper.getTypeFactory().constructMapType(Map.class, String.class, Object.class);

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<CausaLensRequest> request = new HttpEntity<CausaLensRequest>(causaLensReq, headers);
            Map<String,Object> causaLenseResponse = mapper.readValue(restTemplate.postForEntity(causaLensHost+":"+causaLensPort+"/analysis/job/start",request,String.class).getBody(),type); //FIX: Fix the url here

            //TODO: when the python service is available uncomment the commented line and remove the below two lines
//            String content = FileHelper.readFile(AlphaVantageService.class.getResource("/causalensaresonse.json").getPath());
//            Map<String, Object> causaLenseResponse = mapper.readValue(content, type);

            CausaLensTimeSeriesResponse response = new CausaLensTimeSeriesResponse();

            Map<String, Object> model_params = causaLenseResponse.entrySet().parallelStream().filter(a -> a.getKey().equals("model_params")).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            Map<String, Object> model_param_value = (Map<String, Object>) model_params.get("model_params");
            Map<String, Object> model_results = causaLenseResponse.entrySet().parallelStream().filter(a -> a.getKey().equals("model_results")).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            Map<String, Object> model_result_value = (Map<String, Object>) model_results.get("model_results");
            Map<String, Object> predictions = model_result_value.entrySet().parallelStream().filter(e -> e.getKey().equals("predictions")).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            Map<String, Object> experimental_params = model_param_value.entrySet().parallelStream().filter(e -> e.getKey().equals("experimental_params")).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            Map<String, Object> experimental_params_value = (Map<String, Object>) experimental_params.get("experimental_params");
            List<Double> dataSplitArray = (List<Double>) experimental_params_value.get("data_split_by_amount");
            
            String modelName = (String) causaLenseResponse.get("model_name");
            String modelType = (String) causaLenseResponse.get("model_type");
            Double modelScore = (Double) model_result_value.get("optimization_score");
            try {
                if (dataSplitArray != null && !dataSplitArray.isEmpty()) {
                    response.setTraining(dataSplitArray.get(0).toString());
                    response.setValidation(dataSplitArray.get(1).toString());
                    response.setTesting(dataSplitArray.get(2).toString());
                }
            } catch (Exception e) {
                throw new RuntimeException("GadgetService.createTimeSeriesPredictionResult: Error in setting data split" + e.getMessage());
            }

            response.setActual(creatTimeSeriesResultFormActualRecords(causaLensReq.getCsvLocation(), causaLensReq.getFileName(), causaLensReq.getTargetColumn()));
            response.setPredictions(createTimeSeriesResultFromPredictionRecords(predictions, causaLensReq.getFileName(), causaLensReq.getTargetColumn()));
            response.setModelName(modelName);
            response.setModelType(modelType);
            response.setModelScore(modelScore);
            return response;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private TimeSeriesResult createTimeSeriesResultFromPredictionRecords(Map<String, Object> predictions, String fileName, String targetColumn) {
        Map<ZonedDateTime, TimeSeries> resultMap = new TreeMap<>();
        List<Map<String, Object>> prediction_value = (List<Map<String, Object>>) predictions.get("predictions");
        prediction_value.forEach(list -> {
            resultMap.put(parseDate(list.get("future_index").toString()),
                setCausaValue(list.get("predicted_value").toString()));
        });

        MetaData metaData = new MetaData();
        metaData.setInformation("CausaLens Timeseries Predictions");

        if (fileName != null && !fileName.isEmpty() && fileName.contains("-")) {
            String[] fileNameTokens = fileName.split("-");
            metaData.setSymbol(fileNameTokens[0]);
        }
        return new TimeSeriesResult(metaData, resultMap);
    }

    private TimeSeriesResult creatTimeSeriesResultFormActualRecords(String fileUrl, String fileName, String targetColumn) {
        Map<ZonedDateTime, TimeSeries> dateTimeSeriesMap = new TreeMap<>();
        InputStream inputStream = null;
        try {
            inputStream = new URL(fileUrl).openStream();
            File file = File.createTempFile("causalense", ".csv");
            FileUtils.copyInputStreamToFile(inputStream, file);
            List<List<String>> listRecords = readCsv(file);
            //First row from csv is always header row
            List<String> headerRecord = listRecords.get(0);
            
            
            Map<String,Integer> columnIndexMap = new HashMap<String, Integer>();
            int index = 0;
            for(String column: headerRecord) {
            	columnIndexMap.put(column, index);
            	index++;
            }
            List<List<String>> dataRecordList = getRecordList(listRecords);
            dataRecordList.forEach(dataRecord -> {
            	TimeSeries series = setCausaValue(targetColumn, dataRecord, columnIndexMap);
            	if(series.getCausaTargetValue() != 0.0) {
            		dateTimeSeriesMap.put(parseDate(dataRecord.get(0)), series);
            	}
                
            });
            file.deleteOnExit();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        MetaData metaData = new MetaData();
        metaData.setSymbol(fileName.split("-")[0]);
        metaData.setInformation("Causalense request timeseries data");
        return new TimeSeriesResult(metaData, dateTimeSeriesMap);
    }
    
    private List<List<String>> getRecordList(List<List<String>> listRecords) {
        AtomicReference<Integer> i = new AtomicReference<>(0);
        for (List<String> record : listRecords) {
            try {
                parseDate(record.get(0));
            } catch (RuntimeException ex) {
                i.getAndSet(i.get() + 1);
                continue;
            }
            return listRecords.subList(i.get(), listRecords.size());
        }
        return listRecords;
    }


	private TimeSeries setCausaValue(String targetColumn, List<String> predictedVaule, Map<String,Integer> columnIndexMap) {
        TimeSeries timeSeries = new TimeSeries();
        Double value = predictedVaule.size() > columnIndexMap.get(targetColumn) && predictedVaule.get(columnIndexMap.get(targetColumn)) != null &&
            !predictedVaule.get(columnIndexMap.get(targetColumn)).isEmpty() ? Double.valueOf(predictedVaule.get(columnIndexMap.get(targetColumn))): 0.0;

        timeSeries.setCausaTargetValue(value);
        return timeSeries;
    }

    private TimeSeries setCausaValue(String predictedVaule) {
        TimeSeries timeSeries = new TimeSeries();
        Double value = predictedVaule != null && !predictedVaule.isEmpty() ? Double.valueOf(predictedVaule) : 0.0;
        timeSeries.setCausaTargetValue(value);
        return timeSeries;
    }
}
