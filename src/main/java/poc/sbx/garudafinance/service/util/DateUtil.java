package poc.sbx.garudafinance.service.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DateUtil {
    public static ZonedDateTime parseDate(String strDate) {
        if (strDate != null && !strDate.isEmpty()){
            SimpleDateFormat[] formats =
                new SimpleDateFormat[] {new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH), new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH),
                    new SimpleDateFormat("yyyy/MM/dd")};
            Date parsedDate = null;
            for (SimpleDateFormat format : formats) {
                try {
                    parsedDate = format.parse(strDate);
                    return parseDateToZonedDateTime(parsedDate);
                } catch (ParseException e){
                    continue;
                }
            }
        }
        throw new RuntimeException("Unknown date format: '" + strDate + "'");
    }

    public static  Date parseDateInLocaldateandTime(String dateStr){
        Date parsedDate = null;
        List<DateTimeFormatter> formates = new ArrayList<>();
        formates.add(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        for (DateTimeFormatter formate : formates) {

                LocalDate dateTime = LocalDate.parse(dateStr, formate);
            parsedDate = Date.from(dateTime.atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
     return parsedDate;
    }
    
    /**
     * converts Date to ZonedDateTime. Resolves TimeZone issue.
     * @param date
     * @return
     */
    public static ZonedDateTime parseDateToZonedDateTime(Date date) {
    	ZoneId defaultZoneId = ZoneId.systemDefault();
        Instant instant = date.toInstant();
        ZonedDateTime zonedDateTime = instant.atZone(defaultZoneId);
        return zonedDateTime;
    }
    
    public static Date parseDateFromString(String strDate) {
        if (strDate != null && !strDate.isEmpty()){
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
            Date parsedDate = null;
                try {
                    parsedDate = format.parse(strDate);
                    return parsedDate;
                } catch (ParseException e){
                    
                }
        }
        throw new RuntimeException("Unknown date format: '" + strDate + "'");
    }


    public static Boolean comapareDate(String firstDate, String secondDate) {
        try {
            ZonedDateTime date1 = parseDate(firstDate);
            ZonedDateTime date2 = parseDate(secondDate);
            return date1.equals(date2);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }



}
