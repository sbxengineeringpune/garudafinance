package poc.sbx.garudafinance.service.util;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import poc.sbx.garudafinance.config.FileStorageProperties;
import poc.sbx.garudafinance.exception.FileStorageException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import static poc.sbx.garudafinance.service.util.DateUtil.comapareDate;
import static poc.sbx.garudafinance.service.util.DateUtil.parseDate;

public class FileHelper {


    private final Path fileStorageLocation;

    @Autowired
    public FileHelper(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
            .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public static String readFile(String filename) {
        BufferedReader br = null;
        FileReader fr = null;
        StringBuilder content = new StringBuilder();

        try {
            fr = new FileReader(filename);
            br = new BufferedReader(fr);

            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                content.append(sCurrentLine);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        return content.toString();
    }


    public static List<List<String>> readCsv(File file) {
        List<List<String>> records = new ArrayList<>();
        try (Scanner scanner = new Scanner(file);) {
            while (scanner.hasNextLine()) {
                records.add(getRecordFromLine(scanner.nextLine()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return records;
    }

    private static List<String> getRecordFromLine(String line) {
        List<String> values = new ArrayList<String>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(",");
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        return values;
    }

    public static File getInputStreamToFile(InputStream inputStream) {
        File file = null;
        try {
            file = File.createTempFile("tempFile", ".csv");
            FileUtils.copyInputStreamToFile(inputStream, file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }


    public static File updateCsv(File file) {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(file), ',');
            List<String[]> csvBody = reader.readAll();
            List<String[]> newCsvBody = new ArrayList<>();
            for (String[] strArray : csvBody) {
                int k = 0;
                String[] newColumn = new String[7];
                for (int j = 0; j < strArray.length; j++) {
                    if (j < 5 || j > 8) {
                        newColumn[k++] = checkAndmodifyColumnName(strArray[j]);
                    }
                }
                newCsvBody.add(newColumn);
            }
            reader.close();

            CSVWriter writer = new CSVWriter(new FileWriter(file), ',');
            writer.writeAll(newCsvBody);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    private static String checkAndmodifyColumnName(String columnName) {
        switch (columnName) {
            case "open (USD)":
                columnName = "open";
                break;
            case "high (USD)":
                columnName = "high";
                break;
            case "low (USD)":
                columnName = "low";
                break;
            case "close (USD)":
                columnName = "close";
                break;
            case "market cap (USD)":
                columnName = "market cap";
                break;
            default:
                break;
        }

        return columnName;
    }





    public static List<List<String>> getMergedRecordsForTwoCsv(List<List<String>> firstFileRecords, List<List<String>> secondFileRecods) {
        List<List<String>> firstFileRecordswithoutHeaders = firstFileRecords.subList(3, firstFileRecords.size());
        List<List<String>> secondFileRecordswithoutHeaders = secondFileRecods.subList(3, secondFileRecods.size());
        Integer columnCount = getNumberOfCulumsForMergeRecord(firstFileRecordswithoutHeaders,secondFileRecordswithoutHeaders);

        List<List<String>> newRecods = new ArrayList<>();
        List<List<String>> firstFileRecodsRemove = new ArrayList<>();
        List<List<String>> secondFileRecodsRemove = new ArrayList<>();


        firstFileRecordswithoutHeaders.forEach(firstFileRecord -> {
            secondFileRecordswithoutHeaders.forEach(secondFileRecod -> {
                if (comapareDate(firstFileRecord.get(0), secondFileRecod.get(0))) {
                    newRecods.add(Stream.concat(firstFileRecord.stream(), secondFileRecod.subList(1, secondFileRecod.size()).stream())
                        .collect(Collectors.toList()));
                    firstFileRecodsRemove.add(firstFileRecord);
                    secondFileRecodsRemove.add(secondFileRecod);
                }
            });
        });

        firstFileRecordswithoutHeaders.removeAll(firstFileRecodsRemove);
        secondFileRecordswithoutHeaders.removeAll(secondFileRecodsRemove);

        ArrayList<String> comma = new ArrayList<String>();

        List<List<String>> remaingRecordsList = new ArrayList<>();

        if (firstFileRecordswithoutHeaders.size() > 0) {
            firstFileRecordswithoutHeaders.forEach(record -> {

                IntStream.range(record.size(),columnCount)
                    .forEach(idx ->{
                        comma.add("");
                    });

                remaingRecordsList.add(Stream.concat(record.stream(), comma.stream())
                    .collect(Collectors.toList()));
                comma.clear();
            });
        }
        if (secondFileRecordswithoutHeaders.size() > 0) {
            secondFileRecordswithoutHeaders.forEach(record -> {
                IntStream.range(record.size(),columnCount)
                    .forEach(idx ->{
                        comma.add("");
                    });

                remaingRecordsList.add(Stream.concat(comma.stream(), record.stream()).collect(Collectors.toList()));
                comma.clear();
            });
        }
        newRecods.addAll(remaingRecordsList);
        return newRecods;
    }

    public static File csvWritter(List<List<String>> newCsvBody, String filePath) {
        List<String[]> csvBody = new ArrayList<>();

        newCsvBody.forEach(body -> {
            csvBody.add(body.stream().toArray(String[]::new));
        });

        File file = null;
        try {
            file = new File(filePath);
            CSVWriter writer = new CSVWriter(new FileWriter(file),',',CSVWriter.NO_QUOTE_CHARACTER);
            writer.writeAll(csvBody);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return file;
    }

    public static List<List<String>>  createCsvHeader(Boolean isFutureFirst, List<String> firstFileHeader, List<String> secondFileHeader) {
        List<String> headerList = new ArrayList<>();
        headerList.add("\"");
        List<String> firstFile = new ArrayList<>();
        List<String> secondFile = new ArrayList<>();

        List<List<String>> allHeaders = new ArrayList<>();

        if (isFutureFirst ) {
            firstFileHeader.subList(1, firstFileHeader.size()).forEach( header ->{
                firstFile.add(header+"_futures");
            });

            headerList.addAll(firstFile);
            headerList.addAll(secondFileHeader.subList(1, secondFileHeader.size()));
        }else {
            headerList.addAll(firstFileHeader.subList(1, firstFileHeader.size()));
            secondFileHeader.subList(1, secondFileHeader.size()).forEach( header ->{
                secondFile.add(header+"_futures");
            });
            headerList.addAll(secondFile);
        }

        allHeaders.add(headerList);
        
        List<String> staticHeader = new ArrayList<>();
        staticHeader.add("IndexDate");
        List<String> dateHeader = new ArrayList<>();
        dateHeader.add("dateformat=%Y-%m-%d");
         for(int i =1; i<headerList.size(); i++ ){
             staticHeader.add("ValuesNumerical");
             dateHeader.add("");
         }
        allHeaders.add(staticHeader);
        
        allHeaders.add(dateHeader);

        return allHeaders;

    }

    private static Integer getNumberOfCulumsForMergeRecord(List<List<String>> firstFileRecord, List<List<String>> secondFileRecord){
        List<String> firstFile = firstFileRecord.get(0);
        List<String> secondFile = secondFileRecord.get(0);
        AtomicReference<Integer> column = new AtomicReference<>(0);
        firstFile.forEach(record -> {
            try{
                parseDate(record);
            }catch (Exception e){
                column.getAndSet(column.get() + 1);
            }

        });

        secondFile.forEach(record -> {
            try{
                parseDate(record);
            }catch (Exception e){
                column.getAndSet(column.get() + 1);
            }

        });
        return column.get()+1;
    }
}
