package poc.sbx.garudafinance.service.alpha.timeseries;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import poc.sbx.garudafinance.service.alpha.response.MetaData;

/**
 * A representation of the API response for the TimeSeries endpoints.
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TimeSeriesResult {
  private MetaData metaData;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private Map<ZonedDateTime, TimeSeries> timeSeries;


}
