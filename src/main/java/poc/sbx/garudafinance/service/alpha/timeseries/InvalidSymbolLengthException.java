package poc.sbx.garudafinance.service.alpha.timeseries;

public class InvalidSymbolLengthException extends Exception {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public InvalidSymbolLengthException(int size) {
    super(size + " is an invalid length of symbols. Must be between 1-100. " +
          "Request could not be sent.");
  }

}
