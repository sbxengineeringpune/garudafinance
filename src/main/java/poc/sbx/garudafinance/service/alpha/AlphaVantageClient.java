package poc.sbx.garudafinance.service.alpha;

import poc.sbx.garudafinance.config.alpha.AlphaVantageClientConfiguration;
import poc.sbx.garudafinance.config.alpha.DataType;
import poc.sbx.garudafinance.config.alpha.IAlphaVantageClient;
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval;
import poc.sbx.garudafinance.service.alpha.request.OutputSize;
import poc.sbx.garudafinance.service.alpha.timeseries.*;
import poc.sbx.garudafinance.web.rest.util.alpha.JsonParser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class AlphaVantageClient implements IAlphaVantageClient {

    private AlphaVantageClientConfiguration configuration;

    public AlphaVantageClient(AlphaVantageClientConfiguration configuration) {
        this.configuration = configuration;
        JsonParser.addDeserializer(TimeSeriesResult.class, new TimeSeriesResultDeserializer());
        JsonParser.addDeserializer(SymbolResult.class, new SymbolResultDeserializer());
        JsonParser.addDeserializer(CurrencyExchangeResult.class, new CurrencyExchangeResultDeserializer());
        JsonParser.addDeserializer(ForeignExchangeResult.class, new ForeignExchangeResultDeserializer());
        JsonParser.addDeserializer(CryptoCurrencyTimeSeriesResult.class, new CryptoCurrencyTimeSeriesResultDeserializer());
    }

    @Override
    public TimeSeriesResult getTimeSeries(IntradayInterval intradayInterval, String symbol) throws IOException, MissingRequiredQueryParameterException {
        return getTimeSeries(intradayInterval, symbol, OutputSize.COMPACT);
    }

    @Override
    public TimeSeriesResult getTimeSeries(IntradayInterval intradayInterval, String symbol, OutputSize outputSize)
        throws IOException, MissingRequiredQueryParameterException {
        String queryParameters = TimeSeriesRequest.builder().timeSeriesFunction(TimeSeriesFunction.INTRADAY)
            .intradayInterval(intradayInterval).symbol(symbol).outputSize(outputSize).build().toQueryParameters();

        return sendAPIRequest(queryParameters, TimeSeriesResult.class);
    }

    @Override
    public TimeSeriesResult getTimeSeries(TimeSeriesFunction timeSeriesFunction, String symbol)
        throws IOException, MissingRequiredQueryParameterException {
        return getTimeSeries(timeSeriesFunction, symbol, OutputSize.COMPACT);
    }

    @Override
    public TimeSeriesResult getTimeSeries(TimeSeriesFunction timeSeriesFunction, String symbol, OutputSize outputSize)
        throws IOException, MissingRequiredQueryParameterException {
        String queryParameters = TimeSeriesRequest.builder().timeSeriesFunction(timeSeriesFunction).symbol(symbol)
            .outputSize(outputSize).build().toQueryParameters();

        return sendAPIRequest(queryParameters, TimeSeriesResult.class);
    }

    @Override
    public SymbolResult getSymbolList(String keyword) throws IOException, MissingRequiredQueryParameterException {
        String queryParameters = SearchSymbolRequest.builder().keyword(keyword).build().toQueryParameters();
        return sendAPIRequest(queryParameters, SymbolResult.class);
    }

    //  @Override
//  public BatchQuoteResult getBatchQuote(String... symbols)
//      throws MissingRequiredQueryParameterException,
//      InvalidSymbolLengthException, IOException {
//    String queryParameters = BatchQuoteRequest.builder()
//        .symbols(symbols)
//        .build()
//        .toQueryParameters();
//    return sendAPIRequest(queryParameters, BatchQuoteResult.class);
//  }
//
    @Override
    public CurrencyExchange getCurrencyExchange(String fromCurrency, String toCurrency)
        throws MissingRequiredQueryParameterException, IOException {
        String queryParameters = CurrencyExchangeRequest.builder().fromCurrency(fromCurrency).toCurrency(toCurrency)
            .build().toQueryParameters();
        return sendAPIRequest(queryParameters, CurrencyExchangeResult.class).getQuote();
    }

//  public SectorResult getSectorPerformances()
//      throws IOException {
//    String queryParameters = "function=SECTOR";
//    return sendAPIRequest(queryParameters, SectorResult.class);
//  }

    public ForeignExchangeResult getForeignExchange(ForeignExchangeFunction function, String fromCurrency, String toCurrency)
        throws MissingRequiredQueryParameterException, IOException {
        String queryParameters = ForeignExchangeRequest.builder().function(function).fromCurrency(fromCurrency)
            .toCurrency(toCurrency).build().toQueryParameters();
        return sendAPIRequest(queryParameters, ForeignExchangeResult.class);
    }

    /**
     * Append the API Key and the DataType to the query parameters and send the API
     * request to Alpha Vantage.
     *
     * @param queryParameters The query parameter string from the Request.
     * @param resultObject    The expected result object from the API.
     * @return The Result of the API request.
     */
    private <T> T sendAPIRequest(String queryParameters, Class<T> resultObject) throws IOException {
        queryParameters += "&datatype=" + DataType.JSON;
        queryParameters += "&apikey=" + configuration.getApiKey();
        return JsonParser.toObject(Request.sendRequest(queryParameters), resultObject);
    }

    private File sendAPIFileRequest(String queryParameters, DataType dataType)
        throws IOException {
        queryParameters += "&datatype=" + dataType;
        queryParameters += "&apikey=" + configuration.getApiKey();
        return Request.sendFileRequest(queryParameters);
    }

    @Override
    public ForeignExchangeResult getForeignExchange(ForeignExchangeFunction function, String fromCurrency, String toCurrency, IntradayInterval interval)
        throws MissingRequiredQueryParameterException, IOException {

        String queryParameters = ForeignExchangeRequest.builder().function(function).intradayInterval(interval)
            .fromCurrency(fromCurrency).toCurrency(toCurrency).build().toQueryParameters();
        return sendAPIRequest(queryParameters, ForeignExchangeResult.class);
    }

    @Override
    public File getTimeSeriesDataPoint(IntradayInterval interval, String symbol, OutputSize outputSize, DataType dataType
    ) throws IOException, MissingRequiredQueryParameterException {
        String queryParameters = TimeSeriesRequest.builder().timeSeriesFunction(TimeSeriesFunction.INTRADAY)
            .intradayInterval(interval).symbol(symbol).outputSize(outputSize).build().toQueryParameters();
        return sendAPIFileRequest(queryParameters, dataType);
    }


    @Override
    public File getTimeSeriesDataPoint(TimeSeriesFunction timeSeriesFunction, String symbol, DataType dataType)
        throws IOException, MissingRequiredQueryParameterException {
        return getTimeSeriesDataPoint(timeSeriesFunction, symbol, OutputSize.COMPACT, dataType);
    }

    @Override
    public File getTimeSeriesDataPoint(TimeSeriesFunction timeSeriesFunction, String symbol, OutputSize outputSize, DataType dataType)
        throws IOException, MissingRequiredQueryParameterException {
        String queryParameters = TimeSeriesRequest.builder().timeSeriesFunction(timeSeriesFunction).symbol(symbol)
            .outputSize(outputSize).build().toQueryParameters();
        return sendAPIFileRequest(queryParameters, dataType);
    }

	@Override
	public File getForexSeriesDataPoint(IntradayInterval interval, String fromCurrency, String toCurrency,
			OutputSize outputSize, DataType dataType) throws IOException, MissingRequiredQueryParameterException {
		String queryParameters = ForeignExchangeRequest.builder().function(ForeignExchangeFunction.INTRADAY).intradayInterval(interval)
	            .fromCurrency(fromCurrency).toCurrency(toCurrency).outputSize(outputSize).build().toQueryParameters();
	        return sendAPIFileRequest(queryParameters, dataType);
	}

	@Override
	public File getForexSeriesDataPoint(ForeignExchangeFunction function, String fromCurrency, String toCurrency,
			DataType dataType) throws IOException, MissingRequiredQueryParameterException {
		return getForexSeriesDataPoint(function, fromCurrency, toCurrency, OutputSize.COMPACT, dataType);
	}

	@Override
	public File getForexSeriesDataPoint(ForeignExchangeFunction function, String fromCurrency, String toCurrency,
			OutputSize outputSize, DataType dataType) throws IOException, MissingRequiredQueryParameterException {
		String queryParameters = ForeignExchangeRequest.builder().function(function)
	            .fromCurrency(fromCurrency).toCurrency(toCurrency).outputSize(outputSize).build().toQueryParameters();
	        return sendAPIFileRequest(queryParameters, dataType);
	}

    @Override
    public CryptoCurrencyTimeSeriesResult getCrtproCurrencyTimeSeriesData(CryptocurrencyFunction function, String symbol) throws IOException, MissingRequiredQueryParameterException, InvalidSymbolLengthException {
        String queryParameter = CryptoCurrencyRequest.builder().function(function).symbol(symbol).build().toQueryParameters();
        return sendAPIRequest(queryParameter,CryptoCurrencyTimeSeriesResult.class);
    }

	@Override
	public File getCryptoDataPoint(CryptocurrencyFunction function, String symbol, DataType dataType) throws MissingRequiredQueryParameterException, InvalidSymbolLengthException, IOException {
		String queryParameter = CryptoCurrencyRequest.builder().function(function).symbol(symbol).build().toQueryParameters();
		return sendAPIFileRequest(queryParameter, dataType);
	}

}
