package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.Data;

@Data
public class CsvTargetEntity {

	String TargetValue1;
	String TargetValue2;
	String TargetValue3;
	String TargetValue4;
	String TargetValue5;

}
