package poc.sbx.garudafinance.service.alpha;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import poc.sbx.garudafinance.service.alpha.response.ErrorResponse;
import poc.sbx.garudafinance.web.rest.util.alpha.JsonParser;

@Service
public class Request {

	/**
	 * This method performs the HTTP call to the Alpha Vantage API.
	 * 
	 * @param queryParameters Perform the API call with the given query parameters.
	 * @return The API response in JSON.
	 */
	static String sendRequest(String queryParameters) {

		ResponseEntity<String> responseEntity = new RestTemplate().getForEntity(ALPHA_VANTAGE_URL + queryParameters,
				String.class);
		try {
			// Check if the response was an error response first.
			ErrorResponse errorResponse = JsonParser.toObject(responseEntity.getBody(), ErrorResponse.class);
			errorResponse.setQueryParameters(queryParameters);
			throw new HttpServerErrorException(responseEntity.getStatusCode(), errorResponse.toString(),
					responseEntity.getHeaders(), responseEntity.getBody().getBytes(), Charset.defaultCharset());
		} catch (IOException e) {
			// ignore, this means the request was successful.
		}
		return responseEntity.getBody();
	}

	/**
	 * This method performs the HTTP call to the Alpha Vantage API.
	 * 
	 * @param queryParameters Perform the API call with the given query parameters.
	 * @return The API response in CSV file. Converts into causalensformat
	 * @throws IOException 
	 */

	static  File sendFileRequest(String queryParameters)
			throws IOException {
				
		File file = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<byte[]> response = restTemplate.getForEntity(ALPHA_VANTAGE_URL + queryParameters, byte[].class);
            file = File.createTempFile("TimeSeriesData", ".csv");
            FileUtils.writeByteArrayToFile(file, response.getBody() == null ? new byte[]{}: response.getBody());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return file;
	}

	private final static String ALPHA_VANTAGE_URL = "https://www.alphavantage.co/query?";
}
