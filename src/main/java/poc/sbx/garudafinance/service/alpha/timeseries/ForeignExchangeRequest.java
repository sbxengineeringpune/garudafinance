package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.Builder;
import lombok.Data;
import poc.sbx.garudafinance.service.alpha.request.APIRequest;
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval;
import poc.sbx.garudafinance.service.alpha.request.OutputSize;
/**
 * A wrapper class for the available query parameters for the Forex
 * endpoints of the API.
 */

@Data
@Builder
public class ForeignExchangeRequest implements APIRequest {

  @Override
  public String toQueryParameters()
      throws MissingRequiredQueryParameterException {
    if (function == null)
      throw new MissingRequiredQueryParameterException("ForeignExchangeFunction");
    if (fromCurrency == null)
      throw new MissingRequiredQueryParameterException("FromCurrency");
    if (toCurrency == null)
      throw new MissingRequiredQueryParameterException("ToCurrency");
    if (function == ForeignExchangeFunction.INTRADAY
        && intradayInterval == null)
      throw new MissingRequiredQueryParameterException(
          "IntradayInterval", "FX_INTRADAY"
      );

    StringBuilder builder = new StringBuilder();
    builder
        .append("function=")
        .append(function.getQueryParameterKey());
    builder
        .append("&from_symbol=")
        .append(fromCurrency);
    builder
        .append("&to_symbol=")
        .append(toCurrency);
    if (outputSize != null) {
      builder
          .append("&outputsize=")
          .append(outputSize.getQueryParameterKey());
    }
    if (intradayInterval != null) {
      builder
          .append("&interval=")
          .append(intradayInterval.getQueryParameterKey());
    }
    return builder.toString();
  }

  private ForeignExchangeFunction function;
  private String fromCurrency;
  private String toCurrency;
  private IntradayInterval intradayInterval;
  private OutputSize outputSize;

}