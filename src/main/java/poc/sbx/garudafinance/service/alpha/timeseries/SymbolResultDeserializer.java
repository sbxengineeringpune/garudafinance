package poc.sbx.garudafinance.service.alpha.timeseries;

import static poc.sbx.garudafinance.web.rest.util.alpha.AlphaVantageResultDeserializerHelper.sanitizeNodeKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import poc.sbx.garudafinance.web.rest.util.alpha.JsonParser;

public class SymbolResultDeserializer extends JsonDeserializer<SymbolResult> {

	@Override
	  public SymbolResult deserialize(
	      com.fasterxml.jackson.core.JsonParser parser, DeserializationContext context)
	      throws IOException {
		SymbolResult symbolResult = new SymbolResult();
	    ObjectCodec oc = parser.getCodec();
	    JsonNode node = oc.readTree(parser);
	    try {
	    	symbolResult.setBestMatches(getSymbolList(node));
	    } catch (Throwable throwable) {
	      System.out.println("Error when deserializing:");
	      System.out.println(node.toString());
	      throwable.printStackTrace();
	    }
	    return symbolResult;
	  }
	
	 public static List<Symbol> getSymbolList(JsonNode node)
		      throws IOException {
		    List<Symbol> symbolList = new ArrayList<>();
		    node.fields().forEachRemaining(nodeEntry -> {
		    	if (nodeEntry.getKey().equals(SymbolResult.BEST_MATCHES_RESPONSE_KEY)) {
		    		nodeEntry.getValue().forEach(symbolEntry -> {
		    			try {
							symbolList.add(JsonParser.toObject(
							        JsonParser.toJson(sanitizeNodeKeys(symbolEntry)),
							        Symbol.class
							    ));
						} catch (IOException e) {
				            e.printStackTrace();
				          }
		    		});
		    	}
		    });
		    
		    return symbolList;
		  }
}
