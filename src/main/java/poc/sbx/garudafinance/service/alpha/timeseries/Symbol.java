package poc.sbx.garudafinance.service.alpha.timeseries;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Optional;

/**
 * Representation of the data that is returned by the Symbol search endpoints
 * in the API.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Symbol {

  private String symbol;
  private String name;
  private String type;
  private String region;
  private String marketOpen;
  private String marketClose;
  private String timezone;
  private String currency;
  private String matchScore;
  
}
