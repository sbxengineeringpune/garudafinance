package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.Data;

@Data
public class SessionEntity {

	String username;
	String path;
}
