package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.Builder;
import poc.sbx.garudafinance.service.alpha.request.APIRequest;
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval;
import poc.sbx.garudafinance.service.alpha.request.OutputSize;

/**
 * A wrapper class for the available query parameters for the Symbol search
 * endpoints of the API.
 */
@Builder
public class SearchSymbolRequest implements APIRequest {

  /**
   * Convert all the selected query parameters to a query parameter string
   * to be used the in the API request.
   * @return A Query parameter string.
   */
  public String toQueryParameters()
      throws MissingRequiredQueryParameterException {
    
	  if (keyword == null || keyword.isEmpty())
	      throw new MissingRequiredQueryParameterException("Keyword");

    StringBuilder builder = new StringBuilder();
    builder
        .append("function=")
        .append("SYMBOL_SEARCH");
    builder
        .append("&keywords=")
        .append(keyword);
    
    return builder.toString();
  }
  
  private String keyword;
}
