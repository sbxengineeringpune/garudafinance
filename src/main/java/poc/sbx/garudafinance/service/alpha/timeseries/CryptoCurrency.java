package poc.sbx.garudafinance.service.alpha.timeseries;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CryptoCurrency {

	private String currencyCode;
	private String currencyName;
}
