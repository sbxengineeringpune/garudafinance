package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimeseriesDataResult {

	private String url;
	private String fileName;

}
