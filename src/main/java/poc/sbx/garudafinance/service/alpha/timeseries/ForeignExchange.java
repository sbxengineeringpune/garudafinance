package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.Data;

/**
 * Representation of the data that is returned by the Foreign Exchange  endpoints
 * in the API.
 */
@Data
public class ForeignExchange {

  private double open;
  private double high;
  private double low;
  private double close;

}