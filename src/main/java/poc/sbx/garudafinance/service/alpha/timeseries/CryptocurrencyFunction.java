package poc.sbx.garudafinance.service.alpha.timeseries;

public enum CryptocurrencyFunction {

    DAILY("DIGITAL_CURRENCY_DAILY"),
    WEEKLY("DIGITAL_CURRENCY_WEEKLY"),
    MONTHLY("DIGITAL_CURRENCY_MONTHLY");

    CryptocurrencyFunction(String queryParameterKey) {
        this.queryParameterKey = queryParameterKey;
    }

    public String getQueryParameterKey() {
        return queryParameterKey;
    }

    private String queryParameterKey;
}
