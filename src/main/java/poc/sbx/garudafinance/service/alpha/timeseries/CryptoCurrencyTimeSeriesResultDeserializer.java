package poc.sbx.garudafinance.service.alpha.timeseries;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import poc.sbx.garudafinance.service.alpha.response.MetaData;
import poc.sbx.garudafinance.service.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static poc.sbx.garudafinance.web.rest.util.alpha.AlphaVantageResultDeserializerHelper.*;

public class CryptoCurrencyTimeSeriesResultDeserializer extends JsonDeserializer<CryptoCurrencyTimeSeriesResult> {

    private static final SimpleDateFormat DATE_TIME_PARSER = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
    private static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public CryptoCurrencyTimeSeriesResult deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

        CryptoCurrencyTimeSeriesResult cryptoCurrencyTimeSeriesResult = new CryptoCurrencyTimeSeriesResult();
        ObjectCodec oc = parser.getCodec();
        JsonNode node = oc.readTree(parser);
        try {
            cryptoCurrencyTimeSeriesResult.setMetaData(getMetaData(node));
            cryptoCurrencyTimeSeriesResult.setTimeSeries(reduceMapSize(getDateObjectMap(node)));
        } catch (Throwable throwable) {
            System.out.println("Error when deserializing:");
            System.out.println(node.toString());
            throwable.printStackTrace();
        }
        return cryptoCurrencyTimeSeriesResult;
    }

    private Map<ZonedDateTime, TimeSeries> getDateObjectMap(JsonNode node) {
        Map<ZonedDateTime, TimeSeries> timeSeriesMap = new TreeMap<>(Collections.reverseOrder());
        node.fields().forEachRemaining(nodeEntry -> {
            // ignore meta data, we want the time series data
            if (!nodeEntry.getKey().equals(MetaData.META_DATA_RESPONSE_KEY)) {
                nodeEntry.getValue().fields().forEachRemaining(timeSeriesEntry -> {
                    try {
                        timeSeriesMap.put(
                            parseDate(timeSeriesEntry.getKey()),
                            poc.sbx.garudafinance.web.rest.util.alpha.JsonParser.toObject(
                                poc.sbx.garudafinance.web.rest.util.alpha.JsonParser.toJson(sanitizeNodeKeysForCryptoCurrency(timeSeriesEntry.getValue())),
                                TimeSeries.class
                            )
                        );
                    }
                    catch (IOException | ParseException e) {
                        e.printStackTrace();
                    }
                });
            }
        });
        return timeSeriesMap;
    }



    private ZonedDateTime parseDate(String dateStr)
        throws ParseException {
        Date date = DATE_PARSER.parse(dateStr);
        if (dateStr.length() > 10)
            date = DATE_TIME_PARSER.parse(dateStr);

        return DateUtil.parseDateToZonedDateTime(date);
    }
    
    private Map<ZonedDateTime, TimeSeries>  reduceMapSize(Map<ZonedDateTime, TimeSeries> map){
        if(map.size() > 100){
           return map.entrySet().stream().limit(100).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }else {
            return map;
        }
    }

}
