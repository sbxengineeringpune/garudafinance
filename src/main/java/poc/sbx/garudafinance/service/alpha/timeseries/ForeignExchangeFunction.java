package poc.sbx.garudafinance.service.alpha.timeseries;

/**
 * Available parameters for the query parameter function in the Forex endpoints.
 */
public enum ForeignExchangeFunction {

	INTRADAY("FX_INTRADAY"), 
	DAILY("FX_DAILY"), 
	WEEKLY("FX_WEEKLY"), 
	MONTHLY("FX_MONTHLY");

	ForeignExchangeFunction(String queryParameterKey) {
		this.queryParameterKey = queryParameterKey;
	}

	public String getQueryParameterKey() {
		return queryParameterKey;
	}

	private String queryParameterKey;

}