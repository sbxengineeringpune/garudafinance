package poc.sbx.garudafinance.service.alpha.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Query Parameter outputsize for the API.
 */
public enum OutputSize {
  
  @JsonProperty("Compact") COMPACT("compact"), 
  @JsonProperty("Full")FULL("full");

	OutputSize(String queryParameterKey) {
		this.queryParameterKey = queryParameterKey;
	}

	public String getQueryParameterKey() {
		return queryParameterKey;
	}

	private String queryParameterKey;
}
