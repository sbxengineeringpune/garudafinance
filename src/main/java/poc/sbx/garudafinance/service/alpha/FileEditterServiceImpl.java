package poc.sbx.garudafinance.service.alpha;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import poc.sbx.garudafinance.config.alpha.FileEditterService;
import poc.sbx.garudafinance.repository.UserDetailsRepository;
import poc.sbx.garudafinance.service.FileStorageService;
import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity;

@Service
public class FileEditterServiceImpl implements FileEditterService {
	
	@Autowired
	private UserDetailsRepository userDetailsRepository;
	
    @Autowired
    private FileStorageService fileStorageService;
    
    @Autowired
    private HttpServletRequest request;
    
    private static final Logger logger = LoggerFactory.getLogger(FileEditterService.class);

	@Override
	public List<String> getTargets(String csvFilePath) {
		List<String> list = new ArrayList<String>();
		try(InputStream input = new URL(csvFilePath).openStream();
			BufferedReader bfReader = new BufferedReader(new InputStreamReader(input));
				CSVParser csvParser = new CSVParser(bfReader, CSVFormat.DEFAULT);
				) {
			
			for (CSVRecord csvRecord : csvParser) {
				csvRecord.forEach(column -> list.add(column));
				break;
			}
			
            if(list.size() > 0)    
                list.remove(0);
            
		} catch (IOException e) {
			e.printStackTrace();
		}

		return list;
	}

	@Override
	public List<UserEntity> getFileList(Long loggedInUser) {
		List<UserEntity> userList = userDetailsRepository.findAllByUserId(loggedInUser);
		return userList;
	}

	@Override
    /**
     * service to download the csv file.
     *
     * @param fileName
     * @return
     */
    public ResponseEntity<Resource> getFile(String fileName) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(contentType))
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
            .body(resource);
    }

	@Override
	public List<String> getTargets(List<String> csvFilePaths) {
		List<String> finalTargetColumns = new ArrayList<String>();
		
		for(String csvFilePath: csvFilePaths) {
			List<String> list = getTargets(csvFilePath);
			
            if(csvFilePaths.size() > 1 && csvFilePath.contains("FUTURES")) {
            	finalTargetColumns.addAll(list.stream().map(item -> item.concat("_futures")).collect(Collectors.toList()));
            }
            else {
            	finalTargetColumns.addAll(list);
            }
		}

		return finalTargetColumns;
	}

}
