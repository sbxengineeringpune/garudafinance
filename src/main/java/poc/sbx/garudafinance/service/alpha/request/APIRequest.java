package poc.sbx.garudafinance.service.alpha.request;

import poc.sbx.garudafinance.service.alpha.timeseries.InvalidSymbolLengthException;
import poc.sbx.garudafinance.service.alpha.timeseries.MissingRequiredQueryParameterException;

public interface APIRequest {

  /**
   * Convert all the variables in the request to query parameters for the
   * Alpha Vantage API.
   * @return A list of query parameters.
   */
  String toQueryParameters()
      throws MissingRequiredQueryParameterException,
      InvalidSymbolLengthException;

}
