package poc.sbx.garudafinance.service.alpha.timeseries;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Map;

import lombok.Data;
import poc.sbx.garudafinance.service.alpha.response.MetaData;

@Data
public class ForeignExchangeResult {

  private MetaData metaData;
  private Map<ZonedDateTime, ForeignExchange> foreignExchangeQuotes;
}