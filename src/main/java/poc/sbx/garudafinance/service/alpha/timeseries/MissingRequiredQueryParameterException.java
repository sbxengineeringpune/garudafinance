package poc.sbx.garudafinance.service.alpha.timeseries;

public class MissingRequiredQueryParameterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MissingRequiredQueryParameterException(String queryParameterKey) {
		super(queryParameterKey + " is a required parameter and the request could not be processed.");
	}

	public MissingRequiredQueryParameterException(String requiredParameterKey, String dependantParameterKey) {
		super(requiredParameterKey + " is a required parameter " + "when using the " + dependantParameterKey
				+ " query parameter " + "and the request could not be processed.");
	}

}
