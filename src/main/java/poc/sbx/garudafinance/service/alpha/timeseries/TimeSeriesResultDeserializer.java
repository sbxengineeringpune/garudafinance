package poc.sbx.garudafinance.service.alpha.timeseries;

import static poc.sbx.garudafinance.web.rest.util.alpha.AlphaVantageResultDeserializerHelper.getMetaData;
import static poc.sbx.garudafinance.web.rest.util.alpha.AlphaVantageResultDeserializerHelper.sanitizeNodeKeys;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import org.apache.commons.lang3.concurrent.CircuitBreakingException;
import poc.sbx.garudafinance.service.alpha.response.MetaData;
import poc.sbx.garudafinance.service.util.DateUtil;
import poc.sbx.garudafinance.web.rest.util.alpha.JsonParser;



/**
 * A custom deserializer for the TimeSeries API response.
 * It is a custom deserializer due to the complex structure of the response.
 * Firstly, field names are numbered, and are numbered differently between different
 * requests, so the fields need to be sanitized before deserialization.
 * Secondly, the field name for the actual data returned by the API i.e. the non
 * meta data section, is a dynamic string that changes between requests.
 */
public class TimeSeriesResultDeserializer extends JsonDeserializer<TimeSeriesResult> {

  @Override
  public TimeSeriesResult deserialize(
      com.fasterxml.jackson.core.JsonParser parser, DeserializationContext context)
      throws IOException {
    TimeSeriesResult timeSeriesResult = new TimeSeriesResult();
    ObjectCodec oc = parser.getCodec();
    JsonNode node = oc.readTree(parser);
    try {
      timeSeriesResult.setMetaData(getMetaData(node));
      timeSeriesResult.setTimeSeries(reduceMapSize(getDateObjectMap(node)));
    } catch (Throwable throwable) {
      System.out.println("Error when deserializing:");
      System.out.println(node.toString());
      throwable.printStackTrace();
    }
    return timeSeriesResult;
  }

  private ZonedDateTime parseDate(String dateStr)
      throws ParseException {
    Date date = DATE_PARSER.parse(dateStr);
    if (dateStr.length() > 10)
      date = DATE_TIME_PARSER.parse(dateStr);
    
    return DateUtil.parseDateToZonedDateTime(date);
  }

  private Map<ZonedDateTime, TimeSeries> getDateObjectMap(JsonNode node) {
    Map<ZonedDateTime, TimeSeries> timeSeriesMap = new TreeMap<>(Collections.reverseOrder());
    node.fields().forEachRemaining(nodeEntry -> {
      // ignore meta data, we want the time series data
      if (!nodeEntry.getKey().equals(MetaData.META_DATA_RESPONSE_KEY)) {
        nodeEntry.getValue().fields().forEachRemaining(timeSeriesEntry -> {
          try {
            timeSeriesMap.put(
                parseDate(timeSeriesEntry.getKey()),
                JsonParser.toObject(
                    JsonParser.toJson(sanitizeNodeKeys(timeSeriesEntry.getValue())),
                    TimeSeries.class
                )
            );

          }
          catch (IOException | ParseException e) {
            e.printStackTrace();
          }
        });
      }
    });
    return timeSeriesMap;
  }

  private Map<ZonedDateTime, TimeSeries>  reduceMapSize(Map<ZonedDateTime, TimeSeries> map){
      if(map.size() > 100){
         return map.entrySet().stream().limit(100).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
      }else {
          return map;
      }
  }

  private static final SimpleDateFormat DATE_TIME_PARSER = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
  private static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd");
}
