package poc.sbx.garudafinance.service.alpha.timeseries;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Optional;

/**
 * Representation of the data that is returned by the TimeSeries endpoints
 * in the API.
 * The values that are returned for the adjusted TimeSeries endpoints are
 * {@code Optional} typed.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TimeSeries {

//  /*
//   Override the getters to return optional.
//  */
//  public Optional<Double> getAdjustedClose() {
//    return Optional.ofNullable(adjustedClose);
//  }
//
//  public Optional<Double> getDividendAmount() {
//    return Optional.ofNullable(dividendAmount);
//  }
//
//  public Optional<Double> getSplitCoefficient() {
//    return Optional.ofNullable(splitCoefficient);
//  }

  private Double open;
  private Double high;
  private Double low;
  private Double close;
  private Double volume;
  @JsonProperty("adjusted close")
  private Double adjustedClose = null;
  @JsonProperty("dividend amount")
  private Double dividendAmount = null;
  @JsonProperty("split coefficient")
  private Double splitCoefficient = null;
  @JsonProperty("market cap")
  private Double marketCap;

  @JsonProperty("target_value") private Double causaTargetValue;
}
