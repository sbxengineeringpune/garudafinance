package poc.sbx.garudafinance.service.alpha.timeseries;

public enum DigitalCurrencySymbol {

        CURRENCY_BTC("BTC");

    DigitalCurrencySymbol(String queryParameterKey) {
        this.queryParameterKey = queryParameterKey;
    }

    public String getQueryParameterKey() {
        return queryParameterKey;
    }

    private String queryParameterKey;
}
