package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.Builder;
import lombok.Data;
import poc.sbx.garudafinance.service.alpha.request.APIRequest;

@Data
@Builder
public class CurrencyExchangeRequest implements APIRequest {

  @Override
  public String toQueryParameters()
      throws MissingRequiredQueryParameterException {
    if (fromCurrency == null)
      throw new MissingRequiredQueryParameterException("FromCurrency");
    if (toCurrency == null)
      throw new MissingRequiredQueryParameterException("ToCurrency");

    StringBuilder builder = new StringBuilder();
    builder
        .append("function=")
        .append(function);
    builder
        .append("&from_currency=")
        .append(fromCurrency);
    builder
        .append("&to_currency=")
        .append(toCurrency);
    return builder.toString();
  }

  private String fromCurrency;
  private String toCurrency;

  private static final String function = "CURRENCY_EXCHANGE_RATE";
}