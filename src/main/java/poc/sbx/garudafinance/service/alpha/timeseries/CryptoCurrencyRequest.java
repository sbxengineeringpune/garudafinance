package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.Builder;
import poc.sbx.garudafinance.service.alpha.request.APIRequest;

@Builder
public class CryptoCurrencyRequest implements APIRequest {

    private CryptocurrencyFunction function;
    private String symbol;

    @Override
    public String toQueryParameters() throws MissingRequiredQueryParameterException,InvalidSymbolLengthException {

        if (function == null)
            throw new MissingRequiredQueryParameterException("ForeignExchangeFunction");
        if (symbol == null)
            throw new MissingRequiredQueryParameterException("Symbol");

        StringBuilder builder = new StringBuilder();
        builder
            .append("function=")
            .append(function.getQueryParameterKey());
        builder
            .append("&symbol=")
            .append(symbol);
        builder
            .append("&market=")
            .append("USD");

        return builder.toString();
    }
}
