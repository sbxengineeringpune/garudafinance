package poc.sbx.garudafinance.service.alpha.timeseries;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import poc.sbx.garudafinance.service.alpha.response.MetaData;

import java.time.ZonedDateTime;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CryptoCurrencyTimeSeriesResult {
    private MetaData metaData;
    private Map<ZonedDateTime, TimeSeries> timeSeries;
}
