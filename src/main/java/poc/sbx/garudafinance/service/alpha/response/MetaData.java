package poc.sbx.garudafinance.service.alpha.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval;
import poc.sbx.garudafinance.service.alpha.request.OutputSize;

/**
 * Part of the Response from the API is a meta data section.
 * This java class represents the meta data in the response.
 */

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MetaData {
    @JsonProperty("Information")
    private String information;
    @JsonProperty("Symbol")
    private String symbol;
    @JsonProperty("Output Size")
    private OutputSize outputSize;
    @JsonProperty("Time Zone")
    private String timezone;
    @JsonProperty("Last Refreshed")
    private String lastRefreshed;
    @JsonProperty("Interval")
    private IntradayInterval interval;
    @JsonProperty("Notes")
    private String notes;
    @JsonProperty("From Symbol")
    private String fromCurrency;
    @JsonProperty("To Symbol")
    private String toCurrency;
    @JsonProperty("Digital Currency Code")
    private String digitalCurrencyCode;
    @JsonProperty("Digital Currency Name")
    private String digitalCurrencyName;
    @JsonProperty("Market Code")
    private String marketCode;
    @JsonProperty("Market Name")
    private String marketName;

    public static final String META_DATA_RESPONSE_KEY = "Meta Data";
}
