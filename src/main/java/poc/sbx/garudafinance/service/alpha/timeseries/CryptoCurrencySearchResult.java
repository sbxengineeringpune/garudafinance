package poc.sbx.garudafinance.service.alpha.timeseries;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CryptoCurrencySearchResult {

	private List<CryptoCurrency> cryptoCurrencies;
}
