package poc.sbx.garudafinance.service.alpha.timeseries;

import lombok.Data;

@Data
public class CurrencyExchangeResult {
  private CurrencyExchange quote;
}
