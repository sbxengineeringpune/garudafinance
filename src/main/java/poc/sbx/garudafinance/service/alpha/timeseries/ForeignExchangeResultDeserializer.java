package poc.sbx.garudafinance.service.alpha.timeseries;

import static poc.sbx.garudafinance.service.util.AlphaVantageResultDeserializerHelper.getDateObjectMap;
import static poc.sbx.garudafinance.service.util.AlphaVantageResultDeserializerHelper.getMetaData;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Map;

import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import poc.sbx.garudafinance.service.alpha.response.MetaData;;

public class ForeignExchangeResultDeserializer extends JsonDeserializer<ForeignExchangeResult> {
  @Override
  public ForeignExchangeResult deserialize(
      com.fasterxml.jackson.core.JsonParser parser, DeserializationContext deserializationContext
  )
      throws IOException {
    ForeignExchangeResult foreignExchangeResult = new ForeignExchangeResult();
    ObjectCodec oc = parser.getCodec();
    JsonNode node = oc.readTree(parser);
    try {
      MetaData metaData = getMetaData(node);
      Map<ZonedDateTime, ForeignExchange> foreignExchangeQuotes =
          getDateObjectMap(node, ForeignExchange.class);

      foreignExchangeResult.setForeignExchangeQuotes(foreignExchangeQuotes);
      foreignExchangeResult.setMetaData(metaData);
    } catch (Throwable t) {
      t.printStackTrace();
    }

    return foreignExchangeResult;
  }
}