package poc.sbx.garudafinance.service.alpha.timeseries;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

/**
 * A representation of the API response for the symbol search endpoints.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SymbolResult {
  private List<Symbol> bestMatches;
  
  public static final String BEST_MATCHES_RESPONSE_KEY = "bestMatches";
}
