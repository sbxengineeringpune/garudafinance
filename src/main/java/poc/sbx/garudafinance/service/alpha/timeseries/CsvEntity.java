package poc.sbx.garudafinance.service.alpha.timeseries;

import java.util.List;

import lombok.Data;

@Data
public class CsvEntity {
	List<String> columns;

}
