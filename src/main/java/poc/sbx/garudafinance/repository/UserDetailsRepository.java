package poc.sbx.garudafinance.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity;
@Repository
public interface UserDetailsRepository extends MongoRepository <UserEntity,String>{

	List<UserEntity> findAllByUserId(Long loggedInUser);

}
