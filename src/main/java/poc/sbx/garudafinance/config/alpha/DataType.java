package poc.sbx.garudafinance.config.alpha;

/**
 * The Query Parameter datatype for the API.
 */
public enum DataType {
  JSON,
  csv;
}
