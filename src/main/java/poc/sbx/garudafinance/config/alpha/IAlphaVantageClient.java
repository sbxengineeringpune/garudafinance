package poc.sbx.garudafinance.config.alpha;

import poc.sbx.garudafinance.service.alpha.request.IntradayInterval;
import poc.sbx.garudafinance.service.alpha.request.OutputSize;
import poc.sbx.garudafinance.service.alpha.timeseries.*;

import java.io.IOException;
import java.io.File;

public interface IAlphaVantageClient {

	/**
	 * Get the Intraday stock data for a single stock.
	 *
	 * @param intradayInterval The interval between stock quotes.
	 * @param symbol           The stock to get the data for.
	 * @return The Intraday API response.
	 */
	TimeSeriesResult getTimeSeries(IntradayInterval intradayInterval, String symbol)
			throws IOException, MissingRequiredQueryParameterException;

	/**
	 * Get the Intraday stock data for a single stock.
	 *
	 * @param intradayInterval The interval between stock quotes.
	 * @param symbol           The stock to get the data for.
	 * @param outputSize       The output size of either Compact or Full.
	 * @return The Intraday API response.
	 */
	TimeSeriesResult getTimeSeries(IntradayInterval intradayInterval, String symbol, OutputSize outputSize)
			throws IOException, MissingRequiredQueryParameterException;

	/**
	 * Request the TimeSeries Alpha Vantage API for a specific function and symbol.
	 *
	 * @param timeSeriesFunction The function for the TimeSeries request.
	 * @param symbol             The stock to get the data for.
	 * @return The TimeSeries API response.
	 */
	TimeSeriesResult getTimeSeries(TimeSeriesFunction timeSeriesFunction, String symbol)
			throws IOException, MissingRequiredQueryParameterException;

	/**
	 * Request the TimeSeries Alpha Vantage API for a specific function, symbol and
	 * output size.
	 *
	 * @param timeSeriesFunction The function for the TimeSeries request.
	 * @param symbol             The stock to get the data for.
	 * @param outputSize         The output size of either Compact or Full.
	 * @return The TimeSeries API response.
	 */
	TimeSeriesResult getTimeSeries(TimeSeriesFunction timeSeriesFunction, String symbol, OutputSize outputSize)
			throws IOException, MissingRequiredQueryParameterException;

	/**
	 * To retrive list of symbols based on keyword search.
	 *
	 * @param keyword
	 * @return
	 * @throws IOException
	 * @throws MissingRequiredQueryParameterException
	 */
	SymbolResult getSymbolList(String keyword) throws IOException, MissingRequiredQueryParameterException;

//  /**
//   * Request the BatchQuote API for a quote on a selection of specific Symbols.
//   * @param symbols The Symbols to get a quote.
//   * @return The quotes for the symbols requested.
//   */
//  BatchQuoteResult getBatchQuote(String... symbols)
//      throws MissingRequiredQueryParameterException,
//      InvalidSymbolLengthException, IOException;
//

	/**
	 * Request a currency exchange rate from one currency to another.
	 *
	 * @param fromCurrency The from currency in the exchange rate.
	 * @param toCurrency   The to currency in the exchange rate.
	 * @return The quote for the currency exchange.
	 */
//  ForeignExchange getExchangeRate(String fromCurrency, String toCurrency)
//      throws MissingRequiredQueryParameterException,
//      InvalidSymbolLengthException, IOException;

	CurrencyExchange getCurrencyExchange(String fromCurrency, String toCurrency)
			throws MissingRequiredQueryParameterException, IOException;

	ForeignExchangeResult getForeignExchange(ForeignExchangeFunction function, String fromCurrency, String toCurrency,
			IntradayInterval interval) throws MissingRequiredQueryParameterException, IOException;

	ForeignExchangeResult getForeignExchange(ForeignExchangeFunction function, String fromCurrency, String toCurrency)
			throws MissingRequiredQueryParameterException, IOException;

	File getTimeSeriesDataPoint(IntradayInterval interval, String symbol, OutputSize compact, DataType dataType)
			throws IOException, MissingRequiredQueryParameterException;

	File getTimeSeriesDataPoint(TimeSeriesFunction timeSeriesFunction, String symbol, DataType dataType)
			throws IOException, MissingRequiredQueryParameterException;

	File getTimeSeriesDataPoint(TimeSeriesFunction timeSeriesFunction, String symbol, OutputSize outputSize,
			DataType dataType) throws IOException, MissingRequiredQueryParameterException;

	File getForexSeriesDataPoint(IntradayInterval interval, String fromCurrency, String toCurrency,
			OutputSize compact, DataType dataType) throws IOException, MissingRequiredQueryParameterException;

	File getForexSeriesDataPoint(ForeignExchangeFunction function, String fromCurrency, String toCurrency,
			DataType dataType) throws IOException, MissingRequiredQueryParameterException;

	File getForexSeriesDataPoint(ForeignExchangeFunction function, String fromCurrency, String toCurrency,
			OutputSize outputSize, DataType dataType) throws IOException, MissingRequiredQueryParameterException;

	CryptoCurrencyTimeSeriesResult getCrtproCurrencyTimeSeriesData(CryptocurrencyFunction function, String symbol)
			throws IOException, MissingRequiredQueryParameterException, InvalidSymbolLengthException;

	File getCryptoDataPoint(CryptocurrencyFunction function, String symbol, DataType dataType)
			throws MissingRequiredQueryParameterException, InvalidSymbolLengthException, IOException;

}
