package poc.sbx.garudafinance.config.alpha;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity;
public interface FileEditterService {

	List<String> getTargets(String csvFilePath);
	
	List<String> getTargets(List<String> csvFilePaths);

	List<UserEntity> getFileList(Long loggedInUser);
	
    /**
     * service to download the csv file.
     *
     * @param fileName
     * @return
     */
    public ResponseEntity<Resource> getFile(String fileName);

}
