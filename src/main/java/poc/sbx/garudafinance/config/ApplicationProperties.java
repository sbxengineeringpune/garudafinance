package poc.sbx.garudafinance.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Garudafinance.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    @Value("${tickData.api.stream}")
    private  String TICK_URL;
    @Value("${tickData.api.help}")
    private  String TICK_HELP_URL;

    public  String getTickUrl() {
        return TICK_URL;
    }

    public  void setTickUrl(String tickUrl) {
        TICK_URL = tickUrl;
    }
}
