import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicScriptLoaderServiceService } from '../utility/dynamic-script-loader-service.service';
import { Title } from '@angular/platform-browser';
import { BrowserFavicons } from 'app/utility/favicon.service';
// private dynamicScriptLoaderService: DynamicScriptLoaderServiceService

@Component({
    selector: 'jhi-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
    ngOnInit() {
        this.faviconService.reset();
        this.faviconService.activate('alpha');
        this.titleService.setTitle('Alpha Vantage');
    }

    email: string;
    password: string;
    constructor(
        private faviconService: BrowserFavicons,
        private titleService: Title,
        private router: Router,
        private dynamicScriptLoaderService: DynamicScriptLoaderServiceService
    ) {}

    login() {
        localStorage.setItem('email', this.email);
        localStorage.setItem('password', this.password);
        this.dynamicScriptLoaderService

            .load('sender-login')
            .then(data => {
                // Script Loaded Successfully
                // console.log('script run');
                //this.router.navigate(['/alpha']);
            })
            .catch(error => console.log(error));
    }

    ngOnDestroy() {}
}
