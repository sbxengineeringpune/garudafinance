import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicScriptLoaderServiceService } from '../utility/dynamic-script-loader-service.service';
import { LoginComponent } from './login.component';
describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    beforeEach(() => {
        const routerStub = {};
        const dynamicScriptLoaderServiceServiceStub = {
            load: string1 => ({ then: () => ({ catch: () => ({}) }) })
        };
        TestBed.configureTestingModule({
            schemas: [NO_ERRORS_SCHEMA],
            declarations: [LoginComponent],
            providers: [
                { provide: Router, useValue: routerStub },
                {
                    provide: DynamicScriptLoaderServiceService,
                    useValue: dynamicScriptLoaderServiceServiceStub
                }
            ]
        });
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
    });
    it('can load instance', () => {
        expect(component).toBeTruthy();
    });
    describe('login', () => {
        it('makes expected calls', () => {
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.login();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
});
