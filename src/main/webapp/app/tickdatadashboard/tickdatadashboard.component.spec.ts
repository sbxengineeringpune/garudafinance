import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RestService } from 'app/utility/rest.service';
import { DynamicScriptLoaderServiceService } from 'app/utility/dynamic-script-loader-service.service';
import { TickdatadashboardComponent } from './tickdatadashboard.component';
describe('TickdatadashboardComponent', () => {
    let component: TickdatadashboardComponent;
    let fixture: ComponentFixture<TickdatadashboardComponent>;
    beforeEach(() => {
        const restServiceStub = { getRequest: arg1 => ({ subscribe: () => ({}) }) };
        const dynamicScriptLoaderServiceServiceStub = {
            load: string1 => ({ then: () => ({ catch: () => ({}) }) })
        };
        TestBed.configureTestingModule({
            schemas: [NO_ERRORS_SCHEMA],
            declarations: [TickdatadashboardComponent],
            providers: [
                { provide: RestService, useValue: restServiceStub },
                {
                    provide: DynamicScriptLoaderServiceService,
                    useValue: dynamicScriptLoaderServiceServiceStub
                }
            ]
        });
        fixture = TestBed.createComponent(TickdatadashboardComponent);
        component = fixture.componentInstance;
    });
    it('can load instance', () => {
        expect(component).toBeTruthy();
    });
    it('requestedData defaults to: []', () => {
        expect(component.requestedData).toEqual([]);
    });
    it('toggle defaults to: false', () => {
        expect(component.toggle).toEqual(false);
    });
    it('loading defaults to: false', () => {
        expect(component.loading).toEqual(false);
    });
    it('display defaults to: false', () => {
        expect(component.display).toEqual(false);
    });
    it('timeframe defaults to: 30', () => {
        expect(component.timeframe).toEqual(30);
    });
    it('componentToggle defaults to: stocksToggle', () => {
        expect(component.componentToggle).toEqual('stocksToggle');
    });
    describe('logOut', () => {
        it('makes expected calls', () => {
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.logOut();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
    describe('loadStocksData', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            component.loadStocksData();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
        });
    });
    describe('sendStocksDataToCausalens', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.sendStocksDataToCausalens();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
    describe('loadFuturesData', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            component.loadFuturesData();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
        });
    });
    describe('sendFuturesDataToCausalens', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.sendFuturesDataToCausalens();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
});
