import { Component, OnInit } from '@angular/core';
import { RestService } from 'app/utility/rest.service';
import { environment } from '../../environments/environment';
import { DynamicScriptLoaderServiceService } from 'app/utility/dynamic-script-loader-service.service';
import { Title } from '@angular/platform-browser';
import { BrowserFavicons } from 'app/utility/favicon.service';

@Component({
    selector: 'jhi-tickdatadashboard',
    templateUrl: './tickdatadashboard.component.html',
    styleUrls: ['./tickdatadashbaord.component.css']
})
export class TickdatadashboardComponent implements OnInit {
    symbol: any;
    companyId: any;
    rateInput: any;
    errormsg: any;
    selectedSymbol: any;
    searchBox: any;
    searchBoxFuture: any;
    requestedData: any = [];
    toggle: boolean = false;
    public loading = false;
    public display = false;
    public selectedCompany: any;
    public timeframe: number = 60;
    public componentToggle = 'stocksToggle';
    public chartConfig: any = {
        data: null
    };

    constructor(
        private faviconService: BrowserFavicons,
        private titleService: Title,
        private restService: RestService,
        private dynamicScriptLoaderService: DynamicScriptLoaderServiceService
    ) {}
    togglebtn() {
        this.toggle = !this.toggle;
    }
    /*Common*/
    /*Popup*/
    showDialog() {
        this.display = true;
    }
    ngOnInit() {
        this.faviconService.reset();
        this.faviconService.activate('tickdata');
        this.titleService.setTitle('Tick Data');
    }
    /*LogOut */
    logOut() {
        this.dynamicScriptLoaderService

            .load('tickDataLogout-script')
            .then(data => {})
            .catch(error => console.log(error));
    }

    /*TickData */
    /* Clean Component Service */
    onComponentChange(value) {
        this.componentToggle = value;
        this.selectedSymbol = '';
        this.chartConfig = {};
        this.loading = false;
        this.requestedData = [];
        this.selectedCompany = '';
        this.searchBox = '';
        this.searchBoxFuture = '';
        this.errormsg = '';
        this.selectedSymbol = '';
        this.display = false;
        this.timeframe = 60;
        this.companyId = '';
    }
    /* Stocks Data Functionality starts */

    /*Stocks Data Symbol Search */
    stocksSymbolService(event) {
        this.errormsg = '';
        this.selectedSymbol = '';
        this.display = false;
        if (this.searchBox.length > 2) {
            this.restService.getRequest(`${environment.host}${environment.api}/tickdata/search/stocks?keyword=` + this.searchBox).subscribe(
                res => {
                    if (res) {
                        this.requestedData = res;
                    }
                    this.selectedCompany = event.query;
                },
                error => {
                    this.errormsg = 'Sorry! Some internal error occurred. Please try again later.';
                }
            );
        }
    }
    /*Stocks Data Symbol Select */
    onStocksSelect(event) {
        if (event.symbol) {
            this.selectedSymbol = event.symbol;
        }
        this.companyId = event.companyId;
    }
    /*Stocks Chart  Graph */
    loadStocksData() {
        if (this.searchBox && this.timeframe) {
            this.errormsg = '';
            this.display = false;
            this.chartConfig = {
                data: null
            };
            this.loading = true;
            let url =
                `${environment.host}${environment.api}/tickdata/dailybars/stocks?symbol=` +
                this.selectedSymbol +
                '&daysBack=' +
                this.timeframe +
                '&companyId=' +
                this.companyId;

            this.restService.getRequest(url).subscribe(
                (res: any) => {
                    if (!res.timeSeries) {
                        this.loading = false;
                        this.errormsg = 'Sufficient data is not available for the provided time frame.';
                    } else {
                        this.loading = false;
                        this.chartConfig = {
                            data: { ...res }
                        };
                    }
                },
                error => {
                    this.loading = false;
                    this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
                }
            );
        } else {
            this.loading = false;
            this.errormsg = 'Please fill in the required details';
        }
    }
    /*Stocks Recipe Causalens */
    sendStocksDataToCausalens() {
        this.errormsg = '';
        let url =
            `${environment.host}${environment.api}/tickdata/dailybars/data/stocks?symbol=` +
            this.selectedSymbol +
            '&companyId=' +
            this.companyId +
            '&daysBack=' +
            this.timeframe +
            '&userid=' +
            localStorage.getItem('tickDataUid');

        this.restService.getRequest(url).subscribe(
            (res: any) => {
                if (res.url && res.fileName) {
                    localStorage.setItem('url', res.url);
                    localStorage.setItem('fileName', res.fileName);
                }

                this.dynamicScriptLoaderService
                    .load('tickDataSender-script')
                    .then(data => {
                        // Script Loaded Successfully
                        console.log('script run');
                        this.display = false;
                    })
                    .catch(error => console.log(error));
            },
            error => {
                this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
            }
        );
    }

    /* Stocks Data Functionality ends */

    /* Futures Data functionality starts*/

    /*Futures Data Symbol Search */
    futuresSymbolService(event) {
        this.errormsg = '';
        this.selectedSymbol = '';
        this.display = false;
        if (this.searchBoxFuture.length > 2) {
            this.restService
                .getRequest(`${environment.host}${environment.api}/tickdata/search/futures?keyword=` + this.searchBoxFuture)
                .subscribe(
                    res => {
                        if (res) {
                            this.requestedData = res;
                            for (let object in this.requestedData.futures) {
                                this.companyId = this.requestedData.futures[object].value.split('|')[0];
                                this.requestedData.futures[object].value = this.requestedData.futures[object].value.split('|')[1];
                            }
                        }
                        this.selectedCompany = event.query;
                    },
                    error => {
                        this.errormsg = 'Sorry! Some internal error occurred. Please try again later.';
                    }
                );
        }
    }
    /*Futures Data Symbol Select */
    onFuturesSelect(event) {
        if (event.value) {
            this.selectedSymbol = event.value;
        }
    }
    /*Futures Chart  Graph */
    loadFuturesData() {
        if (this.searchBoxFuture && this.timeframe) {
            this.errormsg = '';
            this.display = false;
            this.chartConfig = {
                data: null
            };
            this.loading = true;
            let url =
                `${environment.host}${environment.api}/tickdata/dailybars/futures?symbol=` +
                this.selectedSymbol +
                '&companyId=' +
                this.companyId +
                '&daysBack=' +
                this.timeframe;

            this.restService.getRequest(url).subscribe(
                (res: any) => {
                    if (!res.timeSeries) {
                        this.loading = false;
                        this.errormsg = 'Sufficient data is not available for the provided time frame.';
                    } else {
                        this.loading = false;
                        this.chartConfig = {
                            data: { ...res }
                        };
                    }
                },
                error => {
                    this.loading = false;
                    this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
                }
            );
        } else {
            this.loading = false;
            this.errormsg = 'Please fill in the required details';
        }
    }
    /*Futures Recipe Causalens */
    sendFuturesDataToCausalens() {
        this.errormsg = '';
        let url =
            `${environment.host}${environment.api}/tickdata/dailybars/data/futures?symbol=` +
            this.selectedSymbol +
            '&companyId=' +
            this.companyId +
            '&daysBack=' +
            this.timeframe +
            '&userid=' +
            localStorage.getItem('tickDataUid');

        this.restService.getRequest(url).subscribe(
            (res: any) => {
                if (res.url && res.fileName) {
                    localStorage.setItem('url', res.url);
                    localStorage.setItem('fileName', res.fileName);
                }

                this.dynamicScriptLoaderService
                    .load('tickDataSender-script')
                    .then(data => {
                        // Script Loaded Successfully
                        console.log('script run');
                        this.display = false;
                    })
                    .catch(error => console.log(error));
            },
            error => {
                this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
            }
        );
    }

    /* Futures Data functionality ends*/
}
