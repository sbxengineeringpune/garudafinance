import { Injectable } from '@angular/core';
import { getLineChartConfig, getCandestickChartConfig, getCausaLensChartConfig } from './utils/chart-options';
import { getDarkTheme } from './utils/themes';

/**
 * Custom function to find the max value in an Array
 */
function arrayMax(array) {
    return Math.max.apply(null, array);
}

/**
 * Custom function to find the min value in an array
 */
function arrayMin(array) {
    return Math.min.apply(null, array);
}

@Injectable({
    providedIn: 'root'
})
export class HighstockService {
    constructor() {}
    /**
     *
     * @param type Chart type string.
     * Returns the histocks options object for the type of chart passed
     */
    public getChartOptions(type): any {
        if (type === 'line') return getLineChartConfig();
        if (type === 'candlestick') return getCandestickChartConfig();
        if (type === 'causalens') return getCausaLensChartConfig();
    }

    /**
     *
     * @param theme chart theme string
     * Returns the theme object for chart.
     */
    public getChartTheme(theme): any {
        if (theme === 'dark') return getDarkTheme();
        return undefined;
    }

    /**
     *
     * @param a first value to compare
     * @param b second value to compare
     * Helper function to sort series data array in ascending order of timestamp
     */
    private sortComparator(a, b): number {
        if (a[0] < b[0]) return -1;
        if (a[0] > b[0]) return 1;
        return 0;
    }

    /**
     *
     * @param timeSeries timeSeries object in the response returned from the api
     * Massages and constructs a data array for the line chart series from the timeSeries data
     * returned from api.
     */
    private getLineChartSeriesData(timeSeries): any[] {
        const seriesData = { ...timeSeries };
        let result = [];
        let error: any = false;
        for (let key in seriesData) {
            if (
                !seriesData[key].close &&
                !seriesData[key].low &&
                !seriesData[key].high &&
                !seriesData[key].open &&
                !seriesData[key].target_value
            ) {
                error = 'No sufficient data available to generate chart';
            } else {
                result.push([
                    new Date(key).getTime(),
                    seriesData[key].close ||
                        seriesData[key].low ||
                        seriesData[key].high ||
                        seriesData[key].open ||
                        seriesData[key].target_value
                ]);
            }
        }
        if (!!error) return error;
        return result.sort(this.sortComparator);
    }

    /**
     *
     * @param timeSeries timeSeries object in the response returned from the api
     * Massages and constructs a data array for the candlestick chart series from the timeSeries data
     * returned from api.
     */
    private getCandleChartSeriesData(timeSeries): any[] {
        const seriesData = { ...timeSeries };
        let result = [];
        let error: any = false;
        for (let key in seriesData) {
            if (!seriesData[key].close && !seriesData[key].low && !seriesData[key].high && !seriesData[key].open) {
                error = 'No sufficient data available to generate chart';
            } else {
                result.push([
                    new Date(key).getTime(),
                    seriesData[key].open,
                    seriesData[key].high,
                    seriesData[key].low,
                    seriesData[key].close
                ]);
            }
        }
        if (!!error) return error;
        return result.sort(this.sortComparator);
    }

    /**
     *
     * @param config config input object sent from parent component
     * @param type type of chart selected from toggle switch
     * Constructs the series array for the type of chart selected
     */
    public prepareChartSeries(config, type): any {
        let timeSeriesData = config.data && config.data.timeSeries ? config.data.timeSeries : config.data.foreignExchangeQuotes;
        if (type === 'line') {
            if (config.data.metaData.Symbol || config.data.foreignExchangeQuotes) {
                return [
                    {
                        name: config.data.foreignExchangeQuotes
                            ? `${config.data.metaData['From Symbol']} - ${config.data.metaData['To Symbol']}`
                            : config.data.metaData.Symbol,
                        data: this.getLineChartSeriesData(timeSeriesData),
                        tooltip: {
                            valueDecimals: 2
                        }
                    }
                ];
            } else {
                return [
                    {
                        name: config.data.cryptoCurrencies
                            ? `${config.data.metaData['Digital Currency Code']} - ${config.data.metaData['Market Code']}`
                            : `${config.data.metaData['Digital Currency Code']} - ${config.data.metaData['Market Code']}`,
                        data: this.getLineChartSeriesData(timeSeriesData),
                        tooltip: {
                            valueDecimals: 2
                        }
                    }
                ];
            }
        } else if (type === 'candlestick') {
            if (config.data.metaData.Symbol || config.data.foreignExchangeQuotes) {
                return [
                    {
                        type: 'candlestick',
                        name: config.data.foreignExchangeQuotes
                            ? `${config.data.metaData['From Symbol']} - ${config.data.metaData['To Symbol']}`
                            : config.data.metaData.Symbol,
                        data: this.getCandleChartSeriesData(timeSeriesData)
                    }
                ];
            } else {
                return [
                    {
                        type: 'candlestick',
                        name: config.data.cryptoCurrencies
                            ? `${config.data.metaData['Digital Currency Code']} - ${config.data.metaData['Market Code']}`
                            : `${config.data.metaData['Digital Currency Code']} - ${config.data.metaData['Market Code']}`,
                        data: this.getCandleChartSeriesData(timeSeriesData)
                    }
                ];
            }
        } else if (type === 'causalens') {
            const actualKeys = Object.keys(config.data.actual.timeSeries);
            const lastActualDate = actualKeys.pop();
            const lastActualDateValue = config.data.actual.timeSeries[lastActualDate];
            const predictions = {
                ...config.data.predictions.timeSeries
            };
            predictions[lastActualDate] = lastActualDateValue;
            return [
                {
                    name: 'Actual',
                    data: this.getLineChartSeriesData(config.data.actual.timeSeries),
                    color: '#58508d',
                    marker: {
                        enabled: true,
                        radius: 3
                    }
                },
                {
                    name: 'Prediction',
                    data: this.getLineChartSeriesData(predictions),
                    color: '#ff6361',
                    marker: {
                        enabled: true,
                        radius: 3
                    }
                }
            ];
        }
    }

    /**
     * @param config config input object sent from parent component
     * @param type type of chart selected from toggle switch
     * Calculates the xAxis plotpoint for causa lens graph based on response
     * generates plot points for training, validation and testing
     */
    private getPlotPoint(config, stage): any {
        let allDates = [];
        let percentage;
        if (stage === 'training') percentage = parseFloat(config.data[stage]);
        else if (stage === 'validation') percentage = parseFloat(config.data['training']) + parseFloat(config.data[stage]);
        else if (stage === 'testing')
            percentage = parseFloat(config.data['training']) + parseFloat(config.data['validation']) + parseFloat(config.data[stage]);
        for (let key in config.data.actual.timeSeries) {
            allDates.push(new Date(key).getTime());
        }
        return arrayMin(allDates) + percentage * (arrayMax(allDates) - arrayMin(allDates));
    }

    /**
     *
     * @param config config input object sent from parent component
     * @param type type of chart selected from toggle switch
     * Generates the plotlines array for causalens chart
     */
    public getXAxis(config, type): any {
        if (type === 'causalens') {
            return [
                {
                    color: '#000',
                    width: 2,
                    dashStyle: 'ShortDash',
                    label: {
                        text: 'Training',
                        rotation: 0,
                        textAlign: 'right',
                        x: -10
                    },
                    value: this.getPlotPoint(config, 'training')
                },
                {
                    color: '#000',
                    width: 2,
                    dashStyle: 'ShortDash',
                    label: {
                        text: 'Validation',
                        rotation: 0,
                        textAlign: 'right',
                        x: -10
                    },
                    value: this.getPlotPoint(config, 'validation')
                },
                {
                    color: '#dadadb',
                    width: 2,
                    dashStyle: 'ShortDash',
                    label: {
                        text: 'Testing',
                        rotation: 0,
                        textAlign: 'right',
                        x: -10
                    },
                    value: this.getPlotPoint(config, 'testing')
                }
            ];
        }
    }
}
