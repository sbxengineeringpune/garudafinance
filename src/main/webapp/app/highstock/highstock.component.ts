import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges, AfterViewInit } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { HighstockService } from './highstock.service';

declare var Highcharts: any;

@Component({
    selector: 'highstock',
    templateUrl: './highstock.component.html',
    styleUrls: ['./highstock.component.css']
})
export class HighstockComponent implements OnInit, OnChanges, AfterViewInit {
    /**
     * Input property to be passed from parent component
     */
    @Input() config;

    /**
     * Selector for chart div container. Chart div HTML will be inserted
     * inside this div using the native element reference
     */
    @ViewChild('chartDivContainer') chartDivContainer: ElementRef;

    /**
     * Private variable to hold the id of the generated chart div
     */
    private chartContainerId: string;

    /**
     * Selected type of chart. Value changes on toggle change
     */
    public selectedChartType: string;

    /**
     * ngModel variable for chart type toggle
     */
    public lineChecked: boolean = true;

    constructor(private highstockService: HighstockService) {}

    ngOnInit() {}

    /**
     *
     * @param theme theme string from config input
     * Sets the theme of chart based on the value passed.
     * Defaults to highstocks light theme if none specified.
     * Invoked from initChart function
     */
    private setChartTheme(theme): void {
        Highcharts.theme = this.highstockService.getChartTheme(theme);
        Highcharts.setOptions(Highcharts.theme);
    }

    /**
     *
     * @param options Formatted options object as per highstocks library
     * Generates chart inside the container specified.
     * Invoked from initChart function
     */
    private generateChart(options): void {
        Highcharts.stockChart(this.chartContainerId, options);
    }

    /**
     * Generates a unique id for the chart div so that same div wont be
     * over written if component is used multiple times.
     *
     * Generates the id, assigns it to chartContainerId variable and returns it
     */
    private generateChartId(): string {
        this.chartContainerId = `highstock-${this.selectedChartType}-${UUID.UUID()}`;
        return this.chartContainerId;
    }

    /**
     * Inserts the chart container div with unique id into the main div
     */
    private prepareContainer(): void {
        this.chartDivContainer.nativeElement.innerHTML = `<div id=${this.generateChartId()}></div>`;
    }

    /**
     * @param options Options object as per highstocks
     * Creates the series array for chart
     */
    private prepareChartSeries(options): void {
        options.series = this.highstockService.prepareChartSeries(this.config, this.selectedChartType);
    }

    /**
     *
     * @param options Options object as per highstocks
     * Creates the xAxis plotlines for causa lens graph
     */
    private setXAxis(options): void {
        if (this.selectedChartType === 'causalens') {
            options.xAxis = options.xAxis || {};
            options.xAxis.ordinal = false;
            options.xAxis.plotLines = this.highstockService.getXAxis(this.config, this.selectedChartType);
        }
    }

    /**
     *
     * @param options Options object as per highstocks
     * Sets the chart title depending on type of chart and response
     */
    private setChartTitle(options): void {
        if (this.selectedChartType === 'causalens') {
            options.title.text =
                this.config.title ||
                `${this.config.data.predictions.metaData.Symbol} - ${this.config.data.predictions.metaData.Information} and Actuals`;
        } else {
            if (this.config && this.config.data && this.config.data.foreignExchangeQuotes) {
                options.title.text =
                    this.config.title ||
                    `${this.config.data.metaData['From Symbol']} - ${this.config.data.metaData['To Symbol']} exchange rate over Time`;
            } else {
                if (this.config.data.metaData.Information.match('Futures')) {
                    options.title.text = this.config.title || `${this.config.data.metaData.Symbol} Futures over Time`;
                } else if (this.config.data.metaData.Symbol) {
                    options.title.text = this.config.title || `${this.config.data.metaData.Symbol} Stock Price`;
                } else {
                    options.title.text =
                        this.config.title ||
                        `${this.config.data.metaData['Digital Currency Code']}- ${this.config.data.metaData['Market Code']} rate over time`;
                }
            }
        }
    }

    /**
     * uses data from the config input and constructs and options object as per highstocks library
     * Invokes setChartTheme and generateChart functions with their corresponding arguments.
     * Invoked fro ngAfterViewInit
     */
    private initChart(): void {
        let options = this.highstockService.getChartOptions(this.selectedChartType);
        if (this.config.colors) options.colors = this.config.colors;
        this.setChartTitle(options);
        //this.setXAxis(options);
        this.prepareChartSeries(options);
        this.prepareContainer();
        this.setChartTheme(this.config.theme);
        this.generateChart(options);
    }

    /**
     * Triggers initChart after view is initialized on dom.
     */
    ngAfterViewInit() {
        if (this.config.data) {
            this.onTypeChange(this.config.type);
        }
    }

    /**
     * Redraws chart on change detection
     */
    ngOnChanges() {
        if (this.config.data) {
            this.onTypeChange(this.config.type);
        }
    }

    /**
     *
     * @param e event emitted from the toggle's onChange event
     * Handler function to handle onChange event from toggle switch component from primeng
     */
    public onTypeChange(type): void {
        this.selectedChartType = type || 'line';
        this.initChart();
    }
}
