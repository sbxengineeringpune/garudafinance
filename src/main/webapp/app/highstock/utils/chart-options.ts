/**
 * Config object for line chart as per HighStocks library
 */
export function getLineChartConfig() {
    return {
        chart: {
            alignTicks: false
        },
        rangeSelector: {
            selected: 1,
            inputEnabled: false
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        series: []
    };
}

/**
 * Config object for candlestick chart as per Highstocks library
 */
export function getCandestickChartConfig() {
    return {
        rangeSelector: {
            selected: 1,
            inputEnabled: false
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: true
        },
        series: []
    };
}

export function getCausaLensChartConfig() {
    return getCandestickChartConfig();
}
