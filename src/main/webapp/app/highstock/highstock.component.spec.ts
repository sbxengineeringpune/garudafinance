import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HighstockService } from './highstock.service';
import { HighstockComponent } from './highstock.component';
describe('HighstockComponent', () => {
    let component: HighstockComponent;
    let fixture: ComponentFixture<HighstockComponent>;
    beforeEach(() => {
        const highstockServiceStub = {
            getChartTheme: theme1 => ({}),
            prepareChartSeries: (arg1, arg2) => ({}),
            getXAxis: (arg1, arg2) => ({}),
            getChartOptions: arg1 => ({ colors: {} })
        };
        TestBed.configureTestingModule({
            schemas: [NO_ERRORS_SCHEMA],
            declarations: [HighstockComponent],
            providers: [{ provide: HighstockService, useValue: highstockServiceStub }]
        });
        fixture = TestBed.createComponent(HighstockComponent);
        component = fixture.componentInstance;
    });
    it('can load instance', () => {
        expect(component).toBeTruthy();
    });
    it('lineChecked defaults to: true', () => {
        expect(component.lineChecked).toEqual(true);
    });
    describe('ngAfterViewInit', () => {
        it('makes expected calls', () => {
            spyOn(component, 'onTypeChange').and.callThrough();
            component.ngAfterViewInit();
            expect(component.onTypeChange).toHaveBeenCalled();
        });
    });
    describe('ngOnChanges', () => {
        it('makes expected calls', () => {
            spyOn(component, 'onTypeChange').and.callThrough();
            component.ngOnChanges();
            expect(component.onTypeChange).toHaveBeenCalled();
        });
    });
});
