import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dataConverter'
})
export class DataConverterPipe implements PipeTransform {
    transform(date: string): any {
        return date.substring(0, 10);
    }
}
