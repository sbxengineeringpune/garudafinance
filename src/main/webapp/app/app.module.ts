import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CalendarModule } from 'primeng/calendar';
import { AppComponent } from './app.component';
import { HighstockComponent } from './highstock/highstock.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { RestService } from './utility/rest.service';
import { AuthGuardGuard } from './utility/auth-guard.guard';
import { AlphadashboardComponent } from './AlphaVantage/alphadashboard/alphadashboard.component';
import { RouterModule } from '@angular/router';
import { routes } from './router';
import { LandingComponent } from './landing/landing.component';
import { DialogModule } from 'primeng/dialog';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { FiltersComponent } from './filters/filters.component';
// import { HeaderComponent } from './header/header.component';
// import { FooterComponent } from './footer/footer.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputTextModule } from 'primeng/inputtext';
import { CausalensdashboardComponent } from './CausaLens/causalensdashboard/causalensdashboard.component';
import { LoginComponent } from './login/login.component';
import { SigninComponent } from './signincausalens/signin.component';
import { ChartingPlaygroundComponent } from './charting-playground/charting-playground.component';
import { TickdatadashboardComponent } from './tickdatadashboard/tickdatadashboard.component';
import { TickloginComponent } from './ticklogin/ticklogin.component';
import { AccordionModule } from 'primeng/accordion';
import { DataConverterPipe } from './data-converter.pipe';
import { DateConverterIntradayPipe } from './date-converter-intraday.pipe';
import { Favicons, BrowserFavicons, BROWSER_FAVICONS_CONFIG } from './utility/favicon.service';

@NgModule({
    declarations: [
        AppComponent,
        HighstockComponent,
        AlphadashboardComponent,
        LandingComponent,
        FiltersComponent,
        // HeaderComponent,
        // FooterComponent,
        SearchBarComponent,
        CausalensdashboardComponent,
        LoginComponent,
        SigninComponent,
        ChartingPlaygroundComponent,
        TickdatadashboardComponent,
        TickloginComponent,
        DataConverterPipe,
        DateConverterIntradayPipe
        // filter
    ],
    imports: [
        BrowserModule,
        DropdownModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpModule,
        DialogModule,
        InputSwitchModule,
        ProgressSpinnerModule,
        HttpClientModule,
        FormsModule,
        CalendarModule,
        RouterModule.forRoot(routes, { useHash: true }),
        AngularFontAwesomeModule,
        AutoCompleteModule,
        InputTextModule,
        AccordionModule
    ],
    providers: [
        RestService,
        HttpModule,
        AuthGuardGuard,
        Title,

        {
            provide: BrowserFavicons,
            useClass: BrowserFavicons
        },
        // The BROWSER_FAVICONS_CONFIG sets up the favicon definitions for the browser-
        // based implementation. This way, the rest of the application only needs to know
        // the identifiers (ie, 'happy', 'default') - it doesn't need to know the paths
        // or the types. This allows the favicons to be modified independently without
        // coupling too tightly to the rest of the code.
        {
            provide: BROWSER_FAVICONS_CONFIG,
            useValue: {
                icons: {
                    alpha: {
                        type: 'image/png',
                        href: 'content/images/alpha.png',
                        isDefault: true
                    },
                    causalens: {
                        type: 'image/png',
                        href: 'content/images/causalens.png'
                    },
                    tickdata: {
                        type: 'image/png',
                        href: 'content/images/tickdata.png'
                    }
                }
            }
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
