import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RestService } from '../utility/rest.service';

@Component({
    selector: 'app-filters',
    templateUrl: './filters.component.html',
    styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit {
    searchBox: any;
    requestedData = [
        { companyName: 'Microsoft' },
        { companyName: 'Google' },
        { companyName: 'Facebook' },
        { companyName: 'Innovecture' },
        { companyName: 'Cognizant' }
    ];
    @Input() config: any;

    @Output() public childEvent = new EventEmitter();
    constructor(private restService: RestService) {}

    ngOnInit() {}
    searchValue(event) {
        if (this.searchBox.length > 2) {
            this.childEvent.emit(this.searchBox);
        }
    }
}
