import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicScriptLoaderServiceService } from 'app/utility/dynamic-script-loader-service.service';
import { Title } from '@angular/platform-browser';
import { BrowserFavicons } from 'app/utility/favicon.service';

@Component({
    selector: 'jhi-signin',
    templateUrl: './signin.component.html',
    styleUrls: [`./signin.component.css`]
})
export class SigninComponent implements OnInit {
    email_causalens: string;
    password_causalens: string;

    constructor(
        private faviconService: BrowserFavicons,
        private titleService: Title,
        private router: Router,
        private dynamicScriptLoaderService: DynamicScriptLoaderServiceService
    ) {}

    signin() {
        localStorage.setItem('causalense-email', this.email_causalens);
        localStorage.setItem('causalense-password', this.password_causalens);
        // this.router.navigate(['/causalens']);
        this.dynamicScriptLoaderService
            .load('receiver-script')
            .then(data => {})
            .catch(error => console.log(error));
    }
    ngOnInit() {
        this.faviconService.reset();
        this.faviconService.activate('causalens');
        this.titleService.setTitle('Causalens');
    }

    ngOnDestroy() {}
}
