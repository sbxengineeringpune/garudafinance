import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicScriptLoaderServiceService } from 'app/utility/dynamic-script-loader-service.service';
import { SigninComponent } from './signin.component';
describe('SigninComponent', () => {
    let component: SigninComponent;
    let fixture: ComponentFixture<SigninComponent>;
    beforeEach(() => {
        const routerStub = {};
        const dynamicScriptLoaderServiceServiceStub = {
            load: string1 => ({ then: () => ({ catch: () => ({}) }) })
        };
        TestBed.configureTestingModule({
            schemas: [NO_ERRORS_SCHEMA],
            declarations: [SigninComponent],
            providers: [
                { provide: Router, useValue: routerStub },
                {
                    provide: DynamicScriptLoaderServiceService,
                    useValue: dynamicScriptLoaderServiceServiceStub
                }
            ]
        });
        fixture = TestBed.createComponent(SigninComponent);
        component = fixture.componentInstance;
    });
    it('can load instance', () => {
        expect(component).toBeTruthy();
    });
    describe('signin', () => {
        it('makes expected calls', () => {
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.signin();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
});
