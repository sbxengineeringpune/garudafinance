import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RestService } from './../../utility/rest.service';
import { DynamicScriptLoaderServiceService } from 'app/utility/dynamic-script-loader-service.service';
import { CausalensdashboardComponent } from './causalensdashboard.component';
describe('CausalensdashboardComponent', () => {
    let component: CausalensdashboardComponent;
    let fixture: ComponentFixture<CausalensdashboardComponent>;
    beforeEach(() => {
        const restServiceStub = {
            getRequest: arg1 => ({ subscribe: () => ({}) }),
            postRequest: (arg1, body2) => ({ subscribe: () => ({}) })
        };
        const dynamicScriptLoaderServiceServiceStub = {
            load: string1 => ({ then: () => ({ catch: () => ({}) }) })
        };
        TestBed.configureTestingModule({
            schemas: [NO_ERRORS_SCHEMA],
            declarations: [CausalensdashboardComponent],
            providers: [
                { provide: RestService, useValue: restServiceStub },
                {
                    provide: DynamicScriptLoaderServiceService,
                    useValue: dynamicScriptLoaderServiceServiceStub
                }
            ]
        });
        fixture = TestBed.createComponent(CausalensdashboardComponent);
        component = fixture.componentInstance;
    });
    it('can load instance', () => {
        expect(component).toBeTruthy();
    });
    it('selectedSymbol defaults to: false', () => {
        expect(component.selectedSymbol).toEqual(false);
    });
    it('fileList defaults to: []', () => {
        expect(component.fileList).toEqual([]);
    });
    it('loading defaults to: false', () => {
        expect(component.loading).toEqual(false);
    });
    it('prediction_array defaults to: []', () => {
        expect(component.prediction_array).toEqual([]);
    });
    it('dateFlag defaults to: false', () => {
        expect(component.dateFlag).toEqual(false);
    });
    describe('ngOnInit', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.ngOnInit();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
    describe('analyzeResult', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            spyOn(restServiceStub, 'postRequest').and.callThrough();
            component.analyzeResult();
            expect(restServiceStub.postRequest).toHaveBeenCalled();
        });
    });
    describe('logOut', () => {
        it('makes expected calls', () => {
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.logOut();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
});
