import { Component, OnInit, Input } from '@angular/core';
import { RestService } from './../../utility/rest.service';
import { environment } from './../../../environments/environment';
import { DynamicScriptLoaderServiceService } from 'app/utility/dynamic-script-loader-service.service';
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';
import { Title } from '@angular/platform-browser';
import { BrowserFavicons } from 'app/utility/favicon.service';

@Component({
    selector: 'jhi-app-causalensdashboard',
    templateUrl: './causalensdashboard.component.html',
    styleUrls: ['./causalensdashboard.component.css']
})
export class CausalensdashboardComponent implements OnInit {
    searchBox: any;
    errormsg: String;
    selectedFile: any;
    requestedData: any;
    formData: any = {};
    selectedColumn: any;
    targetColumn: any;
    filePath: any;
    fileName: any;
    selectedSymbol: boolean = false;
    fileList: any = [];
    public loading: boolean = false;
    causalensData: any = {};
    urlObj: any;
    validation: any;
    testing: any;
    response;
    training: any;
    modelName: String;
    modelType: any;
    modelScore: any;
    gadgetName: any = '';
    prediction_array: any = [];
    pred_object: any;
    dateFlag: boolean = false;
    dateString: String;
    dateString_intra: String;
    targetColumnFlag: any;
    array: any = [];
    item: any;
    checkbox_item: any;
    array_list: any = [];
    display_list: any = '';
    fileUrlArray: any = [];
    fileNameArray: any = [];
    inputValue: any = '';
    list: any;
    count: any = 0;
    checkDisable: any;

    public chartConfig: any = {
        type: 'causalens',
        data: null
    };

    constructor(
        private faviconService: BrowserFavicons,
        private titleService: Title,
        private restService: RestService,
        private dynamicScriptLoader: DynamicScriptLoaderServiceService
    ) {}

    ngOnInit() {
        this.faviconService.reset();
        this.faviconService.activate('causalens');
        this.titleService.setTitle('Causalens');
        this.restService.getRequest(`${environment.host}${environment.api}/file/causaFileList?loggedinuser=455`).subscribe(res => {
            this.dynamicScriptLoader
                .load('receiverSocket-script')
                .then(data => {
                    // this.display = false;
                })
                .catch(error => console.log(error));
        });
        if (document.getElementById('linkId')) {
            var element = document.getElementById('linkId');
            element.onclick = this.openCloseList;
        }
        this.gadgetName = '';
    }

    fetchTargetColumns(e, list) {
        this.errormsg = '';
        let isFuture = false;
        let isBtc = false;
        if (e.target.checked) {
            this.count++;
            this.fileUrlArray.push(list.url);
            this.fileNameArray.push(list.file_name);
            this.inputValue += ' ' + list.file_name;
            this.gadgetName = '';
            for (let i = 0; i < this.fileNameArray.length; i++) {
                if (i < this.fileNameArray.length - 1) {
                    this.gadgetName += localStorage.getItem(this.fileNameArray[i]) + ', ';
                } else {
                    this.gadgetName += localStorage.getItem(this.fileNameArray[i]) + ' ';
                }
            }

            if (list.file_name.includes('FUTURES') && list.file_name.includes('BTC') && list.file_name.includes('DAILY')) {
                isFuture = true;
            }

            if (!list.file_name.includes('FUTURES') && list.file_name.includes('BTC') && list.file_name.includes('DAILY')) {
                isBtc = true;
            }

            for (let i = 0; i < this.array_list.length; i++) {
                if (this.count < 2) {
                    if (isFuture) {
                        this.array_list[i].disabled = 'true';
                        if (
                            this.array_list[i].file_name === list.file_name ||
                            (this.array_list[i].file_name.startsWith('BTC') &&
                                this.array_list[i].file_name.includes('DAILY') &&
                                !this.array_list[i].file_name.includes('FUTURES'))
                        ) {
                            this.array_list[i].disabled = null;
                        }
                    } else if (isBtc) {
                        this.array_list[i].disabled = 'true';
                        if (
                            this.array_list[i].file_name === list.file_name ||
                            (this.array_list[i].file_name.startsWith('BTC') &&
                                this.array_list[i].file_name.includes('DAILY') &&
                                this.array_list[i].file_name.includes('FUTURES'))
                        ) {
                            this.array_list[i].disabled = null;
                        }
                    } else {
                        if (this.array_list[i].file_name != list.file_name) {
                            this.array_list[i].disabled = 'true';
                        }
                    }
                } else {
                    console.log(this.fileNameArray);
                    if (!JSON.stringify(this.fileNameArray).includes(this.array_list[i].file_name)) {
                        this.array_list[i].disabled = true;
                    }
                }
            }
        } else if (!e.target.checked) {
            this.count--;
            this.fileUrlArray = this.arrayRemove(this.fileUrlArray, list.url);
            this.fileNameArray = this.arrayRemove(this.fileNameArray, list.file_name);

            this.gadgetName = this.gadgetName.replace(localStorage.getItem(list.file_name), '');
            if (this.gadgetName.includes(',')) {
                this.gadgetName = this.gadgetName.replace(',', '');
            }
            if (this.count === 0) {
                this.inputValue = '';
            } else {
                this.inputValue = this.inputValue.replace(list.file_name, '');
            }

            if (list.file_name.includes('FUTURES') && list.file_name.includes('BTC') && list.file_name.includes('DAILY')) {
                isFuture = true;
            }

            if (!list.file_name.includes('FUTURES') && list.file_name.includes('BTC') && list.file_name.includes('DAILY')) {
                isBtc = true;
            }

            for (let i = 0; i < this.array_list.length; i++) {
                if (this.count > 0) {
                    if (isFuture) {
                        this.array_list[i].disabled = 'true';
                        if (JSON.stringify(this.fileNameArray).includes(this.array_list[i].file_name)) {
                            this.array_list[i].disabled = null;
                        }
                        if (
                            this.array_list[i].file_name === list.file_name ||
                            (this.array_list[i].file_name.startsWith('BTC') &&
                                this.array_list[i].file_name.includes('DAILY') &&
                                this.array_list[i].file_name.includes('FUTURES'))
                        ) {
                            this.array_list[i].disabled = null;
                        }
                    } else if (isBtc) {
                        this.array_list[i].disabled = 'true';
                        if (JSON.stringify(this.fileNameArray).includes(this.array_list[i].file_name)) {
                            this.array_list[i].disabled = null;
                        }
                        if (
                            this.array_list[i].file_name === list.file_name ||
                            (this.array_list[i].file_name.startsWith('BTC') &&
                                this.array_list[i].file_name.includes('DAILY') &&
                                !this.array_list[i].file_name.includes('FUTURES'))
                        ) {
                            this.array_list[i].disabled = null;
                        }
                    } else {
                        if (this.array_list[i].file_name != list.file_name) {
                            this.array_list[i].disabled = null;
                        }
                    }
                } else {
                    this.array_list[i].disabled = null;
                }
            }
        }
        const body = {
            csvFilePaths: this.fileUrlArray
        };
        this.restService.postRequest(`${environment.host}${environment.api}/file/causaTarget/multi`, body).subscribe(res => {
            if (res) {
                this.requestedData = res;
                this.selectedSymbol = true;
                this.targetColumn = '';
            }
        });
    }

    arrayRemove(arr, value) {
        return arr.filter(function(ele) {
            return ele != value;
        });
    }
    selectedTarget(e) {
        this.errormsg = '';
        this.targetColumn = e.target.value.split(':')[1].trim();
    }
    analyzeResult() {
        this.chartConfig = {
            type: 'causalens',
            data: null
        };
        this.targetColumnFlag = this.targetColumn;
        this.loading = true;
        this.errormsg = '';
        this.prediction_array = [];
        let csvLocations = [];
        let fileNames = [];
        csvLocations = this.fileUrlArray;
        fileNames = this.fileNameArray;
        const causalensData = {
            csvLocations: csvLocations,
            targetColumn: this.targetColumn,
            fileNames: fileNames,
            futureIndex: 10
        };
        console.log(causalensData);
        if (this.selectedSymbol && this.targetColumn) {
            this.restService.postRequest(`${environment.host}${environment.api}/causa/prediction`, causalensData).subscribe(
                res => {
                    this.loading = false;
                    this.response = res;
                    this.testing = this.response.testing;
                    this.validation = this.response.validation;
                    this.training = this.response.training;
                    this.modelName = this.response.modelName;
                    this.modelType = this.response.modelType;
                    this.modelScore = this.response.modelScore;

                    let object = this.response.predictions.timeSeries; //prediction table

                    for (let key in object) {
                        this.pred_object = { date: key, value: object[key] };
                        this.prediction_array.push(this.pred_object);
                    }
                    for (let i = 0; i < this.prediction_array.length; i++) {
                        for (let j = i + 1; j < this.prediction_array.length; j++) {
                            this.dateString = this.prediction_array[i].date;
                            this.dateString_intra = this.prediction_array[j].date;
                            if (this.dateString.substring(0, 10) != this.dateString_intra.substring(0, 10)) {
                                this.dateFlag = true;
                            } else {
                                this.dateFlag = false;
                            }
                        }
                    }
                    this.chartConfig = {
                        type: 'causalens',
                        data: { ...res }
                    };
                },
                error => {
                    this.loading = false;
                    this.errormsg = 'Sorry! Some internal error occurred. Please try again later.';
                }
            );
        } else {
            this.loading = false;
            this.errormsg = 'Please provide the required details.';
        }
    }

    logOut() {
        this.dynamicScriptLoader
            .load('receiverLogout-script')
            .then(data => {
                // this.display = false;
            })
            .catch(error => console.log(error));
        localStorage.removeItem('file_details');
    }

    // Multiselect JS with jquery - Mike Lewallen

    openCloseList(e) {
        let array = [];
        if (e.target.classList.contains('open')) {
            e.target.classList.remove('open');
        } else {
            e.target.classList.add('open');
        }

        var list = document.getElementById('list');

        if (list.classList.contains('display-none')) {
            list.classList.remove('display-none');
            list.classList.add('display-block');
        } else {
            list.classList.remove('display-block');
            list.classList.add('display-none');
        }
    }

    getChecklist(searchValue: string) {
        this.array_list = JSON.parse(localStorage.getItem('file_details'));
        this.fileUrlArray = [];
        this.fileNameArray = [];
        this.inputValue = '';
        this.count = 0;
        this.targetColumn = '';
        this.selectedSymbol = false;
    }
}
