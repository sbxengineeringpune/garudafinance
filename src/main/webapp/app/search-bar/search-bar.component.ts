import { Component, OnInit } from '@angular/core';
import { RestService } from '../utility/rest.service';

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
    searchBox: any;
    lineChecked = false;
    requestedData: any = [];

    constructor(private restService: RestService) {}

    ngOnInit() {}

    searchToggle(obj, evt) {
        // this.lineChecked = !this.lineChecked;
    }

    symbolService() {
        if (this.searchBox.length > 2) {
            // this.restService.postRequest("http://localhost:8080/garudafinance/gadget/symbolsearch?keyword="+this.searchBox)

            this.restService.getRequest('/assets/mocks/aapl1.json').subscribe(res => {
                this.requestedData = [
                    { companyName: 'Microsoft' },
                    { companyName: 'Google' },
                    { companyName: 'Facebook' },
                    { companyName: 'Innovecture' },
                    { companyName: 'Cognizant' }
                ];
                //  console.log(this.chartConfig.data=res);
                //  this.loading = false;
            });
        }
    }
}
