import { RouterModule, Routes } from '@angular/router';
import { AlphadashboardComponent } from './AlphaVantage/alphadashboard/alphadashboard.component';
import { CausalensdashboardComponent } from './CausaLens/causalensdashboard/causalensdashboard.component';
import { LoginComponent } from './login/login.component';
import { SigninComponent } from './signincausalens/signin.component';
import { AuthGuardGuard } from './utility/auth-guard.guard';
import { NgModule } from '@angular/core';
import { ChartingPlaygroundComponent } from './charting-playground/charting-playground.component';
import { TickdatadashboardComponent } from './tickdatadashboard/tickdatadashboard.component';
import { TickloginComponent } from './ticklogin/ticklogin.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'alpha',
        pathMatch: 'full'
    },
    {
        path: 'alpha',
        component: LoginComponent
    },
    {
        path: 'alpha/home',
        component: AlphadashboardComponent,
        canActivate: [AuthGuardGuard]
    },
    {
        path: 'causalens',
        component: SigninComponent
    },
    {
        path: 'causalens/home',
        component: CausalensdashboardComponent
        // canActivate: [AuthGuardGuard]
    },
    {
        path: 'charting-playground',
        component: ChartingPlaygroundComponent
    },
    {
        path: 'tick',
        component: TickloginComponent
        // canActivate: [AuthGuardGuard]
    },
    {
        path: 'tick/home',
        component: TickdatadashboardComponent
        // canActivate: [AuthGuardGuard]
    }
];
