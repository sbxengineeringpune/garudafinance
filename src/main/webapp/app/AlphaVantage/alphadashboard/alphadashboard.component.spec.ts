import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RestService } from './../../utility/rest.service';
import { Router } from '@angular/router';
import { DynamicScriptLoaderServiceService } from './../../utility/dynamic-script-loader-service.service';
import { AlphadashboardComponent } from './alphadashboard.component';
describe('AlphadashboardComponent', () => {
    let component: AlphadashboardComponent;
    let fixture: ComponentFixture<AlphadashboardComponent>;
    beforeEach(() => {
        const restServiceStub = { getRequest: arg1 => ({ subscribe: () => ({}) }) };
        const routerStub = {};
        const dynamicScriptLoaderServiceServiceStub = {
            load: string1 => ({ then: () => ({ catch: () => ({}) }) })
        };
        TestBed.configureTestingModule({
            schemas: [NO_ERRORS_SCHEMA],
            declarations: [AlphadashboardComponent],
            providers: [
                { provide: RestService, useValue: restServiceStub },
                { provide: Router, useValue: routerStub },
                {
                    provide: DynamicScriptLoaderServiceService,
                    useValue: dynamicScriptLoaderServiceServiceStub
                }
            ]
        });
        fixture = TestBed.createComponent(AlphadashboardComponent);
        component = fixture.componentInstance;
    });
    it('can load instance', () => {
        expect(component).toBeTruthy();
    });
    it('toggle defaults to: false', () => {
        expect(component.toggle).toEqual(false);
    });
    it('timeframe defaults to: DAILY', () => {
        expect(component.timeframe).toEqual('DAILY');
    });
    it('timeinterval defaults to: ONE_MINUTE', () => {
        expect(component.timeinterval).toEqual('ONE_MINUTE');
    });
    it('loading defaults to: false', () => {
        expect(component.loading).toEqual(false);
    });
    it('componentToggle defaults to: stocksToggle', () => {
        expect(component.componentToggle).toEqual('stocksToggle');
    });
    it('requestedData defaults to: []', () => {
        expect(component.requestedData).toEqual([]);
    });
    it('display defaults to: false', () => {
        expect(component.display).toEqual(false);
    });
    it('fromCurrency defaults to: false', () => {
        expect(component.fromCurrency).toEqual(false);
    });
    it('toCurrency defaults to: false', () => {
        expect(component.toCurrency).toEqual(false);
    });
    it('curtimeframe defaults to: DAILY', () => {
        expect(component.curtimeframe).toEqual('DAILY');
    });
    it('curtimeinterval defaults to: ONE_MINUTE', () => {
        expect(component.curtimeinterval).toEqual('ONE_MINUTE');
    });
    it('currencyData defaults to: []', () => {
        expect(component.currencyData).toEqual([]);
    });
    it('cryptoTimeFrame defaults to: DAILY', () => {
        expect(component.cryptoTimeFrame).toEqual('DAILY');
    });
    describe('ngOnInit', () => {
        it('makes expected calls', () => {
            spyOn(component, 'forexCurrencyService').and.callThrough();
            component.ngOnInit();
            expect(component.forexCurrencyService).toHaveBeenCalled();
        });
    });
    describe('logOut', () => {
        it('makes expected calls', () => {
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.logOut();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
    describe('loadStocksData', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            component.loadStocksData();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
        });
    });
    describe('sendStocksDataToCausalens', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.sendStocksDataToCausalens();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
    describe('forexCurrencyService', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            component.forexCurrencyService();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
        });
    });
    describe('loadForexData', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            component.loadForexData();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
        });
    });
    describe('sendForexDataToCausalens', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.sendForexDataToCausalens();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
    describe('loadCryptoData', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            component.loadCryptoData();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
        });
    });
    describe('sendCryptoDataToCausalens', () => {
        it('makes expected calls', () => {
            const restServiceStub: RestService = fixture.debugElement.injector.get(RestService);
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(restServiceStub, 'getRequest').and.callThrough();
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.sendCryptoDataToCausalens();
            expect(restServiceStub.getRequest).toHaveBeenCalled();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
});
