import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RestService } from './../../utility/rest.service';
import { environment } from './../../../environments/environment';
import { Router } from '@angular/router';
import { DynamicScriptLoaderServiceService } from './../../utility/dynamic-script-loader-service.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Title } from '@angular/platform-browser';
import { BrowserFavicons } from 'app/utility/favicon.service';

@Component({
    selector: 'app-alphadashboard',
    templateUrl: './alphadashboard.component.html',
    styleUrls: ['./alphadashboard.component.css']
})
export class AlphadashboardComponent implements OnInit {
    searchBox: any;
    cryptoSearchSuggestion: any;
    errormsg: String;
    toggle: boolean = false;
    public selectedCompany: any;
    public timeframe: any = 'DAILY';
    public timeinterval: any = 'ONE_MINUTE';
    public loading = false;
    public componentToggle = 'stocksToggle';
    public filterConfig: any;
    public selectedSymbol: any;
    public requestedData: any = [];
    public display = false;
    public fromCurrency = false;
    public toCurrency = false;
    public curtimeframe: any = 'DAILY';
    public curtimeinterval: any = 'ONE_MINUTE';
    public currencyData: any = [];
    public cur_from: any;
    public cur_to: any;
    public cryptoTimeFrame: any = 'DAILY';
    public chartConfig: any = {
        data: null
    };

    // @ViewChild("myElem") myElem: ElementRef;

    constructor(
        private faviconService: BrowserFavicons,
        private titleService: Title,
        private restService: RestService,
        private router: Router,
        private dynamicScriptLoader: DynamicScriptLoaderServiceService
    ) {}
    togglebtn() {
        this.toggle = !this.toggle;
    }

    ngOnInit() {
        this.faviconService.reset();
        this.faviconService.activate('alpha');
        this.titleService.setTitle('Alpha Vantage');

        this.forexCurrencyService();
    }
    /* Common Services */
    /*    Recipe Popup */
    showDialog() {
        this.display = true;
        // window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' });
    }
    /*Logout Service */
    logOut() {
        this.dynamicScriptLoader
            .load('senderLogout-script')
            .then(data => {})
            .catch(error => console.log(error));
    }
    /* Clean Component Service */
    onComponentChange(value) {
        this.componentToggle = value;
        this.selectedSymbol = '';
        this.chartConfig = {};
        this.loading = false;
        this.requestedData = [];
        this.selectedCompany = '';
        this.timeframe = 'DAILY';
        this.timeinterval = 'ONE_MINUTE';
        this.searchBox = '';
        this.curtimeframe = 'DAILY';
        this.curtimeinterval = 'ONE_MINUTE';
        this.cur_from = '';
        this.cur_to = '';
        this.errormsg = '';
        this.selectedSymbol = '';
        this.cryptoSearchSuggestion = '';
        this.display = false;
    }

    /*  Stocks Services  */

    /*  Stocks Symbol Search */
    stocksSymbolService(event) {
        this.errormsg = '';
        this.selectedSymbol = '';
        this.display = false;
        if (this.searchBox.length > 2) {
            this.restService.getRequest(`${environment.host}${environment.api}/alpha/stock/search?keyword=` + this.searchBox).subscribe(
                res => {
                    if (res) {
                        this.requestedData = res;
                    }
                    this.selectedCompany = event.query;
                },
                error => {
                    this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
                }
            );
        }
    }
    /*Stocks Chart Graph */
    loadStocksData() {
        this.errormsg = '';
        this.display = false;
        this.chartConfig = {
            data: null
        };
        if (this.selectedSymbol) {
            this.errormsg = '';
            this.loading = true;
            let url = `${environment.host}${environment.api}/alpha/stock?function=` + this.timeframe + '&symbol=' + this.selectedSymbol;

            if (this.timeinterval && this.timeframe === 'INTRADAY') {
                url = url + '&interval=' + this.timeinterval;
            }
            this.restService
                .getRequest(url)

                .subscribe(
                    res => {
                        this.chartConfig = {
                            data: { ...res }
                        };
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
                    }
                );
        } else {
            this.errormsg = 'Please provide the required details.';
        }
    }
    /*Stocks Recipe Causalens */
    sendStocksDataToCausalens() {
        this.errormsg = '';
        let url =
            `${environment.host}${environment.api}/alpha/stock/data?function=` +
            this.timeframe +
            '&symbol=' +
            this.selectedSymbol +
            '&datatype=csv&userid=' +
            localStorage.getItem('uid');

        if (this.timeinterval && this.timeframe === 'INTRADAY') {
            url = url + '&interval=' + this.timeinterval;
        }
        this.restService.getRequest(url).subscribe(
            (res: any) => {
                // console.log('hello', res);
                // this.router.navigate(['/causalens']);
                if (res.url && res.fileName) {
                    localStorage.setItem('url', res.url);
                    localStorage.setItem('fileName', res.fileName);
                }

                this.dynamicScriptLoader
                    .load('sender-script')
                    .then(data => {
                        // Script Loaded Successfully
                        console.log('script run');
                        this.display = false;
                    })
                    .catch(error => console.log(error));
            },
            error => {
                this.errormsg = 'Sorry! Some internal error occurred. Please try again later.';
            }
        );
    }

    /* Symbol Select Service */
    onStocksSelect(event) {
        console.log(event.symbol);
        if (event.symbol) {
            this.selectedSymbol = event.symbol;
        }
    }

    /*Foreign Exchange Services*/

    /*Forex Currency List Service */
    /* To and From Currency Select Service */
    selectedFromCurrency(e) {
        this.fromCurrency = e.target.value;
    }
    selectedToCurrency(e) {
        this.toCurrency = e.target.value;
    }
    forexCurrencyService() {
        this.display = false;
        this.restService.getRequest('content/currency.json').subscribe(response => {
            this.currencyData = response;
        });
    }
    /* Forex Chart Graph */
    loadForexData() {
        this.errormsg = '';
        this.chartConfig = {
            data: null
        };
        this.display = false;
        this.loading = true;
        if (this.fromCurrency && this.toCurrency) {
            if (this.fromCurrency != this.toCurrency) {
                let url =
                    `${environment.host}${environment.api}/alpha/forexseries?function=` +
                    this.curtimeframe +
                    '&from_currency=' +
                    this.fromCurrency +
                    '&to_currency=' +
                    this.toCurrency;
                if (this.curtimeinterval && this.curtimeframe === 'INTRADAY') {
                    url = url + '&interval=' + this.curtimeinterval;
                }
                this.restService.getRequest(url).subscribe(
                    (res: any) => {
                        this.loading = false;
                        this.chartConfig = {
                            data: { ...res }
                        };
                    },
                    error => {
                        this.loading = false;
                        this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
                    }
                );
            } else {
                this.errormsg = 'Please choose different currency';
                this.loading = false;
            }
        } else {
            this.loading = false;
            this.errormsg = 'Please provide the required currency details';
        }
    }

    /*Forex Recipe Causalens */
    sendForexDataToCausalens() {
        let url =
            `${environment.host}${environment.api}/alpha/forexseries/data?function=` +
            this.curtimeframe +
            '&from_currency=' +
            this.fromCurrency +
            '&to_currency=' +
            this.toCurrency +
            '&datatype=csv&userid=' +
            localStorage.getItem('uid');

        if (this.curtimeinterval && this.curtimeframe === 'INTRADAY') {
            url = url + '&interval=' + this.curtimeinterval;
        }
        this.restService.getRequest(url).subscribe(
            (res: any) => {
                if (res.url && res.fileName) {
                    localStorage.setItem('url', res.url);
                    localStorage.setItem('fileName', res.fileName);
                }
                this.dynamicScriptLoader
                    .load('sender-script')
                    .then(data => {
                        // Script Loaded Successfully
                        console.log('script run');
                        this.display = false;
                    })
                    .catch(error => console.log(error));
            },
            error => {
                this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
            }
        );
    }

    /*CryptoCurrency Services */
    /*CryptoCurrency Symbol Service */
    cryptoSymbolService(event) {
        this.errormsg = '';
        this.display = false;
        if (this.cryptoSearchSuggestion.length > 2) {
            this.restService
                .getRequest(`${environment.host}${environment.api}/alpha/crypto/search?keyword=` + this.cryptoSearchSuggestion)
                .subscribe(
                    res => {
                        if (res) {
                            this.requestedData = res;
                        }
                        this.selectedCompany = event.query;
                    },
                    error => {
                        this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
                    }
                );
        }
    }
    /*CryptoCurrency Select Symbol Service */
    onCryptoSelect(event) {
        console.log(event.currencyCode);
        if (event.currencyCode) {
            this.selectedSymbol = event.currencyCode;
        }
    }

    /*CryptoCurrency Chart Graph Service */
    loadCryptoData() {
        this.errormsg = '';
        this.chartConfig = {
            data: null
        };
        this.display = false;
        if (this.cryptoSearchSuggestion) {
            this.loading = true;
            const url =
                `${environment.host}${environment.api}/alpha/crypto?function=` + this.cryptoTimeFrame + '&symbol=' + this.selectedSymbol;

            this.restService.getRequest(url).subscribe(
                res => {
                    this.chartConfig = {
                        data: { ...res }
                    };
                    this.loading = false;
                },
                error => {
                    this.loading = false;
                    this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
                }
            );
        } else {
            this.loading = false;
            this.errormsg = 'Please provide the required details.';
        }
    }
    /*Crypto Recipe Causalens */
    sendCryptoDataToCausalens() {
        let url =
            `${environment.host}${environment.api}/alpha/crypto/data?function=` +
            this.cryptoTimeFrame +
            '&datatype=csv&userid=' +
            localStorage.getItem('uid') +
            '&symbol=' +
            this.selectedSymbol;

        this.restService.getRequest(url).subscribe(
            (res: any) => {
                if (res.url && res.fileName) {
                    localStorage.setItem('url', res.url);
                    localStorage.setItem('fileName', res.fileName);
                }

                this.dynamicScriptLoader
                    .load('sender-script')
                    .then(data => {
                        // Script Loaded Successfully
                        console.log('script run');
                        this.display = false;
                    })
                    .catch(error => console.log(error));
            },
            error => {
                this.errormsg = 'Sorry! Some internal error occurred. Please try again.';
            }
        );
    }
}
