import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dateConverterIntraday'
})
export class DateConverterIntradayPipe implements PipeTransform {
    transform(date: string): any {
        return date.substring(0, 10) + ', ' + date.substring(11, 19);
    }
}
