import { Injectable } from '@angular/core';
// import {x} from '../scripts/receiverScript'

declare var document: any;
@Injectable({
    providedIn: 'root'
})
export class DynamicScriptLoaderServiceService {
    private scripts: any = {};

    constructor() {
        ScriptStore.forEach((script: any) => {
            this.scripts[script.name] = {
                loaded: false,
                src: script.src
            };
        });
    }

    load(...scripts: string[]) {
        const promises: any[] = [];
        scripts.forEach(script => promises.push(this.loadScript(script)));
        return Promise.all(promises);
    }

    loadScript(name: string) {
        return new Promise((resolve, reject) => {
            //load script
            console.log('loaded2');
            let script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = this.scripts[name].src;
            console.log(script.src);
            if (script.readyState) {
                //IE
                script.onreadystatechange = () => {
                    if (script.readyState === 'loaded' || script.readyState === 'complete') {
                        console.log('hua');
                        script.onreadystatechange = null;
                        this.scripts[name].loaded = true;
                        resolve({ script: name, loaded: true, status: 'Loaded' });
                    }
                };
            } else {
                //Others
                script.onload = () => {
                    console.log('hua');
                    this.scripts[name].loaded = true;
                    resolve({ script: name, loaded: true, status: 'Loaded' });
                };
            }
            script.onerror = (error: any) => resolve({ script: name, loaded: false, status: 'Loaded' });
            console.log(script.onerror);
            document.getElementsByTagName('head')[0].appendChild(script);
        });
    }
}

interface Scripts {
    name: string;
    src: string;
}

// import x from '../../assets/senderScript'
// ../scripts/
export const ScriptStore: Scripts[] = [
    { name: 'receiver-script', src: 'content/receiverScript.js' },
    { name: 'sender-login', src: 'content/senderLoginScript.js' },
    { name: 'sender-script', src: 'content/senderScript.js' },
    { name: 'senderLogout-script', src: 'content/senderLogoutScript.js' },
    { name: 'receiverSocket-script', src: 'content/receiverSocketScript.js' },
    { name: 'receiverLogout-script', src: 'content/receiverLogoutScript.js' },
    { name: 'senderInit-script', src: 'content/senderInitScript.js' },
    { name: 'tickDataLogin-script', src: 'content/tickDataLoginScript.js' },
    { name: 'tickDataLogout-script', src: 'content/tickDataLogoutScript.js' },
    { name: 'tickDataSender-script', src: 'content/tickDataSenderScript.js' }
];
