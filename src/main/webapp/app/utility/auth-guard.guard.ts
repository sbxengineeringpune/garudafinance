import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
    router;
    logedInUser: any;
    constructor(router1: Router) {
        this.router = router1;
    }
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        this.logedInUser = JSON.parse(localStorage.getItem('uid'));
        //if(this.router.url=="/alpha")){
        if (this.logedInUser) {
            return true;
        } else {
            this.router.navigate(['/alpha']);
            return false;
        }
        // }
        //if(this.router.url=="/causalens")){
        //  if (this.logedInUser) {
        //      //this.router.navigate(['/causalens/home']);
        //        return true;
        //  }else {
        //       this.router.navigate(['/causalens']);
        //       return false;
        //  }
        //}
    }
}
