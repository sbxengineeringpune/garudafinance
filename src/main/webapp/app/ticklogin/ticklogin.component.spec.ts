import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicScriptLoaderServiceService } from 'app/utility/dynamic-script-loader-service.service';
import { TickloginComponent } from './ticklogin.component';
describe('TickloginComponent', () => {
    let component: TickloginComponent;
    let fixture: ComponentFixture<TickloginComponent>;
    beforeEach(() => {
        const routerStub = {};
        const dynamicScriptLoaderServiceServiceStub = {
            load: string1 => ({ then: () => ({ catch: () => ({}) }) })
        };
        TestBed.configureTestingModule({
            schemas: [NO_ERRORS_SCHEMA],
            declarations: [TickloginComponent],
            providers: [
                { provide: Router, useValue: routerStub },
                {
                    provide: DynamicScriptLoaderServiceService,
                    useValue: dynamicScriptLoaderServiceServiceStub
                }
            ]
        });
        fixture = TestBed.createComponent(TickloginComponent);
        component = fixture.componentInstance;
    });
    it('can load instance', () => {
        expect(component).toBeTruthy();
    });
    describe('login', () => {
        it('makes expected calls', () => {
            const dynamicScriptLoaderServiceServiceStub: DynamicScriptLoaderServiceService = fixture.debugElement.injector.get(
                DynamicScriptLoaderServiceService
            );
            spyOn(dynamicScriptLoaderServiceServiceStub, 'load').and.callThrough();
            component.login();
            expect(dynamicScriptLoaderServiceServiceStub.load).toHaveBeenCalled();
        });
    });
});
