import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicScriptLoaderServiceService } from 'app/utility/dynamic-script-loader-service.service';
import { Title } from '@angular/platform-browser';
import { BrowserFavicons } from 'app/utility/favicon.service';

@Component({
    selector: 'jhi-ticklogin',
    templateUrl: './ticklogin.component.html',
    styleUrls: ['./ticklogin.component.css']
})
export class TickloginComponent implements OnInit {
    email: string;
    password: string;
    constructor(
        private faviconService: BrowserFavicons,
        private titleService: Title,
        private router: Router,
        private dynamicScriptLoaderService: DynamicScriptLoaderServiceService
    ) {}
    ngOnInit() {
        this.faviconService.reset();
        this.faviconService.activate('tickdata');
        this.titleService.setTitle('Tick Data');
    }
    login() {
        localStorage.setItem('tickDataEmail', this.email);
        localStorage.setItem('tickDataPassword', this.password);
        this.dynamicScriptLoaderService

            .load('tickDataLogin-script')
            .then(data => {})
            .catch(error => console.log(error));
    }
}
