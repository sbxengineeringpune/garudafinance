import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-charting-playground',
    templateUrl: './charting-playground.component.html',
    styleUrls: ['./charting-playground.component.css']
})
export class ChartingPlaygroundComponent implements OnInit {
    public chartType: string;

    public jsonInput: any;

    public chartTypes: string[] = ['Line', 'Candle Stick', 'Causa Lens'];

    public chartConfig: any = {
        data: null
    };

    public generateGraph(): void {
        this.chartConfig = {
            data: { ...JSON.parse(this.jsonInput) },
            type: this.getChartType(this.chartType)
        };
    }

    private getChartType(type): string {
        if (type === 'Line') return 'line';
        else if (type === 'Candle Stick') return 'candlestick';
        else return 'causalens';
    }

    constructor() {}

    ngOnInit() {}
}
