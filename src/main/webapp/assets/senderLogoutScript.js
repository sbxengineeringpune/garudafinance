var GADGET_NAME 		= "Alpha Vantage" // Copy paste your web gadget name from developer portal gadget summary page 
var GADGET_KEY			= "AI9RpVnxN7Qo+hOGRucurfhQZLStS7UhLeSTwOjIfuA=" 
var uid = localStorage.getItem('uid');

garudapro.webapi.extended.sendLogoutRequest(GADGET_NAME, GADGET_KEY, uid);

// alert("senderlogout");
handleLogoutResponse = function (response)
{
	if(response.body.result == 200)
	{
        // localStorage.clear(); //clearing the local storage
		//alert("Logged out from Garuda server");
		localStorage.removeItem("email");
		localStorage.removeItem("password");
		localStorage.removeItem("uid");
		window.location.href = '#/alpha';
		// In case of successful logout you should see this alert in your browser console.
	}
	else
	{
		alert("Unable to logout from Garuda server");
		// In case of logout is failed you should see this alert in your browser.
	}
};