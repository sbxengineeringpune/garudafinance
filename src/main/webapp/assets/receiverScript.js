var GADGET_NAME         = "CausaLens" // Copy paste your web gadget name from developer portal gadget summary page
var GADGET_KEY           = "HIGu2OsNdVh+XSQxhr1UexIRIk8qAJ+6" // Copy paste your web gadget key from developer portal gadget summary page
var GATEWAY_EMAIL        = localStorage.getItem('causalense-email'); // Copy paste your email id that you have used to login to Garuda 2X gateway
var GATEWAY_PASSWORD     = localStorage.getItem('causalense-password'); // Copy paste your password that you have used to login to Garuda 2X gateway

// alert("receiverScript");
garudapro.webapi.scylla.init("http", "50.112.103.132", "8090");

// Next send activation request to Garuda server
// Please copy paste your web gadgets name and gadget key
// for example if your web gadget name is "test" and gadget key is "100" then the call will look like"
// garudapro.webapi.base.Activate("test", "100");

// alert("Send gadget activation request to Garuda server");

garudapro.webapi.base.Activate(GADGET_NAME, GADGET_KEY);  //DEBUG MODE

// Handling gadget activation response

var handleActivateGadgetResponse = function (response) 
{	
	if(response.body.result === 200) 
	{
		// alert("Gadget activated successfully");
		// On successful activation you should see this alert in your browser.

		// Please replace the sample argument values with yours
		garudapro.webapi.extended.sendLoginRequest(GADGET_NAME, GADGET_KEY, GATEWAY_EMAIL, GATEWAY_PASSWORD);
	}
   else
   {
		alert("Gadget activation failed");

		// Please handle other result codes here according to your need
		// You will see this alert in case gadget activation is failed
   }
};

handleLoginSuccessResponse = function (response)
{
	// alert("Login to Garuda successful uid =" + response.body.uid);
	// On successful login you should see this alert in your browser.
	
	uid = response.body.uid;
	localStorage.setItem('recieverUid', uid);
	window.location.href = '#/causalens/home';
};

handleLoginFailResponse = function (response)
{
	alert("Failed to login to Garuda server");
	// In case of login failure you should see this alert in your browser.
};

// //Handler for Receiving Data from other gadgets web or non web
// var handleSentDataToGadgetRequest = function(data)
// {
// 	// alert("inside");
// 	console.log(data);
// 	var gadgetName			= data.body.sourceGadgetName;
// 	var gadgetData			= data.body.data;
// 	var dataFormat			= data.body.format;
// 	var stream				= data.body.isStream;
// 	var fileName 			= data.body.filename;
// 	var dataUrl   			= data.body.data;
// 	if (stream) 
// 	{
// 		alert("You have received stream data from gadget " + gadgetName + " and data format is " + dataFormat);
// 		document.getElementById('showFetchedData').appendChild(gadgetData);
// 		alert(gadgetData);
// 	}
// 	else
// 	{

// 		var option = document.createElement('option');
// 		alert("You have received a data url from gadget " + gadgetName + " and data format is " + dataFormat);
		
// 		// var textNode = document.createTextNode(gadgetData);
// 		option.value = dataUrl;
// 		option.text = fileName;
// 		// node.appendChild(gadgetData);
// 		document.getElementById('showFetchedData').appendChild(option);
// 		alert(gadgetData);
// 	}
// }
