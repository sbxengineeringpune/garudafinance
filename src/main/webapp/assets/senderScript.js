

var GADGET_NAME 		= "Alpha Vantage" // Copy paste your web gadget name from developer portal gadget summary page 
var GADGET_KEY			= "AI9RpVnxN7Qo+hOGRucurfhQZLStS7UhLeSTwOjIfuA=" 
var uid = localStorage.getItem('uid');
garudapro.webapi.base.getCompatibleGadgets(GADGET_NAME, GADGET_KEY, uid , "csv", "csv");

// alert("senderScript");

handleGetCompatibleGadgetListResponse = function (response) 
{
	if (response.result == 200 || response.result == 206) 
	{

		var data_url = localStorage.getItem('url');
		var fileName = localStorage.getItem('fileName');
        garudapro.webapi.base.SendDataToGadgetRequest(GADGET_NAME, GADGET_KEY, "CausaLens", "1033", data_url, false, fileName, "csv", "csv", "", TARGET_DEVICE_ID, uid);
        localStorage.setItem(fileName,GADGET_NAME);
	}
	else if(response.result == 201)
	{

	}
	else
	{
		alert("No Compatible gadgets found or faced some internal error");
		// In case of failure you should see this alert in your browser.	
	}
}

handleSentDataToGadgetResponse = function (response)
{
	if (response.result == 527) 
	{
		alert("In debug mode / draft mode you can only transfer data to garuda guru and web sdk test gadget running in your dashboard");
		return;
	}

	if (response.result >= 200 && response.result <300) 
	{
		alert("Data has been transferred to CausaLens.");
		return;
	}

	if (response.result != 200) 
	{
		alert("Failed to transfer Data");
	}
}
