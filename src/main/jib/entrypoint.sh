#!/bin/sh

echo "The application will start in ${JHIPSTER_SLEEP}s..." && sleep ${JHIPSTER_SLEEP}
exec java ${JAVA_OPTS}  -cp /app/resources/:/app/classes/:/app/libs/* "poc.sbx.garudafinance.GarudafinanceApp"  "$@"
