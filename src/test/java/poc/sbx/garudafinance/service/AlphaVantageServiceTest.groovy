package poc.sbx.garudafinance.service


import poc.sbx.garudafinance.config.alpha.DataType
import poc.sbx.garudafinance.config.alpha.IAlphaVantageClient
import poc.sbx.garudafinance.repository.UserDetailsRepository
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval
import poc.sbx.garudafinance.service.alpha.request.OutputSize
import poc.sbx.garudafinance.service.alpha.response.MetaData
import poc.sbx.garudafinance.service.alpha.timeseries.*
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest
import java.time.ZoneId
import java.time.ZonedDateTime

class AlphaVantageServiceTest extends Specification {

    private IAlphaVantageClient mockClient = Mock(IAlphaVantageClient.class)

    private HttpServletRequest mockRequest = Mock(HttpServletRequest.class)

    private FileStorageService mockFileStorageService = Mock(FileStorageService.class)

    private UserDetailsRepository mockUserDetailsRepository = Mock(UserDetailsRepository.class)

    private AlphaVantageService alphaVantageService

    def setup() {
        alphaVantageService = new AlphaVantageService(client: mockClient, request: mockRequest, fileStorageService: mockFileStorageService, userDetailsRepository: mockUserDetailsRepository)
    }


    def "getTimeSeriesData: Should retun the TimeSeriesResult for Daily"() {
        given:
        String symbol = "APPL"
        MetaData metaData = new MetaData()
        metaData.setInformation("Test")
        TimeSeries timeSeries = new TimeSeries()
        timeSeries.setHigh(2.0)
        timeSeries.setLow(1.0)
        Map<ZonedDateTime, TimeSeries> timeSeriesDataWithDate = new HashMap<ZonedDateTime, TimeSeries>() {
            {
                put(ZonedDateTime.now(ZoneId.of("UTC")), timeSeries)
            }
        }
        TimeSeriesResult timeSeriesResult = new TimeSeriesResult()
        timeSeriesResult.setMetaData(metaData)
        timeSeriesResult.setTimeSeries(timeSeriesDataWithDate)

        when:
        TimeSeriesResult response = alphaVantageService.getTimeSeriesData(symbol, TimeSeriesFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES)
        assert response.metaData.information.equals("Test")

        then:
        1 * mockClient.getTimeSeries(TimeSeriesFunction.DAILY, symbol) >> timeSeriesResult

    }


    def "getTimeSeriesData: Should retun the TimeSeriesResult for Intraday"() {
        given:
        String symbol = "APPL"
        MetaData metaData = new MetaData()
        metaData.setInformation("Test")
        TimeSeries timeSeries = new TimeSeries()
        timeSeries.setHigh(2.0)
        timeSeries.setLow(1.0)
        Map<ZonedDateTime, TimeSeries> timeSeriesDataWithDate = new HashMap<ZonedDateTime, TimeSeries>() {
            {
                put(ZonedDateTime.now(ZoneId.of("UTC")), timeSeries)
            }
        }
        TimeSeriesResult timeSeriesResult = new TimeSeriesResult()
        timeSeriesResult.setMetaData(metaData)
        timeSeriesResult.setTimeSeries(timeSeriesDataWithDate)

        when:
        TimeSeriesResult response = alphaVantageService.getTimeSeriesData(symbol, TimeSeriesFunction.INTRADAY, IntradayInterval.FIFTEEN_MINUTES)
        assert response.metaData.information.equals("Test")

        then:
        1 * mockClient.getTimeSeries(IntradayInterval.FIFTEEN_MINUTES, symbol, OutputSize.COMPACT) >> timeSeriesResult

    }

    def "getTimeSeriesDataPoint: Should return the TimeseriesDataResult"() {
        given:
        String symbol = "Apple Inc"
        TimeseriesDataResult timeseriesDataResult = new TimeseriesDataResult()
        timeseriesDataResult.setUrl("http://filelocation")
        timeseriesDataResult.setFileName("test.csv")
        File file = new File("http://fileLocation")
        UserEntity userEntity = new UserEntity()
        String dateDataType = "IndexDate";
        String dateFormat = "%Y-%m-%d";

        when:
        TimeseriesDataResult response = alphaVantageService.getTimeSeriesDataPoint(symbol, TimeSeriesFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES, DataType.csv, userEntity)

        then:
        1 * mockClient.getTimeSeriesDataPoint(TimeSeriesFunction.DAILY, symbol, DataType.csv) >> file
    }

    def "getSymbols: Should return the SymbolResult"() {
        given:
        String keyword = "APPL"
        Symbol symbol = new Symbol()
        symbol.setSymbol(keyword)
        List symbols = [symbol]
        SymbolResult symbolResult = new SymbolResult();
        symbolResult.setBestMatches(symbols)
        when:
        SymbolResult response = alphaVantageService.getSymbols(keyword)
        assert response.getBestMatches().size() == 1

        then:
        1 * mockClient.getSymbolList(keyword) >> symbolResult
    }

    def "getCurrencyExchange: Should return the CurrencyExchange"() {
        given:
        String toSymbol = "BTC"
        String fromSymbol = "INR"
        CurrencyExchange currencyExchange = new CurrencyExchange()
        currencyExchange.setExchangeRate(1.0D)
        currencyExchange.setFromCurrency("INR")
        currencyExchange.setFromCurrencyName("Rupees")
        currencyExchange.setToCurrency("BTC")
        currencyExchange.setToCurrencyName("Bitcoin")

        when:
        CurrencyExchange response = alphaVantageService.getCurrencyExchange(toSymbol, fromSymbol)
        assert response.exchangeRate == 1.0D

        then:
        1 * mockClient.getCurrencyExchange(toSymbol, fromSymbol) >> currencyExchange

    }


}
