package poc.sbx.garudafinance.web.rest.controller

import poc.sbx.garudafinance.config.alpha.DataType
import poc.sbx.garudafinance.service.AlphaVantageService
import poc.sbx.garudafinance.service.alpha.request.IntradayInterval
import poc.sbx.garudafinance.service.alpha.response.MetaData
import poc.sbx.garudafinance.service.alpha.timeseries.*
import spock.lang.Specification

import java.time.ZoneId
import java.time.ZonedDateTime

class AlphaVantageControllerTest extends Specification {

    AlphaVantageService mockAlphaVantageService = Mock(AlphaVantageService.class)
    AlphaVantageController alphaVantageController

    def setup() {
        alphaVantageController = new AlphaVantageController(alphaService: mockAlphaVantageService)
    }


    def "getSymbolSearchData: Should return the company name and symbol"() {
        given:
        String keyword = "APPL"
        Symbol symbol = new Symbol()
        symbol.setSymbol(keyword)
        List symbols = [symbol]
        SymbolResult symbolResult = new SymbolResult();
        symbolResult.setBestMatches(symbols)

        when:
        SymbolResult result = alphaVantageController.getSymbolSearchData(keyword)
        assert result.getBestMatches().size() == 1

        then:
        1 * mockAlphaVantageService.getSymbols(keyword) >> symbolResult
    }


    def "getTimeSeriesData: Should return the Time series data for stocks"() {
        given:
        String symbol = "Apple Inc"
        MetaData metaData = new MetaData()
        metaData.setInformation("Test")
        TimeSeries timeSeries = new TimeSeries()
        timeSeries.setHigh(2.0)
        timeSeries.setLow(1.0)
        Map<ZonedDateTime, TimeSeries> timeSeriesDataWithDate = new HashMap<ZonedDateTime, TimeSeries>() {
            {
                put(ZonedDateTime.now(ZoneId.of("UTC")), timeSeries)
            }
        }
        TimeSeriesResult timeSeriesResult = new TimeSeriesResult()
        timeSeriesResult.setMetaData(metaData)
        timeSeriesResult.setTimeSeries(timeSeriesDataWithDate)

        when:
        TimeSeriesResult result = alphaVantageController.getTimeSeriesData(symbol, TimeSeriesFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES, "Test")
        assert result.metaData.information == "Test"
        assert result.timeSeries.size() == 1
        then:
        1 * mockAlphaVantageService.getTimeSeriesData(symbol, TimeSeriesFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES) >> timeSeriesResult
    }


    def "getTimeSeriesDataPoint: Should return the TimeseriesDataResult"() {
        given:
        String symbol = "Apple Inc"
        TimeseriesDataResult timeseriesDataResult = new TimeseriesDataResult()
        timeseriesDataResult.setUrl("http://filelocation")
        timeseriesDataResult.setFileName("test.csv")

        when:
        TimeseriesDataResult result = alphaVantageController.getTimeSeriesDataPoint(symbol, TimeSeriesFunction.DAILY, DataType.csv, IntradayInterval.FIFTEEN_MINUTES, 12345L)
        assert result.fileName == "test.csv"
        assert result.url == "http://filelocation"
        then:
        1 * mockAlphaVantageService.getTimeSeriesDataPoint(symbol, TimeSeriesFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES, DataType.csv, _ as UserEntity) >> timeseriesDataResult
    }


    def "getExchangeRate: Should return the CurrencyExchange"() {
        given:
        String toSymbol = "BTC"
        String fromSymbol = "INR"
        CurrencyExchange currencyExchange = new CurrencyExchange()
        currencyExchange.setExchangeRate(1.0D)
        currencyExchange.setFromCurrency("INR")
        currencyExchange.setFromCurrencyName("Rupees")
        currencyExchange.setToCurrency("BTC")
        currencyExchange.setToCurrencyName("Bitcoin")

        when:
        CurrencyExchange result = alphaVantageController.getExchangeRate(toSymbol, fromSymbol)
        assert result.exchangeRate == 1.0D
        then:
        1 * mockAlphaVantageService.getCurrencyExchange(toSymbol, fromSymbol) >> currencyExchange
    }

    def "getForeignExchangeSeries: Should return the ForeignExchangeResult"() {
        given:
        String toSymbol = "BTC"
        String fromSymbol = "INR"
        MetaData metaData = new MetaData()
        metaData.setInformation("Test")
        ForeignExchange timeSeries = new ForeignExchange()
        timeSeries.setHigh(2.0)
        timeSeries.setLow(1.0)
        Map<ZonedDateTime, ForeignExchange> timeSeriesDataWithDate = new HashMap<ZonedDateTime, ForeignExchange>() {
            {
                put(ZonedDateTime.now(ZoneId.of("UTC")), timeSeries)
            }
        }
        ForeignExchangeResult timeSeriesResult = new ForeignExchangeResult()
        timeSeriesResult.setMetaData(metaData)
        timeSeriesResult.setForeignExchangeQuotes(timeSeriesDataWithDate)

        when:
        ForeignExchangeResult result = alphaVantageController.getForeignExchangeSeries(ForeignExchangeFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES, fromSymbol, toSymbol)
        assert result.metaData.information == "Test"
        then:
        1 * mockAlphaVantageService.getForeignExchangeSeries(ForeignExchangeFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES, fromSymbol, toSymbol) >> timeSeriesResult
    }


    def "getForexSeriesDataPoint: Should return the TimeseriesDataResult"() {
        given:
        String toSymbol = "BTC"
        String fromSymbol = "INR"
        TimeseriesDataResult timeseriesDataResult = new TimeseriesDataResult()
        timeseriesDataResult.setUrl("http://filelocation")
        timeseriesDataResult.setFileName("test.csv")

        when:
        TimeseriesDataResult result = alphaVantageController.getForexSeriesDataPoint(ForeignExchangeFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES, fromSymbol, toSymbol, DataType.csv, 12345L)
        assert result.fileName == "test.csv"
        assert result.url == "http://filelocation"
        then:
        1 * mockAlphaVantageService.getForexSeriesDataPoint(ForeignExchangeFunction.DAILY, IntradayInterval.FIFTEEN_MINUTES, fromSymbol, toSymbol, DataType.csv, _ as UserEntity) >> timeseriesDataResult
    }

    def "getCryptoCurrencies: Should return the CryptoCurrencySearchResult"() {
        given:
        String keyword = "INR"
        CryptoCurrency cryptoCurrency = new CryptoCurrency()
        cryptoCurrency.setCurrencyCode("INR")
        cryptoCurrency.setCurrencyName("Rupess")
        List<CryptoCurrency> cryptoCurrencies = [cryptoCurrency]
        CryptoCurrencySearchResult cryptoCurrencySearchResult = new CryptoCurrencySearchResult()
        cryptoCurrencySearchResult.setCryptoCurrencies(cryptoCurrencies)


        when:
        CryptoCurrencySearchResult result = alphaVantageController.getCryptoCurrencies(keyword)
        assert result.cryptoCurrencies.size() == 1
        assert result.cryptoCurrencies.get(0).currencyCode == "INR"

        then:
        1 * mockAlphaVantageService.getCryptoSearchResult(keyword) >> cryptoCurrencySearchResult
    }

    def "getCryptocurrencyTimeSeriesData: Should return the CryptoCurrencyTimeSeriesResult"() {
        given:
        String symbol = "BTC"
        MetaData metaData = new MetaData()
        metaData.setInformation("Test")
        TimeSeries timeSeries = new TimeSeries()
        timeSeries.setHigh(2.0)
        timeSeries.setLow(1.0)
        Map<ZonedDateTime, TimeSeries> timeSeriesDataWithDate = new HashMap<ZonedDateTime, TimeSeries>() {
            {
                put(ZonedDateTime.now(ZoneId.of("UTC")), timeSeries)
            }
        }
        CryptoCurrencyTimeSeriesResult timeSeriesResult = new CryptoCurrencyTimeSeriesResult()
        timeSeriesResult.setMetaData(metaData)
        timeSeriesResult.setTimeSeries(timeSeriesDataWithDate)

        when:
        CryptoCurrencyTimeSeriesResult result = alphaVantageController.getCryptocurrencyTimeSeriesData(CryptocurrencyFunction.DAILY, symbol)
        assert result.metaData.information == "Test"
        then:
        1 * mockAlphaVantageService.getCryptocurrencyTimeSeriesData(CryptocurrencyFunction.DAILY, symbol) >> timeSeriesResult
    }

    def "getCryptoDataPoint: Should return the TimeseriesDataResult"() {
        given:
        String symbol = "Apple Inc"
        TimeseriesDataResult timeseriesDataResult = new TimeseriesDataResult()
        timeseriesDataResult.setUrl("http://filelocation")
        timeseriesDataResult.setFileName("test.csv")

        when:
        TimeseriesDataResult result = alphaVantageController.getCryptoDataPoint(CryptocurrencyFunction.DAILY, symbol, DataType.csv, 12345L)
        assert result.fileName == "test.csv"
        assert result.url == "http://filelocation"
        then:
        1 * mockAlphaVantageService.getCryptoDataPoint(CryptocurrencyFunction.DAILY, symbol, DataType.csv, _ as UserEntity) >> timeseriesDataResult
    }
}
