package poc.sbx.garudafinance.web.rest.controller

import poc.sbx.garudafinance.service.CausalensService
import poc.sbx.garudafinance.service.alpha.response.MetaData
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeries
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeriesResult
import poc.sbx.garudafinance.service.causalens.CausaLensRequest
import poc.sbx.garudafinance.service.causalens.CausaLensTimeSeriesResponse
import spock.lang.Specification

import java.time.ZoneId
import java.time.ZonedDateTime

class CausaLensControllerTest extends Specification{

    CausalensService mockCausalenseService = Mock(CausalensService.class)
    CausaLensController causaLensController

    def setup(){
        causaLensController = new CausaLensController(causaService:mockCausalenseService)
    }

    def "getTimeSeriesPredictionFromCausalens: Should return the CausaLensTimeSeriesResponse"(){
        given:
        CausaLensRequest causaLensRequest = new CausaLensRequest()
        causaLensRequest.setFileName("test.csv")
        causaLensRequest.setCsvLocation("http://csvfilelocation")
        causaLensRequest.setTargetColumn("open")

        MetaData metaData = new MetaData()
        metaData.setInformation("Test")
        TimeSeries timeSeries = new TimeSeries()
        timeSeries.setHigh(2.0)
        timeSeries.setLow(1.0)
        Map<ZonedDateTime, TimeSeries> timeSeriesDataWithDate = new HashMap<ZonedDateTime, TimeSeries>() {
            {
                put(ZonedDateTime.now(ZoneId.of("UTC")), timeSeries)
            }
        }
        TimeSeriesResult timeSeriesResultActual = new TimeSeriesResult()
        timeSeriesResultActual.setMetaData(metaData)
        timeSeriesResultActual.setTimeSeries(timeSeriesDataWithDate)

        TimeSeriesResult timeSeriesResultPrediction = new TimeSeriesResult()
        timeSeriesResultPrediction.setMetaData(metaData)
        timeSeriesResultPrediction.setTimeSeries(timeSeriesDataWithDate)

        CausaLensTimeSeriesResponse causaLensTimeSeriesResponse = new CausaLensTimeSeriesResponse()
        causaLensTimeSeriesResponse.setActual(timeSeriesResultActual)
        causaLensTimeSeriesResponse.setPredictions(timeSeriesResultPrediction)
        causaLensTimeSeriesResponse.setModelName("prediction")


        when:
        CausaLensTimeSeriesResponse response = causaLensController.getTimeSeriesPredictionFromCausalens(causaLensRequest)
        assert response.modelName == "prediction"
        then:
        1 * mockCausalenseService.getTimeSeriesPredictionFromCausalens(causaLensRequest) >> causaLensTimeSeriesResponse

    }

}
