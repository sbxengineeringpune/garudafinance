package poc.sbx.garudafinance.web.rest.controller


import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import poc.sbx.garudafinance.config.alpha.FileEditterService
import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity
import poc.sbx.garudafinance.service.causalens.FileRequest
import spock.lang.Specification

import javax.servlet.http.HttpServletResponse

class FileControllerTest extends Specification {
    FileEditterService mockFileEditterService = Mock(FileEditterService.class)
    FileController fileController

    def setup() {
        fileController = new FileController(fileService: mockFileEditterService)
    }

    def "getTargetValues: Should retun the list of target columns"() {
        given:
        String csvPath = "http://csvfilelocation/test.csv";
        FileRequest fileRequest = new FileRequest()
        fileRequest.setCsvFilePath(csvPath)

        when:
        ResponseEntity<List> response = fileController.getTargetValues(fileRequest)
        assert response.getBody().size() == 4
        assert response.statusCodeValue == 200;

        then:
        1 * mockFileEditterService.getTargets(csvPath) >> ["open", "high", "low", "close"]
    }

    def "getFileList: Should retun the list of UserEntity"() {
        given:
        String csvPath = "http://csvfilelocation/test.csv";
        Long userId = 12345L
        UserEntity userEntity = new UserEntity()
        userEntity.setUserId(userId)
        userEntity.setPath(csvPath)
        List userEntityList = [userEntity]

        when:
        ResponseEntity<List<UserEntity>> response = fileController.getFileList(userId)
        assert response.statusCodeValue == 200;
        assert response.body.get(0).userId == userId

        then:
        1 * mockFileEditterService.getFileList(12345L) >> userEntityList
    }

    def "getDownloadFile: Should retun the  File Resource"() {
        given:
        HttpServletResponse httpServletResponse = Mock(HttpServletResponse.class);
        String csvPath = "http://csvfilelocation/test.csv";
        Resource resource = new FileSystemResource(csvPath)

        when:
        ResponseEntity<Resource> response = fileController.getDownloadFile(httpServletResponse, csvPath)
        assert response.statusCodeValue == 200;

        then:
        1 * mockFileEditterService.getFile(csvPath) >> new ResponseEntity<>(resource, HttpStatus.OK)
    }
}
