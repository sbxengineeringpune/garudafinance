package poc.sbx.garudafinance.web.rest.controller


import poc.sbx.garudafinance.service.TickDataService
import poc.sbx.garudafinance.service.alpha.response.MetaData
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeries
import poc.sbx.garudafinance.service.alpha.timeseries.TimeSeriesResult
import poc.sbx.garudafinance.service.alpha.timeseries.TimeseriesDataResult
import poc.sbx.garudafinance.service.alpha.timeseries.UserEntity
import poc.sbx.garudafinance.service.tickdata.response.TickSearchSymbolResult
import poc.sbx.garudafinance.service.tickdata.response.TickSymbol
import spock.lang.Specification

import java.time.ZoneId
import java.time.ZonedDateTime

class TickDataControllerTest extends Specification {
    TickDataService mockTickDataService = Mock(TickDataService.class)
    TickDataController tickDataController

    def setup() {
        tickDataController = new TickDataController(tickServiceService: mockTickDataService)
    }

    def "getData: should retun the list of TickSymbol"() {
        given:
        String keyword = "APPL"
        TickSymbol tickSymbol = new TickSymbol()
        tickSymbol.setCompanyId("1213")
        tickSymbol.setCompanyName("Apple Inc")
        tickSymbol.setSymbol(keyword)
        List lisTickSymbol = [tickSymbol]
        TickSearchSymbolResult tickSearchSymbolResult = new TickSearchSymbolResult()
        tickSearchSymbolResult.setSymbols(lisTickSymbol)

        when:
        TickSearchSymbolResult response = tickDataController.getData(keyword)
        assert response.symbols.get(0).symbol.equals(keyword)

        then:
        1 * mockTickDataService.getTickSymbolSearhResult(keyword) >> tickSearchSymbolResult
    }


    def "getDailyBarsData: should retun the TimeSeriesResult"() {
        given:
        String keyword = "APPL"
        MetaData metaData = new MetaData()
        metaData.setInformation("Test")
        TimeSeries timeSeries = new TimeSeries()
        timeSeries.setHigh(2.0)
        timeSeries.setLow(1.0)
        Map<ZonedDateTime, TimeSeries> timeSeriesDataWithDate = new HashMap<ZonedDateTime, TimeSeries>() {
            {
                put(ZonedDateTime.now(ZoneId.of("UTC")), timeSeries)
            }
        }
        TimeSeriesResult timeSeriesResult = new TimeSeriesResult()
        timeSeriesResult.setMetaData(metaData)
        timeSeriesResult.setTimeSeries(timeSeriesDataWithDate)

        when:
        TimeSeriesResult response = tickDataController.getDailyBarsData(keyword, "1234", "10")
        assert response.metaData.information.equals("Test")

        then:
        1 * mockTickDataService.getDailyBarsData(keyword, "1234", "10") >> timeSeriesResult
    }

    def "getCryptoDataPoint: Should return the TimeseriesDataResult"() {
        given:
        String symbol = "Apple Inc"
        TimeseriesDataResult timeseriesDataResult = new TimeseriesDataResult()
        timeseriesDataResult.setUrl("http://filelocation")
        timeseriesDataResult.setFileName("test.csv")

        when:
        TimeseriesDataResult result = tickDataController.getCryptoDataPoint(symbol, "1234", "10", 12345L)
        assert result.fileName == "test.csv"
        assert result.url == "http://filelocation"

        then:
        1 * mockTickDataService.getTickDailyDataPoint(symbol, "1234", "10", _ as UserEntity) >> timeseriesDataResult
    }
}
